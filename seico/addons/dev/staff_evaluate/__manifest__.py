# -*- coding: utf-8 -*-
{
    'name':u'Đánh giá nhân sự',
    'description': u'Module đánh giá nhân sự',
    'version':'1.0',
    'author':'CuuNV',

    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',


        'data/data_quota.xml',

        # Kinh doanh
        'data/data_effect_template_gdttkd.xml',
        'data/data_effect_template_cvkdcc.xml',
        'data/data_effect_template_cvkd.xml',
        # Thiết kế
        'data/data_effect_template_gdtttk.xml',
        'data/data_effect_template_tptk.xml',
        'data/data_effect_template_cvtkcc.xml',
        'data/data_effect_template_cvtk.xml',
        # Sản xuất
        'data/data_effect_template_gdsxnm.xml',
        'data/data_effect_template_gdttsx.xml',
        'data/data_effect_template_nhxh.xml',
        'data/data_effect_template_qaqc.xml',
        'data/data_effect_template_qlsx.xml',
        # Thi công
        'data/data_effect_template_cht.xml',
        'data/data_effect_template_gstc.xml',
        'data/data_effect_template_tbpqltc.xml',
        # Nhân sự
        'data/data_effect_template_tbpns.xml',
        'data/data_effect_template_cvns.xml',
        # Marketing
        ################################
        # Tài chính
        'data/data_effect_template_cvtc.xml',
        # Kế toán
        'data/data_effect_template_tbpkt.xml',
        'data/data_effect_template_ktv.xml',
        # Mua hàng
        'data/data_effect_template_tbpmh.xml',
        # Hành chính
        ######################################

        'data/data_trial_template.xml',
        'data/data_position.xml',
        'data/data_users.xml',
        'data/data_company.xml',
        'data/data_files_folders.xml',
        'data/data.xml',
        'views/base.xml',
        'views/staff_view.xml',
        'views/admin_view.xml'
    ],
    'demo': [
		'data/demo.xml',
	],
    'website': 'vtechcom.org',
    'depends': ['base', 'web', 'customize'],
    "qweb": [
        'static/src/xml/templates.xml',
    ],
    'application': True,
}
