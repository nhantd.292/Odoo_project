odoo.define('staff_evaluate', function (require) {
"use strict";

var core = require('web.core');
var ListView = require('web.ListView');
var FormView = require('web.FormView');
var Model = require('web.DataModel');
var data = require('web.data');
var Pager = require('web.Pager');
var View = require('web.View');
var framework = require('web.framework');
var form_widgets = require('web.form_widgets');
var list_widget_registry = core.list_widget_registry;
var form_relational = require('web.form_relational');
var common = require('web.form_common');
var formats = require('web.formats');
var Dialog = require('web.Dialog');

var _t = core._t;
var _lt = core._lt;
var QWeb = core.qweb;

form_relational.FieldMany2ManyTags.include({
    render_value: function() {
        var self = this;
        var values = this.get("value");
        var handle_names = function(_data) {
            _.each(_data, function(el) {
                if (typeof(el.display_name) !== 'undefined')
                    el.display_name = el.display_name.trim() ? _.str.escapeHTML(el.display_name) : '';
            });
            self.render_tag(_data);
        };
        if (!values || values.length > 0) {
            if (self.name == 'groups_id') {
                var tmp = [];
                for (var i = 0; i < values.length; i++) {
                    if (USING_GROUP_IDS.indexOf(values[i]) != -1) {
                        tmp.push(values[i]);
                    }
                }
                values = tmp;
            }
            return this.alive(this.get_render_data(values)).done(handle_names);
        } else {
            handle_names([]);
        }
    }
});

ListView.include({
    init: function(){
        this._super.apply(this, arguments);
        // this.options.selectable = false;
        var pmodels = ['eval.effect.ticket', 'eval.trial.ticket', 'eval.incorrect.ticket', 'eval.files'];

        if ($.inArray(this.model, pmodels) === -1) {
            $('body.o_web_client').addClass('v_backend');
            $('body.o_web_client').removeClass('v_frontend');
        }
        else {
            $('body.o_web_client').addClass('v_frontend');
            $('body.o_web_client').removeClass('v_backend');
        }

        if (this.model == 'res.users') {
            $('div.v_users_list_header').show();
        }
        else {
            $('div.v_users_list_header').hide();
        }

        if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
            $('.o_web_client.v_frontend > .o_content').attr("style", "overflow:initial");
        }
    }
});

var UsersListView = ListView.extend({
    init: function(){
        this._super.apply(this, arguments);
        this.options.selectable = false;
        this.searchview_hidden = true;
        this.PM = new Model('eval.position');
        this.QM = new Model('eval.quota');
        this.filter_domain = [];
    },
    start: function(){
        this._super.apply(this, arguments);
        var self = this;
        var defs = [];

        // Add staff information top of view
        if ($('div.v_users_list_header').length == 0) {
            $('.o_web_client > .o_content').before(QWeb.render("UsersListViewFilter"));
        }

        this.QM.call('name_search', []).then(function(res){
            var htm = '<option></option><option value="">Tất cả ngạch bậc</option>';
            for (var i = 0; i < res.length; i++) {
                htm += '<option value="'+ res[i][0] +'">'+ res[i][1] +'</option>';
            }
            $('select.user_filter_select_quota').html(htm);
            $('select.user_filter_select_quota').chosen({search_contains:true});
            $('select.user_filter_select_quota').on('change', function(e, p) {
                self.my_filter();
            });
        });

        this.PM.call('name_search', []).then(function(res){
            var htm = '<option></option><option value="">Tất cả chức vụ</option>';
            for (var i = 0; i < res.length; i++) {
                htm += '<option value="'+ res[i][0] +'">'+ res[i][1] +'</option>';
            }
            $('select.user_filter_select_position').html(htm);
            $('select.user_filter_select_position').chosen({search_contains:true});
            $('select.user_filter_select_position').on('change', function(e, p) {
                self.my_filter();
            });
        });

        $('select.user_filter_select_level').chosen({disable_search:true});
        $('select.user_filter_select_level').on('change', function(e, p) {
            self.my_filter();
        });

        $('select.user_filter_select_state').chosen({disable_search:true});
        $('select.user_filter_select_state').on('change', function(e, p) {
            self.my_filter();
        });

        $('input.user_filter_name').on("keypress", function(e){
            if(e.which === 13) {
                self.my_filter();
            }
        });
    },
    my_filter: function(){
        var self = this;
        this.filter_domain = [];
        
        var position = $('select.user_filter_select_position').val();
        if ($.isNumeric(position)) {
            self.filter_domain.push(["position", "=", parseInt(position)]);
        }

        var quota = $('select.user_filter_select_quota').val();
        if ($.isNumeric(quota)) {
            self.filter_domain.push(["quota_id", "=", parseInt(quota)]);
        }

        var level = $('select.user_filter_select_level').val();
        if ($.isNumeric(level)) {
            self.filter_domain.push(["level", "=", parseInt(level)]);
        }

        var state = $('select.user_filter_select_state').val();
        if ($.isNumeric(state)) {
            self.filter_domain.push(["active", "=", parseInt(state) === 1 ? true : false]);
        }

        var name = $('input.user_filter_name').val();
        if (name != '') {
            self.filter_domain.push(["name", "ilike", name]);
        }

        this.do_search(self.last_domain, self.last_context, self.last_group_by);
    },
    do_search: function(domain, context, group_by) {
        this.last_domain = domain;
        this.last_context = context;
        this.last_group_by = group_by;
        this.old_search = _.bind(this._super, this);
        return this.search();
    },
    do_show: function () {
        this._super();
        $('div.v_users_list_header').show();
    },
    search: function() {
        var self = this;
        var domain = this.filter_domain;
        var compound_domain = new data.CompoundDomain(self.last_domain, domain);
        self.dataset.domain = compound_domain.eval();
        return self.old_search(compound_domain, self.last_context, self.last_group_by);
    }
});

var EvalEffectiveListView = ListView.extend({
    init: function(){
        this._super.apply(this, arguments);
        this.options.selectable = false;

        this.EvalEffectiveModel = new Model('eval.effect.ticket');
        this.ResUsersModel = new Model('res.users');
        this.base_domain = [];
        this.filter_domain = [];
    },
    willStart: function() {
        var self = this;
        var userInfoDef = this.ResUsersModel.call('getUserInfo', []).then(function(res) {
            self.user = res;
        });
        return $.when(this._super(), userInfoDef);
    },
    start: function(){
        this._super.apply(this, arguments);
        var self = this;
        var defs = [];

        var r = this.$el.parent();

        // Hide control panel
        // $('div.o_control_panel').addClass('panel_tmp_hidden');
        this.$el.addClass('evalListTable');

        // Add staff information top of view
        // this.$el.before(QWeb.render("EvalStaffInformationTemplate", {data : self.user}));
        
        // Add list view header
        var currentTime = new Date();
        this.$el.before(
            QWeb.render("EvalEffectHeadList", {
                year: currentTime.getFullYear(),
                user: self.user,
                is_ios: navigator.userAgent.match(/(iPod|iPhone|iPad)/)
            })
        );

        this.pager = new Pager(this, this.dataset.size(), 1, this._limit, null);
        this.pager.appendTo(this.$el.parent().find('span.evalPagerPlaceholder'));
        this.pager.on('pager_changed', this, function (new_state) {
            var self = this;
            var limit_changed = (this._limit !== new_state.limit);

            this._limit = new_state.limit;
            this.current_min = new_state.current_min;
            this.reload_content().then(function() {
                // Reset the scroll position to the top on page changed only
                if (!limit_changed) {
                    self.set_scrollTop(0);
                    self.trigger_up('scrollTo', {offset: 0});
                }
            });
        });

        // Reload list view after saved record
        r.find('span.effect-list-reload').click(function() {
            self.reload();
        });
        
        // Open creates form view
        r.find('a.effect-list-new').click(function(){
            self.new_draft();
        });

        // By default, if actor is director, then load staff tickets, else load self tickets
        // Director default load
        if (self.user.level == 1 || self.user.can_effect_view_all) {
            if (self.user.can_effect_view_all)
                self.base_domain = ['|', ['achieved', '=', true], ['achieved', '=', false]];
            else
                self.base_domain = [['create_uid', '=', self.user.id], ['achieved', '=', false]];
            r.find('a.effect-staff-list-btn').addClass('active');
            r.find('a.effect-self-list-btn').removeClass('active');
            r.find('a.effect-list-new').removeClass('hidden');
            r.find('.effect_filter2').removeClass('hidden');
        }
        // Staff default load
        else {
            self.base_domain = [['user', '=', self.user.id]];
        }

        // Self list view switch
        r.find('a.effect-self-list-btn').click(function(){
            self.$el.parent().find('a.effect-self-list-btn').addClass('active');
            self.$el.parent().find('a.effect-staff-list-btn').removeClass('active');
            self.$el.parent().find('a.effect-list-new').addClass('hidden');
            self.$el.parent().find('.effect_filter2').addClass('hidden');
            self.base_domain = [['user', '=', self.user.id]];
            self.do_search(self.last_domain, self.last_context, self.last_group_by);
        });

        // Staff list view switch
        r.find('a.effect-staff-list-btn').click(function(){
            self.$el.parent().find('a.effect-self-list-btn').removeClass('active');
            self.$el.parent().find('a.effect-staff-list-btn').addClass('active');
            self.$el.parent().find('a.effect-list-new').removeClass('hidden');
            self.$el.parent().find('.effect_filter2').removeClass('hidden');
            if (self.user.can_effect_view_all)
                self.base_domain = ['|', ['achieved', '=', true], ['achieved', '=', false]];
            else if (self.user.level <= 3) {
                self.base_domain = [['create_uid', '=', self.user.id], ['achieved', '=', false]];
            }
            self.do_search(self.last_domain, self.last_context, self.last_group_by);
        });

        r.find('select.effect_filter').on('change', function(){
            if ($(this).hasClass('effect-list-filter-custom') && $(this).val() == '') {
                r.find('input.effect-list-filter-text').prop('disabled', true);
                return;
            }
            self.$el.parent().find('input.effect-list-filter-text').prop('disabled', false);
            self.my_filter();
        });

        r.find('input.effect-list-filter-text').on("keypress", function(e){
            if(e.which === 13) {
                self.my_filter();
            }
        });
        
        r.find('a.effect-list-export').click(function() {
            framework.blockUI();
            self.session.get_file({
                'url': '/web/export/effect_tickets_excel_export',
                'data': {
                    data: JSON.stringify({
                        domain : self.my_filter(true),
                    })
                },
                'complete': framework.unblockUI()
            });
        });
    },
    my_filter: function(get_domain){
        var self = this;
        this.filter_domain = [];
        var r = this.$el.parent();
        var year = r.find('select.effect-list-filter-year').val();
        var month = r.find('select.effect-list-filter-month').val();
        var field = r.find('select.effect-list-filter-custom').val();
        var text = r.find('input.effect-list-filter-text').val();

        if ($.isNumeric(year)) {
            self.filter_domain.push(["year", "=", parseInt(year)]);
        }
        if ($.isNumeric(month)) {
            self.filter_domain.push(["month", "=", parseInt(month)]);
        }
        if (text != '') {
            self.filter_domain.push(["a1", "=", field]);
            self.filter_domain.push(["a2", "=", text]);
        }
        if (get_domain === true) {
            return $.merge( $.merge( [], self.base_domain ), self.filter_domain );
        }

        this.do_search(self.last_domain, self.last_context, self.last_group_by);
    },
    new_draft: function(){
        this.do_action({
            name: 'Thêm phiếu mới',
            views: [[false, 'form_eval_effective0']],
            res_model: 'eval.effect.ticket',
            type: 'ir.actions.act_window',
            target: 'new',
            res_id: false,
        });
    },
    select_record:function (index, view) {
        var self = this;
        self.ResUsersModel.call('getUserEffectTemplate', [self.dataset.ids[index]]).then(function(res) {
            if (res.leader_points !== null) {
                var leader_points = JSON.parse(res.leader_points);
                
                var leader_points2 = [];
                for (var i = 0; i < leader_points.length; i++) {
                    leader_points2[leader_points[i].id] = leader_points[i].point;
                }
                res.leader_points = leader_points2;
            }

            var formLoaded = self.do_action({
                name: 'Xem phiếu đánh giá',
                views: [[false, 'form_eval_effective']],
                res_model: 'eval.effect.ticket',
                type: 'ir.actions.act_window',
                target: 'new',
                res_id: self.dataset.ids[index],
            });

            $.when(formLoaded).then(function(){
                $('tr.evalTicketIndicatorsPlace').replaceWith(
                    QWeb.render("EvalEffectIndicators", { data: res })
                );
                $('span.evalTicketManagerTotal').text(res.leader_total);
                $('span.trial_template_total').text(res.total);
                if (res.active) {
                    $('.eval_effect_form_buttons > button').replaceWith('<button type="button" class="btn btn-primary btn-sm o_form_button_print">In phiếu</button>');
                    $('.eval_effect_form_buttons > button.o_form_button_print').on('click', function(){
                        $('.modal .modal-content .o_view_manager_content table').printElement();
                    });
                }
                if (self.user.is_admin) {
                    $('.modal-dialog .eval_effect_form_buttons').append('<button style="float:left;background-color:#f00;border-color:#f00;" data-rid="'+self.dataset.ids[index]+'" type="button" class="btn btn-primary btn-sm o_form_button_unlink" accesskey="s">Xoá</button>');
                }
            });
        });
    },
    do_search: function(domain, context, group_by) {
        this.last_domain = domain;
        this.last_context = context;
        this.last_group_by = group_by;
        this.old_search = _.bind(this._super, this);
        return this.search();
    },
    search: function() {
        var self = this;
        var domain = $.merge( $.merge( [], self.base_domain ), self.filter_domain );
        var compound_domain = new data.CompoundDomain(self.last_domain, domain);
        self.dataset.domain = compound_domain.eval();
        return self.old_search(compound_domain, self.last_context, self.last_group_by);
    },
    load_list: function() {
        var self = this;
        return $.when(this._super.apply(this, arguments)).done(function(){
            self.$el.parent().find('.evalListTable table tbody td[data-field="order"] span').each(function(i, e){
                $(this).html(i + 1);
            });
        });
    },
    do_show: function(){
        var self = this;
        return $.when(this._super.apply(this, arguments)).then(function(){
            if (!self.user.can_view_trial && !self.user.can_eval_trial) {
                function render(){
                    $('a[data-menu="'+INCORRECT_MENU_ID+'"]').remove();
                }
                function wait(){
                    setTimeout(function(){
                        if ($('a[data-menu="'+INCORRECT_MENU_ID+'"]').length) {
                            render();
                        }
                        else {
                            wait();
                        }
                    }, 50);
                }
                wait();
            }
        });
    },
    render_pager: function($node, options) {}
});

var EvalEffectiveFormDraft = FormView.extend({
    defaults: _.extend({}, View.prototype.defaults, {
        not_interactible_on_create: false,
        initial_mode: "edit",
        disable_autofocus: false,
        footer_to_buttons: true,
    }),
    start: function(){
        // $('div.o_control_panel').addClass('panel_tmp_hidden');
        this._super.apply(this, arguments);
        this.ResUsersModel = new Model('res.users');
    },
    render_buttons: function($node) {
        this.$buttons = $('<div/>');

        var $footer = this.$('footer');
        if (this.options.action_buttons !== false || this.options.footer_to_buttons && $footer.children().length === 0) {
            this.$buttons.append(QWeb.render("EvalEffectiveFormDraft.buttons", {'widget': this}));
        }
        if (this.options.footer_to_buttons) {
            $footer.appendTo(this.$buttons);
        }

        // Show or hide the buttons according to the view mode
        this.toggle_buttons();
        this.$buttons.on('click', '.o_form_button_save', this.on_button_save);
        this.$buttons.appendTo($node);
    },
    record_created: function(r, prepend_on_create) {
        var self = this;
        if ($.isNumeric(r) && r > 0) {
            framework.blockUI();
            var loaded = this.ResUsersModel.call('getUserEffectTemplate', [r]).then(function(res) {
                if (res.leader_points !== null) {
                    var leader_points = JSON.parse(res.leader_points);
                    
                    var leader_points2 = [];
                    for (var i = 0; i < leader_points.length; i++) {
                        leader_points2[leader_points[i].id] = leader_points[i].point;
                    }
                    res.leader_points = leader_points2;
                }

                var draftFormLoaded = self.do_action({
                    name: 'Thêm phiếu mới',
                    views: [[false, 'form_eval_effective']],
                    res_model: 'eval.effect.ticket',
                    type: 'ir.actions.act_window',
                    target: 'new',
                    res_id: r,
                });

                $.when(draftFormLoaded).then(function(){
                    $('tr.evalTicketIndicatorsPlace').replaceWith(
                        QWeb.render("EvalEffectIndicators", { data: res })
                    );
                    $('span.evalTicketManagerTotal').text(res.leader_total);
                    $('span.trial_template_total').text(res.total);
                });
            });

            return $.when(loaded).then(function(){
                framework.unblockUI();
            });
        }
    },
    do_show: function (options) {
        var self = this;
        $.when(this._super.apply(this, arguments)).done(function(){
            $('.modal.in').on('hide.bs.modal', function(e){
                e.preventDefault();
                e.stopImmediatePropagation();
                self.can_be_discarded().then(function() {
                    self.destroy();
                    $('div.modal.in').remove();
                    $('div.modal-backdrop.in').remove();
                    $('body').removeClass('modal-open');
                });
            });
        });
    }
});

var EvalEffectiveFormView = FormView.extend({
    defaults: _.extend({}, View.prototype.defaults, {
        not_interactible_on_create: false,
        initial_mode: "edit",
        disable_autofocus: false,
        footer_to_buttons: true,
    }),
    start: function(){
        // $('div.o_control_panel').addClass('panel_tmp_hidden');
        this._super.apply(this, arguments);
        this.EvalEffectiveModel = new Model('eval.effect.ticket');
    },
    do_show: function (options) {
        var self = this;
        $.when(this._super.apply(this, arguments)).done(function(){
            $('.modal.in').on('hide.bs.modal', function(e){
                e.preventDefault();
                e.stopImmediatePropagation();
                self.can_be_discarded().then(function() {
                    self.destroy();
                    $('div.modal.in').remove();
                    $('div.modal-backdrop.in').remove();
                    $('body').removeClass('modal-open');
                });
            });
                    
            $('table.evalEffectFormTicket input.pointInput2').on('keyup', function(){
                var numbers = $(this).val();
                $(this).val(numbers.replace(/\D/, ''));
            });
            $('table.evalEffectFormTicket input.pointInput2').on('change', function(){
                self.on_form_changed();
                var total = 0;
                $('table.evalEffectFormTicket input.pointInput2').each(function(i, e){
                    self.do_notify_change();
                    var value = parseInt($(e).val());
                    var max = parseInt($(e).attr('max'));
                    if ((Math.floor(value) != value) || !$.isNumeric(value) || value < 0 || value > max) {
                        $(e).val(0);
                        value = 0;
                    }
                    else {
                        $(e).val(value);
                    }
                    total += value;
                });
                $('span.evalTicketManagerTotal').text(total);
            });

            $('button.o_form_button_unlink').on('click', function(){
                var rid = parseInt($(this).attr('data-rid'));
                if (! $.isNumeric(rid))
                    return;
                if (confirm("Phiếu sau khi xoá không thể khôi phục được.\nChắc chắn xoá?")) {
                    self.EvalEffectiveModel.call('unlink_ticket', [rid]).then(function(res){
                        if (res === true) {
                            self.destroy();
                            $('button[data-dismiss="modal"]').click();
                            $('span[class="effect-list-reload"]').click();
                        }
                        else {
                            alert('Lỗi trong quá trình xoá phiếu. Vui lòng thử lại sau!');
                        }   
                    });
                }
            });
        });
    },
    render_buttons: function($node) {
        this.$buttons = $('<div/>');

        var $footer = this.$('footer');
        if (this.options.action_buttons !== false || this.options.footer_to_buttons && $footer.children().length === 0) {
            this.$buttons.append(QWeb.render("EvalEffectiveFormView.buttons", {'widget': this}));
        }
        if (this.options.footer_to_buttons) {
            $footer.appendTo(this.$buttons);
        }

        // Show or hide the buttons according to the view mode
        this.toggle_buttons();
        this.$buttons.on('click', '.o_form_button_save', this.on_button_save);
        this.$buttons.appendTo($node);
    },
    on_button_save: function() {
        if (!confirm("Phiếu chỉ được gửi một lần mỗi tháng cho mỗi nhân viên. Chắc chắn gửi?"))
            return;
        var self = this;
        
        var points_data = [];
        $('table.evalEffectFormTicket input.pointInput2').each(function(i, e){
            $(e).removeClass('evalInputInvalid');
            var point = {
                    'id': $(e).attr('name'),
                    'max': $(e).attr('max'),
                    'point': $(e).val(),
                    'token': $(e).attr('token')
            }
            points_data.push(point);
        });
        
        $('span.evalTicketPointsData input').val(JSON.stringify(points_data));
        this.disable_button();
        return this.save().then(function(result) {
            if (result === true) {
                self.trigger("save", result);
                return self.reload().then(function() {
                    self.to_view_mode();
                    core.bus.trigger('do_reload_needaction');
                    core.bus.trigger('form_view_saved', self);
                    
                    self.destroy();
                    $('button[data-dismiss="modal"]').click();
                    $('span[class="effect-list-reload"]').click();
                });
            }
            else if (result.error) {
                var domeEdited = $('table.evalEffectFormTicket input.pointInput2').each(function(i, e){
                    if (result.error.indexOf(parseInt($(e).attr('name'))) != -1) {
                        $(e).addClass('evalInputInvalid');
                    }
                });
                return $.when(domeEdited).then(function(){
                    self.do_warn(_t("Dữ liệu không hợp lệ"), _t("Vui lòng điền vào các ô có viền đỏ trên phiếu!"), false);
                    return;
                });
            }
            
        }).always(function(){
            self.enable_button();
        });
    },
    on_button_cancel: function() {
        var self = this;
        this.can_be_discarded().then(function() {
            if (self.get('actual_mode') === 'create') {
                self.trigger('history_back');
                self.destroy();
                $('button[data-dismiss="modal"]').click();
            } else {
                self.to_view_mode();
                $.when.apply(null, self.render_value_defs).then(function(){
                    self.trigger('load_record', self.datarecord);

                    self.destroy();
                    $('button[data-dismiss="modal"]').click();
                });
            }
        });
        this.trigger('on_button_cancel');
        return false;
    },
});

var EvalTrialListView = ListView.extend({
    init: function(){
        this._super.apply(this, arguments);
        this.options.selectable = false;

        this.EvalTrialModel = new Model('eval.trial.ticket');
        this.ResUsersModel = new Model('res.users');
        this.base_domain = [];
        this.filter_domain = [];
    },
    willStart: function() {
        var self = this;
        var userInfoDef = this.ResUsersModel.call('getUserInfo', []).then(function(res) {
            self.user = res;
        });
        return $.when(this._super(), userInfoDef);
    },
    start: function(){
        this._super.apply(this, arguments);
        var self = this;
        var defs = [];

        var r = this.$el.parent();
        if (!self.user.can_view_trial && !self.user.can_eval_trial) {
            window.location.href="/web";
            return;
        }

        // Hide control panel
        // $('div.o_control_panel').addClass('panel_tmp_hidden');
        this.$el.addClass('evalListTable');

        // Add staff information top of view
        // this.$el.before(QWeb.render("EvalStaffInformationTemplate", {data : self.user}));
        
        // Add list view header
        var currentTime = new Date();
        this.$el.before(
            QWeb.render("EvalTrialHeadList", {
                year: currentTime.getFullYear(),
                user: self.user,
                is_ios: navigator.userAgent.match(/(iPod|iPhone|iPad)/)
            })
        );

        this.pager = new Pager(this, this.dataset.size(), 1, this._limit, null);
        this.pager.appendTo(this.$el.parent().find('span.evalPagerPlaceholder'));
        this.pager.on('pager_changed', this, function (new_state) {
            var self = this;
            var limit_changed = (this._limit !== new_state.limit);

            this._limit = new_state.limit;
            this.current_min = new_state.current_min;
            this.reload_content().then(function() {
                // Reset the scroll position to the top on page changed only
                if (!limit_changed) {
                    self.set_scrollTop(0);
                    self.trigger_up('scrollTo', {offset: 0});
                }
            });
        });

        // Reload list view after saved record
        r.find('span.effect-list-reload').click(function() {
            self.reload();
        });
        
        // Open creates form view
        r.find('a.effect-list-new').click(function(){
            framework.blockUI();
            self.ResUsersModel.call('getEvalTrialTemplate', []).then(function(res){
                var trialFormLoaded = self.do_action({
                    name: 'Thêm phiếu mới',
                    views: [[false, 'form_eval_trial']],
                    res_model: 'eval.trial.ticket',
                    type: 'ir.actions.act_window',
                    target: 'new',
                    res_id: false,
                });

                $.when(trialFormLoaded).then(function(){
                    $('tr.evalTicketIndicatorsPlace').replaceWith(
                        QWeb.render("EvalTrialIndicators", { data: res })
                    );
                    $('span.evalTicketManagerTotal').text(res.leader_total);
                    
                    $('table.evalEffectFormTicket input.pointInput2').on('keyup', function(){
                        var numbers = $(this).val();
                        $(this).val(numbers.replace(/\D/, ''));
                    });
                    
                    $('table.evalEffectFormTicket input.pointInput2').on('change', function(){
                        var total = 0;
                        $('table.evalEffectFormTicket input.pointInput2').each(function(i, e){
                            var value = parseInt($(e).val());
                            var max = parseInt($(e).attr('max'));
                            if ((Math.floor(value) != value) || !$.isNumeric(value) || value < 0 || value > max) {
                                $(e).val(0);
                                value = 0;
                            }
                            else {
                                $(e).val(value);
                            }
                            total += value;
                        });
                        $('span.evalTicketManagerTotal').text(total);
                    });
                    $('span.trial_template_total').text(res.total);
                    framework.unblockUI();
                });
            });
        });

        // Staff list
        if (self.user.can_view_trial) {
            r.find('.effect-all-list-btn').removeClass('hidden');
        }
        self.base_domain = [['create_uid', '=', self.user.id]];

        // Staff list view switch
        r.find('a.effect-staff-list-btn').click(function(){
            self.$el.parent().find('a.effect-all-list-btn').removeClass('active');
            self.$el.parent().find('a.effect-staff-list-btn').addClass('active');
            self.$el.parent().find('a.effect-list-new').removeClass('hidden');
            self.$el.parent().find('.effect_filter2').removeClass('hidden');

            self.base_domain = [['create_uid', '=', self.user.id], ['achieved', '=', false]];
            self.do_search(self.last_domain, self.last_context, self.last_group_by);
        });

        // All list view switch
        r.find('a.effect-all-list-btn').click(function(){
            self.$el.parent().find('a.effect-all-list-btn').addClass('active');
            self.$el.parent().find('a.effect-staff-list-btn').removeClass('active');
            self.$el.parent().find('a.effect-list-new').addClass('hidden');
            self.$el.parent().find('.effect_filter2').addClass('hidden');

            if (self.user.can_view_trial) {
                self.base_domain = ['|', ['achieved', '=', true], ['achieved', '=', false]];
            }
            else {
                self.base_domain = [];
            }
            self.do_search(self.last_domain, self.last_context, self.last_group_by);
        });

        r.find('select.effect-list-filter-year').on('change', function(){
            self.my_filter();
        });
        r.find('input.effect-list-filter-text').on("keypress", function(e){
            if(e.which === 13) {
                self.my_filter();
            }
        });
        
        r.find('a.effect-list-export').click(function() {
            framework.blockUI();
            self.session.get_file({
                'url': '/web/export/trial_tickets_excel_export',
                'data': {
                    data: JSON.stringify({
                        domain : self.my_filter(true),
                    })
                },
                'complete': framework.unblockUI()
            });
        });
    },
    my_filter: function(get_domain){
        var self = this;
        this.filter_domain = [];
        var r = this.$el.parent();
        var year = r.find('select.effect-list-filter-year').val();
        var name = r.find('input.effect-list-filter-text').val();

        if ($.isNumeric(year)) {
            self.filter_domain.push(["year", "=", year]);
        }
        if (name != '') {
            self.filter_domain.push(["user", "ilike", name]);
        }

        if (get_domain === true) {
            return $.merge( $.merge( [], self.base_domain ), self.filter_domain );
        }

        this.do_search(self.last_domain, self.last_context, self.last_group_by);
    },
    select_record:function (index, view) {
        var self = this;
        self.ResUsersModel.call('getEvalTrialTemplate', [self.dataset.ids[index]]).then(function(res) {
            if (res.leader_points !== null) {
                var leader_points = JSON.parse(res.leader_points);
                
                var leader_points2 = [];
                for (var i = 0; i < leader_points.length; i++) {
                    leader_points2[leader_points[i].id] = leader_points[i].point;
                }
                res.leader_points = leader_points2;
            }

            var formLoaded = self.do_action({
                name: 'Xem phiếu đánh giá',
                views: [[false, 'form_eval_trial2']],
                res_model: 'eval.trial.ticket',
                type: 'ir.actions.act_window',
                target: 'new',
                res_id: self.dataset.ids[index],
            });

            $.when(formLoaded).then(function(){
                $('tr.evalTicketIndicatorsPlace').replaceWith(
                    QWeb.render("EvalTrialIndicators", { data: res })
                );
                $('span.trial_template_total').text(res.total);
                if (res.approved || !self.user.can_approve_trial) {
                    $('.eval_effect_form_buttons > button').replaceWith('<button type="button" class="btn btn-primary btn-sm o_form_button_print">In phiếu</button>');
                    $('.eval_effect_form_buttons > button.o_form_button_print').on('click', function(){
                        $('.modal .modal-content .o_view_manager_content table').printElement();
                    });
                }
                if (!self.user.can_approve_trial) {
                    $('button.eval_effect_form_buttons_approve').remove();
                }

                if (self.user.is_admin) {
                    $('.modal-dialog .eval_effect_form_buttons').append('<button style="float:left;background-color:#f00;border-color:#f00;" data-rid="'+self.dataset.ids[index]+'" type="button" class="btn btn-primary btn-sm o_form_button_unlink" accesskey="s">Xoá</button>');
                }
            });
        });
    },
    do_search: function(domain, context, group_by) {
        this.last_domain = domain;
        this.last_context = context;
        this.last_group_by = group_by;
        this.old_search = _.bind(this._super, this);
        return this.search();
    },
    search: function() {
        var self = this;
        var domain = $.merge( $.merge( [], self.base_domain ), self.filter_domain );
        var compound_domain = new data.CompoundDomain(self.last_domain, domain);
        self.dataset.domain = compound_domain.eval();
        return self.old_search(compound_domain, self.last_context, self.last_group_by);
    },
    load_list: function() {
        var self = this;
        return $.when(this._super.apply(this, arguments)).done(function(){
            self.$el.parent().find('.evalListTable table tbody td[data-field="order"] span').each(function(i, e){
                $(this).html(i + 1);
            });
        });
    },
    render_pager: function($node, options) {}
});

var EvalTrialFormView = FormView.extend({
    defaults: _.extend({}, View.prototype.defaults, {
        not_interactible_on_create: false,
        initial_mode: "edit",
        disable_autofocus: false,
        footer_to_buttons: true,
    }),
    start: function(){
        // $('div.o_control_panel').addClass('panel_tmp_hidden');
        this._super.apply(this, arguments);
    },
    do_show: function (options) {
        var self = this;
        $.when(this._super.apply(this, arguments)).done(function(){
            $('.modal.in').on('hide.bs.modal', function(e){
                e.preventDefault();
                e.stopImmediatePropagation();
                self.can_be_discarded().then(function() {
                    self.destroy();
                    $('div.modal.in').remove();
                    $('div.modal-backdrop.in').remove();
                    $('body').removeClass('modal-open');
                });
            });
            $('table.evalEffectFormTicket input.pointInput2').on('change', function(){
                self.do_notify_change();
            });
        });
    },
    render_buttons: function($node) {
        this.$buttons = $('<div/>');

        var $footer = this.$('footer');
        if (this.options.action_buttons !== false || this.options.footer_to_buttons && $footer.children().length === 0) {
            this.$buttons.append(QWeb.render("EvalTrialFormView.buttons", {'widget': this}));
        }
        if (this.options.footer_to_buttons) {
            $footer.appendTo(this.$buttons);
        }

        // Show or hide the buttons according to the view mode
        this.toggle_buttons();
        this.$buttons.on('click', '.o_form_button_save', this.on_button_save);
        this.$buttons.appendTo($node);
    },
    on_button_save: function() {
        if (!confirm("Phiếu sau khi gửi không thể xoá và sửa. Chắc chắn gửi?"))
            return;
        var self = this;
        
        var points_data = [];
        $('table.evalEffectFormTicket input.pointInput2').each(function(i, e){
            $(e).removeClass('evalInputInvalid');
            var point = {
                    'id': $(e).attr('name'),
                    'max': $(e).attr('max'),
                    'point': $(e).val(),
                    'token': $(e).attr('token')
            }
            points_data.push(point);
        });
        
        $('span.evalTicketPointsData input').val(JSON.stringify(points_data));
        this.disable_button();
        return this.save().then(function(result) {
            if (result) {
                self.trigger("save", result);
                return self.reload().then(function() {
                    self.to_view_mode();
                    core.bus.trigger('do_reload_needaction');
                    core.bus.trigger('form_view_saved', self);
                    
                    self.destroy();
                    $('button[data-dismiss="modal"]').click();
                    $('span[class="effect-list-reload"]').click();
                });
            }
        }).always(function(){
            self.enable_button();
        });
    },
    record_created: function(r, prepend_on_create) {
        var self = this;
        if (r.error) {
            $('table.evalEffectFormTicket input.pointInput2').each(function(i, e){
                if (r.error.indexOf(parseInt($(e).attr('name'))) != -1) {
                    $(e).addClass('evalInputInvalid');
                }
            });
            self.do_warn(_t("Dữ liệu không hợp lệ"), _t("Vui lòng điền vào các ô có viền đỏ trên phiếu!"), false);
            return;
        }
        return this._super.apply(this, arguments);
    },
    on_button_cancel: function() {
        var self = this;
        this.can_be_discarded().then(function() {
            if (self.get('actual_mode') === 'create') {
                self.trigger('history_back');
                
                self.destroy();
                $('button[data-dismiss="modal"]').click();
            } else {
                self.to_view_mode();
                $.when.apply(null, self.render_value_defs).then(function(){
                    self.trigger('load_record', self.datarecord);

                    self.destroy();
                    $('button[data-dismiss="modal"]').click();
                });
            }
        });
        this.trigger('on_button_cancel');
        return false;
    },
});

var EvalTrialFormView2 = FormView.extend({
    defaults: _.extend({}, View.prototype.defaults, {
        not_interactible_on_create: false,
        initial_mode: "edit",
        disable_autofocus: false,
        footer_to_buttons: true,
    }),
    start: function(){
        // $('div.o_control_panel').addClass('panel_tmp_hidden');
        this._super.apply(this, arguments);
        this.EvalTrialModel = new Model('eval.trial.ticket');
    },
    do_show: function (options) {
        var self = this;
        $.when(this._super.apply(this, arguments)).done(function(){
            $('.modal.in').on('hide.bs.modal', function(e){
                e.preventDefault();
                e.stopImmediatePropagation();
                self.can_be_discarded().then(function() {
                    self.destroy();
                    $('div.modal.in').remove();
                    $('div.modal-backdrop.in').remove();
                    $('body').removeClass('modal-open');
                });
            });

            $('button.o_form_button_unlink').on('click', function(){
                var rid = parseInt($(this).attr('data-rid'));
                if (! $.isNumeric(rid))
                    return;
                if (confirm("Phiếu sau khi xoá không thể khôi phục được.\nChắc chắn xoá?")) {
                    self.EvalTrialModel.call('unlink_ticket', [rid]).then(function(res){
                        if (res === true) {
                            self.destroy();
                            $('button[data-dismiss="modal"]').click();
                            $('span[class="effect-list-reload"]').click();
                        }
                        else {
                            alert('Lỗi trong quá trình xoá phiếu. Vui lòng thử lại sau!');
                        }   
                    });
                }
            });
        });
    },
    render_buttons: function($node) {
        this.$buttons = $('<div/>');

        var $footer = this.$('footer');
        if (this.options.action_buttons !== false || this.options.footer_to_buttons && $footer.children().length === 0) {
            var footerLoaded = this.$buttons.append(QWeb.render("EvalTrialFormView2.buttons", {'widget': this}));
            $.when(footerLoaded).then(function(){
                $('button.btn_trial_approve').on('click', function(){
                    
                })
            });
        }
        if (this.options.footer_to_buttons) {
            $footer.appendTo(this.$buttons);
        }

        // Show or hide the buttons according to the view mode
        this.toggle_buttons();
        this.$buttons.on('click', '.o_form_button_save', this.on_button_save);
        this.$buttons.appendTo($node);
    },
    on_button_save: function() {
        if (!confirm("Phiếu sau khi duyệt không thể sửa. Chắc chắn duyệt?"))
            return;
        var self = this;
        var tid = $('span.eval_trial_ticket_id span').text();
        var EvalTrialModel = new Model('eval.trial.ticket');
        var set_approved = EvalTrialModel.call('set_approve', [parseInt(tid)]).then(function(result){
            if (result) {
                self.destroy();
                $('button[data-dismiss="modal"]').click();
                $('span[class="effect-list-reload"]').click();
            }
            else {
                alert('Lỗi trong quá trình duyệt phiếu. Vui lòng thử lại sau!');
            }   
        });
    },
    on_button_cancel: function() {
        var self = this;
        this.can_be_discarded().then(function() {
            if (self.get('actual_mode') === 'create') {
                self.trigger('history_back');
                
                self.destroy();
                $('button[data-dismiss="modal"]').click();
            } else {
                self.to_view_mode();
                $.when.apply(null, self.render_value_defs).then(function(){
                    self.trigger('load_record', self.datarecord);

                    self.destroy();
                    $('button[data-dismiss="modal"]').click();
                });
            }
        });
        this.trigger('on_button_cancel');
        return false;
    },
});

var EvalIncorrectListView = ListView.extend({
    init: function(){
        this._super.apply(this, arguments);
        this.options.selectable = false;

        this.EvalIncorrectModel = new Model('eval.incorrect.ticket');
        this.ResUsersModel = new Model('res.users');
        this.filter_domain = [];
        this.last_cat_all = null;
        this.self_tab = true;
        this.cat_id = null;
    },
    willStart: function() {
        var self = this;
        var userInfoDef = this.ResUsersModel.call('getUserInfoIncorrect', []).then(function(res) {
            self.user = res;
        });
        return $.when(this._super(), userInfoDef);
    },
    start: function(){
        this._super.apply(this, arguments);
        var self = this;

        var r = this.$el.parent();

        // Hide control panel
        // $('div.o_control_panel').addClass('panel_tmp_hidden');
        this.$el.addClass('evalListTable');

        // Add staff information top of view
        // this.$el.before(QWeb.render("EvalStaffInformationTemplate", {data : self.user}));
        
        // Add list view header
        var currentTime = new Date();
        this.$el.before(
            QWeb.render("EvalIncorrectHeadList", {
                data : self.user, 
                year: currentTime.getFullYear(),
                is_ios: navigator.userAgent.match(/(iPod|iPhone|iPad)/)
            })
        );

        $('h3.incorrect_category_title span').text(self.user.cat_all[0].name.toLowerCase());
        self.last_cat_all = parseInt(self.user.cat_all[0].id);
        if (self.user.cat_available && self.user.cat_available.length > 0) {
            self.last_cat_avail = self.user.cat_available[0].id;
        }
        self.filter_domain = [ ['category_id', '=', self.last_cat_all], ['create_uid', '=', self.user.id] ];
        this.cat_id = self.last_cat_all;

        r.find('li.v_incorrect_available').on('click', function(){
            self.$el.parent().find('li.v_incorrect_available').removeClass('active');
            $(this).addClass('active');

            $('h3.incorrect_category_title span').text($(this).attr('name').toLowerCase());

            self.last_cat_avail = parseInt($(this).attr('id'));
            self.filter_domain = [ ['category_id', '=', self.last_cat_avail] ];
            self.cat_id = self.last_cat_avail;
            self.my_filter();
            return false;
        });

        r.find('li.v_incorrect_all').on('click', function(){
            self.$el.parent().find('li.v_incorrect_all').removeClass('active');
            $(this).addClass('active');
            self.last_cat_all = parseInt($(this).attr('id'));
            self.filter_domain = [ ['category_id', '=', self.last_cat_all], ['create_uid', '=', self.user.id] ];
            self.cat_id = self.last_cat_all;
            $('h3.incorrect_category_title span').text($(this).attr('name').toLowerCase());
            // self.do_search(self.last_domain, self.last_context, self.last_group_by);
            self.my_filter();
            return false;
        });

        r.find('li.v_tabble').on('click', function(){
            self.$el.parent().find('li.v_tabble').removeClass('active')
            $(this).addClass('active');
            var sub = $(this).attr('data-sub');
            $('div.v_subtab > ul').addClass('hidden');
            $('div.v_subtab > ul.'+sub).removeClass('hidden');
            var a = '';
            if ($(this).hasClass('v_self')) {
                self.self_tab = true;
                for (var i = 0; i < self.user.cat_all.length; i++) {
                    if (self.user.cat_all[i].id == self.last_cat_all) {
                        a = self.user.cat_all[i].name;
                        break;
                    }
                }
                self.filter_domain = [ ['category_id', '=', parseInt(self.last_cat_all)], ['create_uid', '=', self.user.id] ];
                self.cat_id = self.last_cat_all;
            }
            else {
                self.self_tab = false;
                for (var i = 0; i < self.user.cat_available.length; i++) {
                    if (self.user.cat_available[i].id == self.last_cat_avail) {
                        a = self.user.cat_available[i].name;
                        break;
                    }
                }
                self.filter_domain = [ ['category_id', '=', parseInt(self.last_cat_avail)]];
                self.cat_id = self.last_cat_avail;
            }
            $('h3.incorrect_category_title span').text(a.toLowerCase());
            self.my_filter();
            return false;
        });

        this.pager = new Pager(this, this.dataset.size(), 1, this._limit, null);
        this.pager.appendTo(this.$el.parent().find('span.evalPagerPlaceholder'));
        this.pager.on('pager_changed', this, function (new_state) {
            var self = this;
            var limit_changed = (this._limit !== new_state.limit);

            this._limit = new_state.limit;
            this.current_min = new_state.current_min;
            this.reload_content().then(function() {
                // Reset the scroll position to the top on page changed only
                if (!limit_changed) {
                    self.set_scrollTop(0);
                    self.trigger_up('scrollTo', {offset: 0});
                }
            });
        });

        // Reload list view after saved record
        r.find('span.effect-list-reload').click(function() { self.reload(); });
        
        // Open creates form view
        r.find('a.effect-list-new').on('click', function(){
            framework.blockUI();
            var formLoaded = self.do_action({
                name: 'Thêm phiếu mới',
                views: [[false, 'form_eval_incorrect']],
                res_model: 'eval.incorrect.ticket',
                type: 'ir.actions.act_window',
                target: 'new',
                res_id: false,
                context: {'cat_id': self.cat_id}
            });

            $.when(formLoaded).then(function(){
                framework.unblockUI();
            });
        });

        new Model('eval.incorrect.loc').call('name_search', []).then(function(res){
            var htm = '<option selected="selected" value="">Nơi phát hiện KPH</option>';
            for (var i = 0; i < res.length; i++) {
                htm += '<option value="'+ res[i][0] +'">'+ res[i][1] +'</option>';
            }
            r.find('select.effect-list-filter-loc').html(htm);
        });

        r.find('select.effect_filter').on('change', function(){
            if ($(this).hasClass('effect-list-filter-custom') && $(this).val() == '') {
                r.find('input.effect-list-filter-text').prop('disabled', true);
                return;
            }
            self.$el.parent().find('input.effect-list-filter-text').prop('disabled', false);
            self.my_filter();
        });

        r.find('input.effect-list-filter-text').on("keypress", function(e){
            if(e.which === 13) {
                self.my_filter();
            }
        });
        
        r.find('a.effect-list-export').click(function() {
            framework.blockUI();
            self.session.get_file({
                'url': '/web/export/incorrect_tickets_excel_export',
                'data': {
                    data: JSON.stringify({
                        domain : self.my_filter(true),
                    })
                },
                'complete': framework.unblockUI()
            });
            return false;
        });
    },
    do_show: function(){
        var self = this;
        return $.when(this._super.apply(this, arguments)).then(function(){
            if (!self.user.can_view_trial && !self.user.can_eval_trial) {
                function render(){
                    $('a[data-menu="'+INCORRECT_MENU_ID+'"]').remove();
                }
                function wait(){
                    setTimeout(function(){
                        if ($('a[data-menu="'+INCORRECT_MENU_ID+'"]').length) {
                            render();
                        }
                        else {
                            wait();
                        }
                    }, 50);
                }
                wait();
            }
        });
    },
    clean_filter: function(attrs) {
        var count = 0;
        for (var i=0; i < this.filter_domain.length; i++) {
            if (count == attrs.length)
                break;
            if (attrs.indexOf(this.filter_domain[i][0]) !== -1) {
                this.filter_domain.splice(i, 1);
                i--;
                count++;
            }
        }
    },
    my_filter: function(get_domain){
        var self = this;
        // this.filter_domain = [];
        var r = this.$el.parent();
        var year = parseInt(r.find('select.effect-list-filter-year').val());
        var month = parseInt(r.find('select.effect-list-filter-month').val());
        var loc = parseInt(r.find('select.effect-list-filter-loc').val());
        var field = r.find('select.effect-list-filter-custom').val();
        var text = r.find('input.effect-list-filter-text').val();

        self.clean_filter(['year', 'month', 'location_id', 'a1', 'a2']);
        if ($.isNumeric(year)) {
            self.filter_domain.push(["year", "=", year]);
        }
        if ($.isNumeric(month)) {
            self.filter_domain.push(["month", "=", month]);
        }
        if ($.isNumeric(loc)) {
            self.filter_domain.push(["location_id", "=", loc]);
        }
        if (text != '') {
            self.filter_domain.push(["a1", "=", field]);
            self.filter_domain.push(["a2", "=", text]);
        }

        if (get_domain === true) {
            if (self.self_tab) {
                self.filter_domain.push(["create_uid", "=", self.user.id]);
            }
            return self.filter_domain;
        }

        this.do_search(self.last_domain, self.last_context, self.last_group_by);
    },
    select_record:function (index, view) {
        var self = this;
        var formLoaded = self.do_action({
            name: 'Xem phiếu báo cáo KPH',
            views: [[false, 'form_eval_incorrect']],
            res_model: 'eval.incorrect.ticket',
            type: 'ir.actions.act_window',
            target: 'new',
            res_id: self.dataset.ids[index],
        });

        $.when(formLoaded).then(function(){
            $('div.eval_effect_form_buttons > button').remove();
            
            if (self.user.is_admin) {
                $('.modal-dialog .eval_effect_form_buttons').append('<button style="float:left;background-color:#f00;border-color:#f00;" data-rid="'+self.dataset.ids[index]+'" type="button" class="btn btn-primary btn-sm o_form_button_unlink" accesskey="s">Xoá</button>');
            }
            framework.unblockUI();
        });
    },
    do_search: function(domain, context, group_by) {
        if (self.self_tab) {
            self.filter_domain.push(["create_uid", "=", self.user.id]);
        }
        this.last_domain = domain;
        this.last_context = context;
        this.last_group_by = group_by;
        this.old_search = _.bind(this._super, this);
        return this.search();
    },
    search: function() {
        var self = this;
        var domain = self.filter_domain;
        var compound_domain = new data.CompoundDomain(self.last_domain, domain);
        self.dataset.domain = compound_domain.eval();
        return self.old_search(compound_domain, self.last_context, self.last_group_by);
    },
    load_list: function() {
        var self = this;
        return $.when(this._super.apply(this, arguments)).done(function(){
            self.$el.parent().find('.evalListTable table tbody td[data-field="order"] span').each(function(i, e){
                $(this).html(i + 1);
            });
        });
    },
    render_pager: function($node, options) {}
});

var EvalIncorrectFormView = FormView.extend({
    defaults: _.extend({}, View.prototype.defaults, {
        not_interactible_on_create: false,
        initial_mode: "edit",
        disable_autofocus: false,
        footer_to_buttons: true,
    }),
    start: function(){
        // $('div.o_control_panel').addClass('panel_tmp_hidden');
        this._super.apply(this, arguments);
    },
    do_show: function (options) {
        var self = this;
        $.when(this._super.apply(this, arguments)).done(function(){
            $('.modal.in').on('hide.bs.modal', function(e){
                e.preventDefault();
                e.stopImmediatePropagation();
                self.can_be_discarded().then(function() {
                    self.destroy();
                    $('div.modal.in').remove();
                    $('div.modal-backdrop.in').remove();
                    $('body').removeClass('modal-open');
                });
            });

            $('button.o_form_button_unlink').on('click', function(){
                var rid = parseInt($(this).attr('data-rid'));
                if (! $.isNumeric(rid))
                    return;
                if (confirm("Phiếu sau khi xoá không thể khôi phục được.\nChắc chắn xoá?")) {
                    new Model('eval.incorrect.ticket').call('unlink_ticket', [rid]).then(function(res){
                        if (res === true) {
                            self.destroy();
                            $('button[data-dismiss="modal"]').click();
                            $('span[class="effect-list-reload"]').click();
                        }
                        else {
                            alert('Lỗi trong quá trình xoá phiếu. Vui lòng thử lại sau!');
                        }   
                    });
                }
            });
        });
    },
    render_buttons: function($node) {
        this.$buttons = $('<div/>');

        var $footer = this.$('footer');
        if (this.options.action_buttons !== false || this.options.footer_to_buttons && $footer.children().length === 0) {
            this.$buttons.append(QWeb.render("EvalTrialFormView.buttons", {'widget': this}));
        }
        if (this.options.footer_to_buttons) {
            $footer.appendTo(this.$buttons);
        }

        // Show or hide the buttons according to the view mode
        this.toggle_buttons();
        this.$buttons.on('click', '.o_form_button_save', this.on_button_save);
        this.$buttons.appendTo($node);
    },
    on_button_save: function() {
        if (!confirm("Phiếu đã gửi không thể sửa và xoá. Chắc chắn gửi?"))
            return;
        var self = this;
        
        var points_data = [];
        $('table.evalEffectFormTicket input.pointInput2').each(function(i, e){
            $(e).removeClass('evalInputInvalid');
            var point = {
                    'id': $(e).attr('name'),
                    'max': $(e).attr('max'),
                    'point': $(e).val(),
                    'token': $(e).attr('token')
            }
            points_data.push(point);
        });
        
        $('span.evalTicketPointsData input').val(JSON.stringify(points_data));
        this.disable_button();
        return this.save().then(function(result) {
            if (result) {
                self.trigger("save", result);
                return self.reload().then(function() {
                    self.to_view_mode();
                    core.bus.trigger('do_reload_needaction');
                    core.bus.trigger('form_view_saved', self);
                    
                    self.destroy();
                    $('button[data-dismiss="modal"]').click();
                    $('span[class="effect-list-reload"]').click();
                });
            }
            
        }).always(function(){
            self.enable_button();
        });
    },
    record_created: function(r, prepend_on_create) {
        var self = this;
        if (r.error) {
            $('table.evalEffectFormTicket input.pointInput2').each(function(i, e){
                if (r.error.indexOf($(e).attr('name')) != -1) {
                    $(e).addClass('evalInputInvalid');
                }
            });
            this.do_warn(_t("Dữ liệu không hợp lệ"), _t("Vui lòng điền vào các ô có viền đỏ trên phiếu!"), false);
            return;
        }
        return this._super.apply(this, arguments);
    },
    on_button_cancel: function() {
        var self = this;
        this.can_be_discarded().then(function() {
            if (self.get('actual_mode') === 'create') {
                self.trigger('history_back');
                
                self.destroy();
                $('button[data-dismiss="modal"]').click();
            } else {
                self.to_view_mode();
                $.when.apply(null, self.render_value_defs).then(function(){
                    self.trigger('load_record', self.datarecord);

                    self.destroy();
                    $('button[data-dismiss="modal"]').click();
                });
            }
        });
        this.trigger('on_button_cancel');
        return false;
    },
});

var EvalFilesListView = ListView.extend({
    init: function(){
        this._super.apply(this, arguments);
        this.options.selectable = false;
        this.ResUsersModel = new Model('res.users');
        this.filter_domain = [];
        this.folder_id = null;
    },
    willStart: function() {
        var self = this;
        var userInfoDef = this.ResUsersModel.call('getUserInfoFiles', []).then(function(res) {
            self.user = res;
        });
        return $.when(this._super(), userInfoDef);
    },
    start: function(){
        this._super.apply(this, arguments);
        var self = this;
        var defs = [];

        var r = this.$el.parent();

        // Hide control panel
        // $('div.o_control_panel').addClass('panel_tmp_hidden');
        this.$el.addClass('evalListTable');

        // Add staff information top of view
        // this.$el.before(QWeb.render("EvalStaffInformationTemplate", {data : self.user}));
        
        // Add list view header
        var currentTime = new Date();
        this.$el.before(
            QWeb.render("EvalFilesHeadList", {
                user: self.user,
            })
        );

        this.pager = new Pager(this, this.dataset.size(), 1, this._limit, null);
        this.pager.appendTo(this.$el.parent().find('span.evalPagerPlaceholder'));
        this.pager.on('pager_changed', this, function (new_state) {
            var self = this;
            var limit_changed = (this._limit !== new_state.limit);
            this._limit = new_state.limit;
            this.current_min = new_state.current_min;
            this.reload_content().then(function() {
                // Reset the scroll position to the top on page changed only
                if (!limit_changed) {
                    self.set_scrollTop(0);
                    self.trigger_up('scrollTo', {offset: 0});
                }
            });
        });

        // Reload list view after saved record
        r.find('span.effect-list-reload').click(function() {
            self.reload();
        });

        this.filter_domain = [ ['folder_id', '=', parseInt(this.user.folders[0].id)] ];
        this.folder_id = parseInt(this.user.folders[0].id);

        r.find('li.v_incorrect_all').on('click', function(){
            self.$el.parent().find('li.v_incorrect_all').removeClass('active');
            $(this).addClass('active');

            self.folder_id = parseInt( $(this).attr('id') );
            
            self.filter_domain = [ ['folder_id', '=', self.folder_id ] ];
            $('h3.incorrect_category_title span').text($(this).attr('name').toLowerCase());
            self.do_search(self.last_domain, self.last_context, self.last_group_by);
            return false;
        });
        
        // Open creates form view
        r.find('a.effect-list-new').click(function(){
            framework.blockUI();
            var formLoaded = self.do_action({
                name: 'Thêm tài liệu',
                views: [[false, 'form_eval_file']],
                res_model: 'eval.files',
                type: 'ir.actions.act_window',
                target: 'new',
                res_id: false,
                context: {'folder_id': self.folder_id}
            });

            $.when(formLoaded).then(function(){
                framework.unblockUI();
            });
        });
    },
    do_show: function(){
        var self = this;
        return $.when(this._super.apply(this, arguments)).then(function(){
            if (!self.user.can_view_trial && !self.user.can_eval_trial) {
                function render(){
                    $('a[data-menu="'+INCORRECT_MENU_ID+'"]').remove();
                }
                function wait(){
                    setTimeout(function(){
                        if ($('a[data-menu="'+INCORRECT_MENU_ID+'"]').length) {
                            render();
                        }
                        else {
                            wait();
                        }
                    }, 50);
                }
                wait();
            }
        });
    },
    do_search: function(domain, context, group_by) {
        this.last_domain = domain;
        this.last_context = context;
        this.last_group_by = group_by;
        this.old_search = _.bind(this._super, this);
        return this.search();
    },
    search: function() {
        var self = this;
        var domain = self.filter_domain;
        var compound_domain = new data.CompoundDomain(self.last_domain, domain);
        self.dataset.domain = compound_domain.eval();
        return self.old_search(compound_domain, self.last_context, self.last_group_by);
    },
    load_list: function() {
        var self = this;
        return $.when(this._super.apply(this, arguments)).done(function(){
            self.$el.parent().find('.evalListTable table tbody td[data-field="order"] span').each(function(i, e){
                $(this).html(i + 1);
            });
        });
    },
    render_pager: function($node, options) {}
});

var EvalFilesFormView = FormView.extend({
    defaults: _.extend({}, View.prototype.defaults, {
        not_interactible_on_create: false,
        initial_mode: "edit",
        disable_autofocus: false,
        footer_to_buttons: true,
    }),
    // start: function(){
    //     // $('div.o_control_panel').addClass('panel_tmp_hidden');
    //     this._super.apply(this, arguments);
    // },
    do_show: function (options) {
        var self = this;
        $.when(this._super.apply(this, arguments)).done(function(){
            $('.modal.in').on('hide.bs.modal', function(e){
                e.preventDefault();
                e.stopImmediatePropagation();
                self.can_be_discarded().then(function() {
                    self.destroy();
                    $('div.modal.in').remove();
                    $('div.modal-backdrop.in').remove();
                    $('body').removeClass('modal-open');
                    $('.evalListTable button[disabled="disabled"]').removeAttr('disabled');
                });
            });
        });
    },
    render_buttons: function($node) {
        this.$buttons = $('<div/>');

        var $footer = this.$('footer');
        if (this.options.action_buttons !== false || this.options.footer_to_buttons && $footer.children().length === 0) {
            this.$buttons.append(QWeb.render("EvalFilesFormView.buttons", {'widget': this}));
        }
        if (this.options.footer_to_buttons) {
            $footer.appendTo(this.$buttons);
        }

        // Show or hide the buttons according to the view mode
        this.toggle_buttons();
        this.$buttons.on('click', '.o_form_button_save', this.on_button_save);
        this.$buttons.appendTo($node);
    },
    on_button_save: function() {
        var self = this;
        this.disable_button();
        return this.save().then(function(result) {
            if (result) {
                self.trigger("save", result);
                return self.reload().then(function() {
                    self.to_view_mode();
                    core.bus.trigger('do_reload_needaction');
                    core.bus.trigger('form_view_saved', self);
                    
                    self.destroy();
                    $('button[data-dismiss="modal"]').click();
                    $('span[class="effect-list-reload"]').click();
                });
            }
        }).always(function(){
            self.enable_button();
        });
    },
});

var FieldCharRaw = form_widgets.FieldChar.extend({
    render_value: function() {
        var show_value = this.get('value');
        if (this.$input) {
            this.$input.val(show_value);
        } else {
            this.$el.html(show_value);
        }
    }
});

var ColumnShortText = ListView.Column.extend({
    _format: function (row_data, options) {
        return _.str.sprintf(
            '<span class="o_list_shorttext">%s</span>',
            row_data[this.id].value || ''
        );
    }
});

var ColumnLinkUrl = ListView.Column.extend({
    _format: function (row_data, options) {
        return _.str.sprintf(
            '<span class="o_list_shorttext"><a href="%s" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></span>',
            row_data[this.id].value || '', row_data[this.id].value || ''
        );
    }
});

var ColumnRawCell = ListView.Column.extend({
    _format: function (row_data, options) {
        return _.str.sprintf(
            '<span class="o_list_rawcell">%s</span>',
            row_data[this.id].value || ''
        );
    }
});

var FieldTextarea = core.form_widget_registry.get("text").extend({
    template: 'FieldTextarea',
    render_value: function() {
        if (this.get("effective_readonly")) {
            var txt = this.get("value") || '';
            this.$el.text(txt);
        } else {
            var show_value = formats.format_value(this.get('value'), this, '');
            this.$el.val(show_value);
        }
    }
});

var MyImage = core.form_widget_registry.get("image").extend({
    render_value: function() {
        var self = this;
        this._super.apply(this, arguments);
        if(this.get('value')) {
            var image = this.$el.find('img[name="' + this.name + '"]');
            var src = $(image).attr('src');
            $(image).css('cursor', 'pointer');
            $(image).on('click', function(){
                new Dialog(this, {
                    size: 'small',
                    title: self.string,
                    subtitle: '',
                    $content: $('<div>').html(QWeb.render('PreviewableImageWidget', {src: src})),
                    buttons: [{text: "Đóng", close: true}]
                }).open();
            });
        }
        // else if (this.get("actual_mode") === "view") {
        //     this.$el.remove();
        // }
    }
});

core.view_registry.add('tree_users', UsersListView);
// core.view_registry.add('form_users', UsersFormView);

core.view_registry.add('tree_eval_effective', EvalEffectiveListView);
core.view_registry.add('form_eval_effective0', EvalEffectiveFormDraft);
core.view_registry.add('form_eval_effective', EvalEffectiveFormView);

core.view_registry.add('tree_eval_trial', EvalTrialListView);
core.view_registry.add('form_eval_trial', EvalTrialFormView);
core.view_registry.add('form_eval_trial2', EvalTrialFormView2);

core.view_registry.add('tree_eval_incorrect', EvalIncorrectListView);
core.view_registry.add('form_eval_incorrect', EvalIncorrectFormView);

core.view_registry.add('tree_eval_file', EvalFilesListView);
core.view_registry.add('form_eval_file', EvalFilesFormView);

list_widget_registry.add('field.rawcell', ColumnRawCell);
list_widget_registry.add('field.shorttext', ColumnShortText);
list_widget_registry.add('field.link_url', ColumnLinkUrl);
core.form_widget_registry.add('raw_char', FieldCharRaw);
core.form_widget_registry.add('textarea', FieldTextarea);
core.form_widget_registry.add('my_image', MyImage);

});
