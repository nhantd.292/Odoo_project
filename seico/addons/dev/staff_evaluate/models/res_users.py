# -*- coding: utf-8 -*-
from odoo import fields, models, api
from odoo.exceptions import Warning

class UserMovement(models.Model):
	_name = 'user.movement'
	_order = 'create_date desc'
	
	user_id = fields.Many2one('res.users', u'Nhân sự', readonly=True)
	user_code = fields.Char(u'Mã nhân sự')
	position_b = fields.Many2one('eval.position', string=u'Chức vụ trước', readonly=True)
	position_a = fields.Many2one('eval.position', string=u'Chức vụ mới', readonly=True)
	quota_b = fields.Many2one('eval.quota', string=u'Ngạch trước', readonly=True)
	quota_b_number = fields.Float(u'Bậc trước', readonly=True)
	date = fields.Date(u'Ngày chuyển')
	company_b = fields.Many2one('res.company', u'Công ty trước')
	company_a = fields.Many2one('res.company', u'Công ty mới')


class ResUsers(models.Model):
	_inherit = 'res.users'
	
	position = fields.Many2one('eval.position', string=u'Chức vụ')
	code = fields.Char(u'Mã nhân viên')
	number = fields.Integer(u'Bậc')
	quota_id = fields.Many2one('eval.quota', related='position.quota_id', store=True)
	level = fields.Selection(related='position.quota_id.level', store=True)
	movement = fields.One2many('user.movement', 'user_id', u'Lịch sử thay đổi', readonly=True)
	
	@api.model
	def set_data_root_user(self):
		root = self.browse(1)
		root.update({
			'position': self.env.ref('staff_evaluate.eval_position_1').id,
			'groups_id': [(4, self.env.ref('base.group_system').id), (4, self.env.ref('staff_evaluate.group_seico_admin').id)]
		})
	
	@api.onchange('company_id', 'company_ids')
	def onchange_company(self):
		current_company = self.env['res.company'].browse(self.company_id.id)
		if current_company:
			idx = [current_company.id] + self.company_ids.ids
			self.company_ids = [(6, 0, idx)]
		
	@api.onchange('position')
	def onchange_position(self):
		level = int(self.quota_id.level)
		if level < 4:
			self.groups_id = [(6, 0, [self.env.ref('staff_evaluate.group_seico_manager').id])]
		else:
			self.groups_id = [(6, 0, [self.env.ref('staff_evaluate.group_seico_staff').id])]
	
	@api.multi
	def write(self, vals):
		self.ensure_one()
		if ('position' in vals and vals['position'] != self.position.id) or \
			('number' in vals) or\
			('company_id' in vals):
			data = {
				'user_id': 			self.id,
				'user_code': 		self.code,
				'position_b': 		self.position.id,
				'position_a': 		self.position.id if 'position' not in vals else vals['position'],
				'quota_b': 			self.quota_id.id,
				'quota_b_number': 	self.number,
				'company_b': 		self.company_id.id,
				'company_a': 		self.company_id.id if 'company_id' not in vals else vals['company_id']
			}
			self.env['eval.effect.ticket'].sudo().search([ ('create_uid', '=', self.id), ('achieved', '=', False) ]).update({'achieved':True})
			if (self.env['user.movement'].sudo().create(data)):
				return super(ResUsers, self).write(vals)
			else:
				raise Warning(u"Lỗi cập nhật thông tin tài khoản!")
		else:
			return super(ResUsers, self).write(vals)
	
	# @api.multi
	# def read(self, fields=None, load='_classic_read'):
	# 	# if 'action_id' in fields and len(fields) == 1:
	# 	# 	action = self.env.ref('staff_evaluate.action_eval_effect')
	# 	# 	res = [ {'id': self._uid, 'action_id': (action.id, action.name) }]
	# 	# 	return res
	# 	# else:
	# 		return super(ResUsers, self).read(fields, load)
	
	@api.model
	def name_search(self, name='', args=None, operator='ilike', limit=100):
		context = self._context
		if not context.get('search_staff'):
			return super(ResUsers, self).name_search(name, args, operator=operator, limit=limit)
		else:
			actor = self.browse(self._uid)
			users = None
			level = int(actor.level)
			if actor.quota_id.can_effect_cross:
				quota_group = self.env['eval.quota.group'].browse(actor.quota_id.effect_group_ids.ids)
				idx = quota_group.ids
				idx.append(actor.quota_id.group.ids)
				quotas = self.env['eval.quota'].search([('group', 'in', (idx))])
				if actor.quota_id.can_effect_all:
					users = self.search([ ('name', 'ilike', name), ('active', '=', True), ('quota_id', 'in', (quotas.ids)), '|', ('level', '!=', '1'), ('level', '=', None), ('id', '!=', self._uid) ])
				else:
					users = self.search([ ('name', 'ilike', name), ('active', '=', True), ('quota_id', 'in', (quotas.ids)), ('id', '!=', self._uid), '|', ('level', '=', str(level + 1)), ('level', '=', str(level + 2)) ])
			elif level < 4:
				quotas = self.env['eval.quota'].search([('group', 'in', (actor.quota_id.group.ids))])
				if actor.quota_id.can_effect_all:
					users = self.search([ ('name', 'ilike', name), ('active', '=', True), ('quota_id', 'in', (quotas.ids)), '|', ('level', '!=', '1'), ('level', '=', None), ('id', '!=', self._uid) ])
				else:
					users = self.search([ ('name', 'ilike', name), ('active', '=', True), ('quota_id', 'in', (quotas.ids)), ('id', '!=', self._uid), '|', ('level', '=', str(level + 1)), ('level', '=', str(level + 2)) ])
			if users:
				res = []
				for user in users:
					res.append(( user.id, "%s" % (user.name) ))
				return res
			else:
				return False
	
	@api.model
	def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
		context = self._context
		if context.get('search_staff'):
			actor = self.browse(self._uid)
			level = int(actor.level)
			if actor.quota_id.can_effect_cross:
				quota_group = self.env['eval.quota.group'].browse(actor.quota_id.effect_group_ids.ids)
				idx = quota_group.ids
				idx.append(actor.quota_id.group.ids)
				quotas = self.env['eval.quota'].search([('group', 'in', (idx))])
				if actor.quota_id.can_effect_all:
					domain += [ ('active', '=', True), ('quota_id', 'in', (quotas.ids)), '|', ('level', '!=', '1'), ('level', '=', None), ('id', '!=', self._uid) ]
				else:
					domain += [ ('active', '=', True), ('quota_id', 'in', (quotas.ids)), ('id', '!=', self._uid), '|', ('level', '=', str(level + 1)), ('level', '=', str(level + 2)) ]
			elif level < 4:
				quotas = self.env['eval.quota'].search([('group', 'in', (actor.quota_id.group.ids))])
				if actor.quota_id.can_effect_all:
					domain += [ ('active', '=', True), ('quota_id', 'in', (quotas.ids)), '|', ('level', '!=', '1'), ('level', '=', None), ('id', '!=', self._uid) ]
				else:
					domain += [ ('active', '=', True), ('quota_id', 'in', (quotas.ids)), ('id', '!=', self._uid), '|', ('level', '=', str(level + 1)), ('level', '=', str(level + 2)) ]
		
		return super(ResUsers, self).search_read(domain, fields, offset, limit, order)

	@api.multi
	def name_get(self):
		self = self.sudo()
		return super(ResUsers, self).name_get()
	
	@api.model
	def getUserEffectTemplate(self, idx=None):
		def _getIndicators(indicators):
			res = []
			for indicator in indicators:
				tmp = {
					'id': indicator.id,
					'name': indicator.name,
					'content': indicator.content,
					'point': indicator.point,
					'note': indicator.note or '',
					'token': indicator.token or ''
				}
				if len(indicator.indicator_ids):
					tmp.update({'childs': _getIndicators(indicator.indicator_ids)})
				res.append(tmp)
			return res
		ticket = self.env['eval.effect.ticket'].sudo().browse(idx)
		ret = {}
		template = ticket.template_id
		user = ticket.create_uid
		ret.update({
			'user_name': user.name,
			'leader_points': ticket.leader_points,
			'leader_total': ticket.leader_total or '',
			'active': ticket.active,
			'id': idx })
		
		total = 0
		for indicatx in template.indicator_ids:
			total += indicatx.point
		ret.update({
			'indicators': _getIndicators(template.indicator_ids),
			'total': total
		})
		return ret
	
	@api.model
	def getEvalTrialTemplate(self, idx=None):
		def _getIndicators(indicators):
			res = []
			for indicator in indicators:
				tmp = {
					'id': indicator.id,
					'name': indicator.name,
					'content': indicator.content,
					'point': indicator.point,
					'note': indicator.note or '',
					'token': indicator.token or ''
				}
				if len(indicator.indicator_ids):
					tmp.update({'childs': _getIndicators(indicator.indicator_ids)})
				res.append(tmp)
			return res
		ticket = self.env['eval.trial.ticket'].sudo().browse(idx)
		if ticket:
			template = ticket.template_id
			ret = {
				'leader_points': ticket.leader_points,
				'leader_total': ticket.leader_total or '',
				'approved': ticket.manager_approved,
				'id': idx
			}
		else:
			template = self.env['eval.trial.template'].search([ ('using', '=', True) ], limit=1)
			ret = {
				'leader_points': None,
				'leader_total': 0,
				'approved': False,
				'id': False
			}
		total = 0
		for indicatx in template.indicator_ids:
			total += indicatx.point
		ret.update({
			'indicators': _getIndicators(template.indicator_ids),
			'total': total
		})
		return ret
	
	@api.model
	def getUserInfoIncorrect(self):
		actor = self.env['res.users'].sudo().browse(self.env.uid)
		admin_gid = self.env.ref('staff_evaluate.group_seico_admin').id
		# companies = [x.name for x in actor.company_ids if x.id != actor.company_id.id]
		cat_all = self.env['eval.incorrect'].sudo().search_read([], ['id', 'name'])
		cat_available = []
		for incorrect in actor.quota_id.incorrect_ids:
			cat_available.append({'id': incorrect.id, 'name': incorrect.name})
		res = {
			# 'id': actor.id,
			# 'name': actor.name,
			# 'position': actor.position.name,
			# 'quota': actor.quota_id.code,
			# 'number': actor.number,
			# 'company': actor.company_id.name,
			# 'companies': companies,
			'cat_all': cat_all,
			'cat_available': cat_available,
			'is_admin': admin_gid in actor.groups_id.ids,
			'can_view_trial': actor.quota_id.can_view_trial,
			'can_eval_trial': actor.quota_id.can_eval_trial,
			'can_approve_trial': actor.quota_id.can_approve_trial,
		}
		return res
	
	@api.model
	def getUserInfo(self):
		user = self.env['res.users'].sudo().browse(self.env.uid)
		admin_gid = self.env.ref('staff_evaluate.group_seico_admin').id
		# companies = [x.name for x in user.company_ids if x.id != user.company_id.id]
		res = {
			# 'id': user.id,
			# 'name': user.name,
			# 'position': user.position.name,
			# 'quota': user.quota_id.code,
			# 'number': user.number,
			# 'company': user.company_id.name,
			# 'companies': companies,
			'level': int(user.level),
			'can_effect_cross': user.quota_id.can_effect_cross,
			'can_effect_all': user.quota_id.can_effect_all,
			'can_effect_view_all': user.quota_id.can_effect_view_all,
			'can_eval_trial': user.quota_id.can_eval_trial,
			'can_view_trial': user.quota_id.can_view_trial,
			'can_approve_trial': user.quota_id.can_approve_trial,
			'is_admin': admin_gid in user.groups_id.ids,
		}
		return res
	
	@api.model
	def getUserInfoFiles(self):
		user = self.env['res.users'].sudo().browse(self.env.uid)
		# companies = [x.name for x in user.company_ids if x.id != user.company_id.id]
		folders = self.env['eval.folders'].sudo().search_read([], ['id', 'name'])
		res = {
			# 'id': user.id,
			# 'name': user.name,
			# 'position': user.position.name,
			# 'quota': user.quota_id.code,
			# 'number': user.number,
			# 'company': user.company_id.name,
			# 'companies': companies,
			'level': int(user.level),
			'can_edit_files': user.quota_id.can_edit_files,
			'can_eval_trial': user.quota_id.can_eval_trial,
			'can_view_trial': user.quota_id.can_view_trial,
			'folders': folders
		}
		return res