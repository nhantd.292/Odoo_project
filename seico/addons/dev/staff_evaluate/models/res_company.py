# -*- coding: utf-8 -*-

from odoo import models, api

class Company(models.Model):
	_inherit = "res.company"
	
	@api.model
	def name_search(self, name='', args=None, operator='ilike', limit=100):
		sudox = self._context.get('sudox', False)
		if sudox:
			res = self.sudo().search(args, limit=limit)
		else:
			res = self.search(args, limit=limit)
		return res.name_get()
	
	@api.model
	def edit_main_company_info(self):
		main_company = self.env.ref('base.main_company')
		vi_currency_id = self.env.ref('base.VND').id
		main_company.update({
			'name': 'SEICO JSC',
			'currency_id': vi_currency_id
		})
