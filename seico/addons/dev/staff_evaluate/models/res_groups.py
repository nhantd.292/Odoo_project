# -*- coding: utf-8 -*-
from odoo import models, api
import os
from using_group_ids import USING_GROUP_IDS

class ResGroups(models.Model):
	_inherit = 'res.groups'
	
	@api.model
	def name_search(self, name, args=None, operator='ilike', limit=100):
		res = super(ResGroups, self).sudo().search_read(args, ['name'])
		res[:] = ((d.get('id'), d.get('name')) for d in res if d.get('id') in USING_GROUP_IDS)
		return res
	
	@api.model
	def _set_using_group_ids(self):
		path = os.path.abspath(__file__ + "/../../static/src/js/data.js")
		path2 = os.path.abspath(__file__ + "/../../models/using_group_ids.py")
		path3 = os.path.abspath(__file__ + "/../../static/src/js/data_incorrect_menu.js")
		record = [
			'group_seico_user',
			'group_seico_staff',
			'group_seico_manager',
			'group_seico_admin',
		]
		ids = ''
		for r in record:
			ids += str(self.env.ref('staff_evaluate.%s' % (r)).id) + ','
		with open(path, 'w') as f:
			f.write('var USING_GROUP_IDS = [%s];' %(ids[:-1]))
		with open(path2, 'w') as f:
			f.write('# -*- coding: utf-8 -*-\nUSING_GROUP_IDS = [%s];' %(ids[:-1]))
		mid = str(self.env.ref('staff_evaluate.menu_action_eval_trial').id)
		with open(path3, 'w') as f:
			f.write('var INCORRECT_MENU_ID = %s;' %(mid))
