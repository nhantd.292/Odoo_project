# -*- coding: utf-8 -*-
from odoo import models, fields
from odoo.addons.base.ir.ir_actions import VIEW_TYPES

class IrActionsActWindowView(models.Model):
    _inherit = 'ir.actions.act_window.view'
    VIEW_TYPES.append(('tree_eval_effective', 'Tree'))
    VIEW_TYPES.append(('form_eval_effective0', 'Form'))
    VIEW_TYPES.append(('form_eval_effective', 'Form'))
    
    VIEW_TYPES.append(('tree_eval_trial', 'Tree'))
    VIEW_TYPES.append(('form_eval_trial', 'Form'))
    VIEW_TYPES.append(('form_eval_trial2', 'Form'))
    
    VIEW_TYPES.append(('tree_eval_incorrect', 'Tree'))
    VIEW_TYPES.append(('form_eval_incorrect', 'Form'))
    
    VIEW_TYPES.append(('tree_eval_file', 'Tree'))
    VIEW_TYPES.append(('form_eval_file', 'Form'))
    
    VIEW_TYPES.append(('tree_users', 'Tree'))
    VIEW_TYPES.append(('form_users', 'Form'))


class View(models.Model):
    _inherit = 'ir.ui.view'

    type = fields.Selection(
		selection_add=[
			('tree_eval_effective', 'Tree'),
			('form_eval_effective0', 'Form'),
			('form_eval_effective', 'Form'),
			
			('tree_eval_trial', 'Tree'),
			('form_eval_trial', 'Form'),
			('form_eval_trial2', 'Form'),
			
			('tree_eval_incorrect', 'Tree'),
			('form_eval_incorrect', 'Form'),
			
			('tree_eval_file', 'Tree'),
			('form_eval_file', 'Form'),
			
			('tree_users', 'Tree'),
			('form_users', 'Form'),
		])


class ActWindowView(models.Model):
    _inherit = 'ir.actions.act_window.view'

    view_mode = fields.Selection(
		selection_add=[
			('tree_eval_effective', 'Tree'), 
			('form_eval_effective0', 'Form'),
			('form_eval_effective', 'Form'),
			
			('tree_eval_trial', 'Tree'),
			('form_eval_trial', 'Form'),
			('form_eval_trial2', 'Form'),
			
			('tree_eval_incorrect', 'Tree'),
			('form_eval_incorrect', 'Form'),
			
			('tree_eval_file', 'Tree'),
			('form_eval_file', 'Form'),
			
			('tree_users', 'Tree'),
			('form_users', 'Form'),
		])
