# -*- coding: utf-8 -*-
from odoo import fields, models, api
import sys
import datetime
from datetime import timedelta
from odoo.exceptions import Warning
import json
import hashlib
from const import SECRET_KEY1
from operator import itemgetter
from odoo import tools
try:
	import cStringIO as StringIO
except ImportError:
	import StringIO

from PIL import Image

from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT

reload(sys).setdefaultencoding('utf-8')

now_const = datetime.datetime.now()
MAX_IMAGE_SIZE = 800

def isfloat(x):
	try:
		float(x)
	except ValueError:
		return False
	else:
		return True

def isint(x):
	try:
		a = float(x)
		b = int(a)
	except ValueError:
		return False
	else:
		return a == b


class EvalFolders(models.Model):
	_name = 'eval.folders'
	_description = u'Thư mục'
	
	name = fields.Char(u'Tên thư mục')
	descx = fields.Text(u'Mô tả')


class EvalFiles(models.Model):
	_name = 'eval.files'
	_description = u'Tệp tin'
	
	@api.multi
	def compute_order(self):
		for i, r in enumerate(self):
			r.order = i+1
	
	order = fields.Integer(u'STT', compute=compute_order, store=False, readonly=True)
	name = fields.Char(u'Tên file')
	descx = fields.Text(u'Mô tả')
	url = fields.Char(u'Link tải')
	folder_id = fields.Many2one('eval.folders', u'Thư mục', required=True)
	
	@api.multi
	def delete_file_action(self):
		self.ensure_one()
		actor = self.env['res.users'].browse(self._uid)
		if actor.quota_id.can_edit_files:
			self.sudo().unlink()
			return True
		else:
			return False
		
	@api.multi
	def edit_file_action(self):
		self.ensure_one()
		actor = self.env['res.users'].browse(self._uid)
		if actor.quota_id.can_edit_files:
			return {
				'name': 'Cập nhật tài liệu',
				'views': [[False, 'form_eval_file']],
                'res_model': 'eval.files',
				'type': 'ir.actions.act_window',
				'target': 'new',
				'res_id': self.id,
			}
		else:
			return {
				'type': 'ir.actions.client',
				'tag': 'reload'
			}
	
	@api.model
	def default_get(self, fields_list):
		r = super(EvalFiles, self).default_get(fields_list)
		context = self._context
		if context.get('folder_id') > 0:
			r.update({'folder_id': context.get('folder_id')})
		return r


class EvalIncorrect (models.Model):
	_name = 'eval.incorrect'
	_description = u'Lĩnh vực không phù hợp'
	_order = 'sequence asc'
	
	name = fields.Char(string=u'Tên lĩnh vực', required=True)
	sequence = fields.Integer(u'Thứ tự', readonly=True)
	state = fields.Selection([ (2, u'Tắt'), (1, u'Bật') ], string=u'Trạng thái', default=1, required=True)
	
	@api.model
	def create(self, vals):
		maxx = self.search_read([], ['sequence'], limit=1, order='sequence desc')
		if not maxx:
			vals.update({ 'sequence': 1 })
		else:
			vals.update({ 'sequence': maxx[0]['sequence'] + 1 })
		return super(EvalIncorrect, self).create(vals)


class EvalIncorrectLoc(models.Model):
	_name = 'eval.incorrect.loc'
	_description = u'Nơi phát hiện KPH'
	
	name = fields.Char(u'Tên')


class EvalIncorrectTicket (models.Model):
	_name = 'eval.incorrect.ticket'
	_description = u'Báo cáo không phù hợp'
	_order = 'create_date desc'
	
	@api.model
	def default_get(self, fields_list):
		r = super(EvalIncorrectTicket, self).default_get(fields_list)
		context = self._context
		if context.get('cat_id') > 0:
			r.update({'category_id': context.get('cat_id')})
		return r
		
	@api.multi
	def compute_order(self):
		for i, r in enumerate(self):
			r.order = len(self) - i
	
	order = fields.Integer(u'STT', compute=compute_order, store=False, readonly=False)
	date = fields.Date(u'Ngày')
	create_uid = fields.Many2one('res.users', u'Người phát hiện')
	project = fields.Char(u'Dự án', help=u"Không phù hợp ở dự án nào?")
	step = fields.Char(u'Qui trình/công đoạn', help=u"Không phù hợp ở qui trình/công đoạn nào?")
	content = fields.Text(u'Nội dung KPH')
	number = fields.Integer(u'Số lượng', help=u"Số lượng mục không phù hợp", default=1)
	respon = fields.Char(u'Trách nhiệm KPH')
	category_id = fields.Many2one('eval.incorrect', u'Lĩnh vực')
	solution = fields.Text(u'Xử lý')
	year = fields.Integer(readonly=True)
	month = fields.Integer(readonly=True)
	is_serious = fields.Boolean(u'Lỗi nghiêm trọng', help=u"Đây có phải là lỗi nghiêm trọng không?", default=False)
	is_repeat = fields.Boolean(u'Lỗi lặp lại', help=u"Đây có phải là lỗi lặp lại nhiều lần không?", default=False)
	image_1 = fields.Binary(u'Ảnh 1', help=u'Ảnh kích thước dài rộng tối đa 800px\ntự động thu nhỏ ảnh nếu lớn hơn\nChỉ thêm ảnh khi thực sự cần thiết')
	image_2 = fields.Binary(u'Ảnh 2', help=u'Ảnh kích thước dài rộng tối đa 800px\ntự động thu nhỏ ảnh nếu lớn hơn\nChỉ thêm ảnh khi thực sự cần thiết')
	location_id = fields.Many2one('eval.incorrect.loc', u'Nơi phát hiện')
	
	@api.model
	def unlink_ticket(self, idx):
		try:
			self.search([ ('id', '=', idx) ]).unlink()
			return True
		except:
			return False
	
	@api.model
	def create(self, vals):
		if 'date' in vals:
			date_obj = datetime.datetime.strptime(vals['date'], DEFAULT_SERVER_DATE_FORMAT)
			vals['year'], vals['month'] = date_obj.year, date_obj.month
		if 'image_1' in vals and vals['image_1']:
			image_stream = StringIO.StringIO(vals['image_1'].decode('base64'))
			image = Image.open(image_stream)
			# if width need to be optimize
			if image.size[0] > MAX_IMAGE_SIZE and image.size[0] > image.size[1]:
				vals['image_1'] = tools.image_resize_image_big(vals['image_1'], size=(MAX_IMAGE_SIZE, image.size[1]*MAX_IMAGE_SIZE/image.size[0]), avoid_if_small=True)
			elif image.size[1] > MAX_IMAGE_SIZE and image.size[1] > image.size[0]:
				vals['image_1'] = tools.image_resize_image_big(vals['image_1'], size=(image.size[0]*MAX_IMAGE_SIZE/image.size[1], MAX_IMAGE_SIZE), avoid_if_small=True)
			else:
				vals['image_1'] = tools.image_resize_image_big(vals['image_1'], size=(MAX_IMAGE_SIZE, MAX_IMAGE_SIZE), avoid_if_small=True)
		
		if 'image_2' in vals and vals['image_2']:
			image_stream = StringIO.StringIO(vals['image_2'].decode('base64'))
			image = Image.open(image_stream)
			# if width need to be optimize
			if image.size[0] > MAX_IMAGE_SIZE and image.size[0] > image.size[1]:
				vals['image_2'] = tools.image_resize_image_big(vals['image_2'], size=(MAX_IMAGE_SIZE, image.size[1]*MAX_IMAGE_SIZE/image.size[0]), avoid_if_small=True)
			elif image.size[1] > MAX_IMAGE_SIZE and image.size[1] > image.size[0]:
				vals['image_2'] = tools.image_resize_image_big(vals['image_2'], size=(image.size[0]*MAX_IMAGE_SIZE/image.size[1], MAX_IMAGE_SIZE), avoid_if_small=True)
			else:
				vals['image_2'] = tools.image_resize_image_big(vals['image_2'], size=(MAX_IMAGE_SIZE, MAX_IMAGE_SIZE), avoid_if_small=True)
		
		return super(EvalIncorrectTicket, self).create(vals)
	
	@api.model
	def search(self, args, offset=0, limit=None, order=None, count=False):
		field, text, op = '', '', '='
		remove_index = []
		owner = False
		for i, d in enumerate(args):
			if d[0] == u'create_uid' and d[2] == self._uid:
				owner = True
			elif d[0] == 'a1':
				field = d[2]
				remove_index.append(i)
			elif d[0] == 'a2':
				text = d[2]
				if isint(text):
					text = int(text)
				elif isfloat(text):
					text = float(text)
				else:
					op = 'ilike'
				remove_index.append(i)
		if len(remove_index):
			args = [e for i,e in enumerate(args) if i not in remove_index]
		if field != '' and text != '':
			args.append([field, op, text])

		actor = self.env['res.users'].browse(self._uid)
		for d in args:
			if not owner and d[0] == u'category_id' and d[2] not in actor.quota_id.incorrect_ids.ids:
				args.append(['create_uid', '=', self._uid])
		return super(EvalIncorrectTicket, self).search(args, offset, limit, order, count)
	
	@api.model
	def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
		field, text, op = '', '', '='
		remove_index = []
		owner = False
		for i, d in enumerate(domain):
			if d[0] == u'create_uid' and d[2] == self._uid:
				owner = True
			elif d[0] == 'a1':
				field = d[2]
				remove_index.append(i)
			elif d[0] == 'a2':
				text = d[2]
				if isint(text):
					text = int(text)
				elif isfloat(text):
					text = float(text)
				else:
					op = 'ilike'
				remove_index.append(i)
		if len(remove_index):
			domain = [e for i,e in enumerate(domain) if i not in remove_index]
		if field != '' and text != '':
			domain.append([field, op, text])

		actor = self.env['res.users'].browse(self._uid)
		for d in domain:
			if not owner and d[0] == u'category_id' and d[2] not in actor.quota_id.incorrect_ids.ids:
				domain.append(['create_uid', '=', self._uid])
		res = super(EvalIncorrectTicket, self).search_read(domain=domain, fields=fields, offset=offset, limit=limit, order='create_date desc')
		return res


class EvalQuotaGroup (models.Model):
	_name = 'eval.quota.group'
	_description = u'Nhóm ngạch'
	
	name = fields.Char(string=u'Tên nhóm ngạch', required=True)


class EvalQuota (models.Model):
	_name = 'eval.quota'
	_description = u'Ngạch bậc'
	
	@api.onchange('can_approve_trial')
	def onchange_can_approve_trial(self):
		if self.can_approve_trial:
			self.can_view_trial = True
	
	name = fields.Char(string=u'Tên ngạch', required=True)
	code = fields.Char(string=u'Mã ngạch', required=True)
	# number = fields.Integer(string=u'Số bậc', required=True)
	group = fields.Many2one('eval.quota.group', string=u'Nhóm', required=True)
	effect_id = fields.Many2one('eval.effect.template', string=u'Mẫu phiếu hiệu quả',
							help=u"Chọn một mẫu phiếu đánh giá hiệu quả cho ngạch này")
	level = fields.Selection([('1', u'QL cấp 1'), ('2', u'QL cấp 2'), ('3', u'QL cấp 3'), ('4', u'Nhân viên')],
							string=u'Ghi chú chức vụ', default='4')
	state = fields.Selection([ (1, u'Bật'), (2, u'Tắt') ], string=u'Trạng thái', default=1)
	can_view_trial = fields.Boolean(string=u'Xem phiếu thử việc', 
								help=u"Nhân sự ngạch này có được xem toàn bộ\nphiếu đánh giá thử việc không?")
	can_eval_trial = fields.Boolean(string=u'Đánh giá thử việc', 
								help=u"Nhân sự ngạch này có được tạo phiếu\nđánh giá thử việc không?")
	can_approve_trial = fields.Boolean(string=u'Duyệt phiếu thử việc',
								help=u"Nhân sự ngạch này có được duyệt phiếu\nđánh giá thử việc các cấp không?")
	effect_group_ids = fields.Many2many('eval.quota.group', 'eval_quota_group_rel', 'quota_id', 'group_id', string=u'Đánh giá ở nhóm',
									help=u"Nhân sự ngạch này có thể tạo phiếu đánh giá\nhiệu quả chéo ở các nhóm ngạch khác?")
	can_effect_view_all = fields.Boolean(string=u'Xem phiếu hiệu quả toàn bộ', 
								help=u"Nhân sự ngạch này có được xem phiếu\nđánh giá hiệu quả toàn bộ không?")
	can_effect_all = fields.Boolean(string=u'Đánh giá hiệu quả các cấp', 
								help=u"Nhân sự ngạch này có được tạo phiếu\nđánh giá hiệu quả tất cả các cấp không?")
	can_effect_cross = fields.Boolean(string=u'Đánh giá hiệu quả chéo', 
								help=u"Nhân sự ngạch này có được tạo phiếu\nđánh giá hiệu quả chéo không?")
	incorrect_ids = fields.Many2many('eval.incorrect', 'eval_incorrect_quota_rel', 'incorrect_id', 'quota_id', string=u'Các lĩnh vực KPH',
									help=u"Nhân sự ngạch này được xem tất cả\nphiếu không phù hợp ở những lĩnh vực nào?")
	can_edit_files = fields.Boolean(string=u'Cập nhật tài liệu', 
								help=u"Nhân sự ngạch này có được\ncập nhật tài liệu không?")
	
	@api.model
	def create(self, vals):
		if 'can_approve_trial' in vals and vals['can_approve_trial'] == True:
			vals['can_view_trial'] = True
		return super(EvalQuota, self).create(vals)
	
	@api.multi
	def write(self, vals):
		if 'can_approve_trial' in vals and vals['can_approve_trial'] == True:
			vals['can_view_trial'] = True
		return super(EvalQuota, self).write(vals)


class EvalTrialIndicator (models.Model):
	_name = 'eval.trial.indicator'
	_description = u'Chỉ tiêu'
	_order = 'sequence asc, create_date desc'
	
	name = fields.Char(required=True, string=u'Tên chỉ tiêu')
	content = fields.Text(string=u'Nội dung', required=True)
	point = fields.Integer(string=u'Điểm đánh giá', help=u"Điểm tối đa cho chỉ tiêu này\nYêu cầu nhập số nguyên")
	display_point = fields.Integer(compute='_compute_display_point', store=False, string=u'Tổng điểm', readonly=True)
	is_group = fields.Boolean(string=u'Là nhóm', help=u"Chỉ tiêu này có chứa các chỉ tiêu nhỏ không?")
	note = fields.Text(string=u'Ghi chú', default='')
	parent_id = fields.Many2one('eval.trial.indicator', string=u'Chỉ tiêu cha', ondelete='cascade')
	indicator_ids = fields.One2many('eval.trial.indicator', 'parent_id', string=u'Chỉ tiêu đánh giá con')
	template_id = fields.Many2one('eval.trial.template', string=u'Thuộc mẫu thử việc',
								help=u"Chỉ tiêu này thuộc mẫu phiếu\nđánh giá thử việc nào?", ondelete='cascade')
	sequence = fields.Integer(u'Thứ tự', default=1)
	token = fields.Char(readonly=True)

	@api.model
	def create(self, vals):
		if 'indicator_ids' in vals and len(vals['indicator_ids']):
			points = 0
			for r in vals['indicator_ids']:
				points += r[2].get('point')
			vals.update({ 'point': points })
		res = super(EvalTrialIndicator, self).create(vals)
		if not res.is_group:
			res.update({'token': hashlib.md5("%d%d%s%d" % (res.point, res.id, SECRET_KEY1, res.id)).hexdigest()})
		return res
	
	@api.multi
	def write(self, vals):
		self.ensure_one()
		if 'point' in vals and not self.is_group:
			vals.update({'token': hashlib.md5("%d%d%s%d" % (int(vals['point']), self.id, SECRET_KEY1, self.id)).hexdigest()})
		return super(EvalTrialIndicator, self).write(vals)

	@api.onchange('indicator_ids', 'is_group')
	def _compute_display_point(self):
		def _get_child_point(record):
			res = 0
			if record.is_group and len(record.indicator_ids):
				for r in record.indicator_ids:
					res += _get_child_point(r)
			else:
				res = record.point
			return res
		for record in self:
			record.display_point = record.point = _get_child_point(record)


class EvalEffectIndicator (models.Model):
	_name = 'eval.effect.indicator'
	_description = u'Chỉ tiêu đánh giá'
	_order = 'sequence asc, create_date desc'
	
	name = fields.Char(required=True, string=u'Tên chỉ tiêu')
	content = fields.Text(string=u'Nội dung', required=True)
	point = fields.Integer(string=u'Điểm đánh giá', help=u"Điểm tối đa cho chỉ tiêu này\nYêu cầu nhập số nguyên")
	display_point = fields.Integer(compute='_compute_display_point', store=False, string=u'Tổng điểm', readonly=True)
	is_group = fields.Boolean(string=u'Là nhóm', help=u"Chỉ tiêu này có chứa các chỉ tiêu nhỏ không?")
	note = fields.Text(string=u'Ghi chú', default='')
	parent_id = fields.Many2one('eval.effect.indicator', string=u'Chỉ tiêu cha', ondelete='cascade')
	indicator_ids = fields.One2many('eval.effect.indicator', 'parent_id', string=u'Chỉ tiêu đánh giá con')
	template_id = fields.Many2one('eval.effect.template', string=u'Thuộc mẫu hiệu quả',
								help=u"Chỉ tiêu này thuộc mẫu phiếu\nđánh giá hiệu quả nào?", ondelete='cascade')
	sequence = fields.Integer(u'Thứ tự', default=1)
	token = fields.Char(readonly=True)

	@api.model
	def create(self, vals):
		if 'indicator_ids' in vals and len(vals['indicator_ids']):
			points = 0
			for r in vals['indicator_ids']:
				points += r[2].get('point')
			vals.update({ 'point': points })
		res = super(EvalEffectIndicator, self).create(vals)
		if not res.is_group:
			res.update({'token': hashlib.md5("%d%d%s%d" % (res.point, res.id, SECRET_KEY1, res.id)).hexdigest()})
		return res
	
	@api.multi
	def write(self, vals):
		self.ensure_one()
		if 'point' in vals and not self.is_group:
			vals.update({'token': hashlib.md5("%d%d%s%d" % (int(vals['point']), self.id, SECRET_KEY1, self.id)).hexdigest()})
		return super(EvalEffectIndicator, self).write(vals)

	@api.onchange('indicator_ids', 'is_group')
	def _compute_display_point(self):
		def _get_child_point(record):
			res = 0
			if record.is_group and len(record.indicator_ids):
				for r in record.indicator_ids:
					res += _get_child_point(r)
			else:
				res = record.point
			return res
		for record in self:
			record.display_point = record.point = _get_child_point(record)


class EvalEffectTemplate (models.Model):
	_name = 'eval.effect.template'
	_description = u'Mẫu phiếu đánh giá hiệu quả'
	_order = 'create_date desc'
	
	def _get_version_name(self, quota_id):
		if quota_id:
			count = self.search_count([ ('quota_id', '=', quota_id) ]) + 1
			return 'Ver: %02d Date: %02d/%02d/%d' % (count, now_const.day, now_const.month, now_const.year)
		return ''
	
	@api.onchange('quota_id')
	def onchange_quota_id(self):
		self.version_name = self._get_version_name(self.quota_id.id)

	quota_id = fields.Many2one('eval.quota', string=u'Ngạch bậc',
							help=u"Mẫu phiếu này dùng cho ngạch nào?", required=True)
	name = fields.Char(string=u'Tên mẫu phiếu', required=True)
	version_name = fields.Char(string=u'Phiên bản', readonly=True)
	indicator_ids = fields.One2many('eval.effect.indicator', 'template_id', string=u'Chỉ tiêu đánh giá')
	state = fields.Selection([ ('draft', u'Chưa áp dụng'), ('applied', u'Đã áp dụng') ], string=u'Trạng thái', default='draft', readonly=True)

	@api.model
	def getIndicatorsByTemplateId(self, idx):
		record = self.browse(idx)
		if not record:
			return None
		def _getIndicators(indicators):
			res = []
			for indicator in indicators:
				if len(indicator.indicator_ids):
					for i in _getIndicators(indicator.indicator_ids):
						res.append(i)
				else:
					tmp = {
						'id': indicator.id,
						'point': indicator.point,
						'token': indicator.token or ''
					}
					res.append(tmp)
			return res
		return _getIndicators(record.indicator_ids)
	
	@api.multi
	def set_apply(self):
		self.ensure_one()
		self.state = 'applied'
		
	@api.multi
	def dupplicate(self):
		self.ensure_one()
		new_template = self.create({
			'quota_id': self.quota_id.id,
			'name': self.name,
			#'version_name': self._get_version_name(self.quota_id.id),
			'state': 'draft',
			#'indicator_ids': self.quota_id,
		})
		if new_template and new_template.id:
			def dupplicate_indicators(indicators, parent_id):
				for indicator in indicators:
					new_ind = self.env['eval.effect.indicator'].create({
						'name': indicator.name,
						'content': indicator.content,
						'point':indicator.point,
						'is_group': indicator.is_group,
						'parent_id': parent_id,
						'sequence': indicator.sequence,
					})
					if new_ind:
						if (indicator.is_group):
							dupplicate_indicators(indicator.indicator_ids, new_ind.id)
					else:
						raise Warning("Lỗi sao chép bản ghi!")
					
			for indicator in self.indicator_ids:
				new_indicator = self.env['eval.effect.indicator'].create({
					'name': indicator.name,
					'content': indicator.content,
					'point':indicator.point,
					'is_group': indicator.is_group,
					'template_id': new_template.id,
					'sequence': indicator.sequence,
				})
				if new_indicator and new_indicator.id:
					if (indicator.is_group):
						dupplicate_indicators(indicator.indicator_ids, new_indicator.id)
				else:
					raise Warning("Lỗi sao chép bản ghi!")
			return {
	            'name':"Products to Process",
	            'view_mode': 'form',
	            'view_id': False,
	            'view_type': 'form',
	            'res_model': 'eval.effect.template',
	            'res_id': new_template.id,
	            'type': 'ir.actions.act_window',
	            'target': 'current',
	        }
		else:
			raise Warning("Lỗi sao chép bản ghi!")

	@api.model
	def create(self, vals):
		version_name = self._get_version_name(vals['quota_id'])
		vals.update({ 'version_name': version_name })
		return super(EvalEffectTemplate, self).create(vals)
	
	@api.model
	def unlink_template(self, idx):
		try:
			self.search([ ('id', '=', idx) ]).unlink()
			return True
		except:
			return False
	
	@api.model
	def name_search(self, name, args=None, operator='ilike', limit=100):
		context = self._context
		if not context.get('select_effect_template'):
			return super(EvalEffectTemplate, self).name_search(name, args, operator=operator, limit=limit)
		else:
			templates = self.search_read([('quota_id', '=', context.get('quota_id')), ('state', '=', 'applied')], ['name', 'version_name'], order='create_date desc')
			if templates:
				res = []
				for template in templates:
					res.append((template.get('id'), template.get('name') + ' - ' + template.get('version_name')))
				return res
			else:
				return False


class EvalPosition (models.Model):
	_name = 'eval.position'
	_description = u'Chức vụ'
	
	name = fields.Char(string=u'Tên chức vụ', required=True)
	quota_id = fields.Many2one('eval.quota', string=u'Ngạch bậc', required=True,
							help=u"Chức vụ này thuộc ngạch bậc nào?")
	
	@api.model
	def create(self, vals):
		if 'name' not in vals or 'quota_id' not in vals:
			raise Warning(u"Dữ liệu không hợp lệ!")
		is_existed = self.search_count([ ('name', '=', vals['name']), ('quota_id', '=', vals['quota_id']) ])
		if is_existed > 0:
			raise Warning(u"Chức vụ cùng tên và ngạch bạn vừa nhập đã tồn tại.\nVui lòng chọn tên khác!")
		return super(EvalPosition, self).create(vals)


class EvalEffectTicket (models.Model):
	_name = 'eval.effect.ticket'
	_description = u'Phiếu đánh giá hiệu quả'
	_order = 'create_date desc'
	
	@api.model
	def unlink_ticket(self, idx):
		try:
			self.search([ ('id', '=', idx) ]).unlink()
			return True
		except:
			return False
	
	@api.model
	def search(self, args, offset=0, limit=None, order=None, count=False):
		field, text, op = '', '', '='
		remove_index = []
		for i, d in enumerate(args):
			if field != '' and text != '':
				break
			if d[0] == 'a1':
				field = d[2]
				remove_index.append(i)
			elif d[0] == 'a2':
				text = d[2]
				if isint(text):
					text = int(text)
				elif isfloat(text):
					text = float(text)
				else:
					op = 'ilike'
				remove_index.append(i)
		if len(remove_index):
			args = [e for i,e in enumerate(args) if i not in remove_index]
		if field != '' and text != '':
			args.append([field, op, text])
		return super(EvalEffectTicket, self).search(args, offset, limit, order, count)
	
	@api.model
	def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
		field, text, op = '', '', '='
		remove_index = []
		for i, d in enumerate(domain):
			if field != '' and text != '':
				break
			if d[0] == 'a1':
				field = d[2]
				remove_index.append(i)
			elif d[0] == 'a2':
				text = d[2]
				if isint(text):
					text = int(text)
				elif isfloat(text):
					text = float(text)
				else:
					op = 'ilike'
				remove_index.append(i)
		if len(remove_index):
			domain = [e for i,e in enumerate(domain) if i not in remove_index]
		if field != '' and text != '':
			domain.append([field, op, text])
		return super(EvalEffectTicket, self).search_read(domain, fields, offset, limit, order)
	
	@api.model
	def _default_year(self):
		return now_const.year
	
	@api.model
	def _default_month(self):
		return now_const.month
	
	@api.model
	def getFormInfo(self, record_id=False):
		tmp = self.env.ref('staff_evaluate.view_evaluate_effective_form', False)
		if tmp:
			res = {'id': tmp.id}
		else:
			res = {'id': False}
		if record_id:
			record = self.browse(record_id)
			res.update({'name': record.name})
		else:
			res.update({'name': 'Thêm phiếu mới'})
		return res

	@api.onchange('user', 'month', 'year')
	def onchange_draft(self):
		if not self.month or not self.user or not self.year:
			self.user_code = False
			self.position_id = False
			self.quota_id = False
			self.number = False
			self.company = False
			self.message = ''
			self.styles = '<style>.eval_effect_form_buttons button {display:none !important;}</style>'
			return
		existed = self.sudo().search([('user', '=', self.user.id), ('month', '=', self.month), ('year', '=', self.year)], limit=1)
		staff = self.env['res.users'].sudo().browse(self.user.id)
		self.template_id = staff.position.quota_id.effect_id
		if not self.template_id:
			self.message = u"Ngạch của người này chưa chọn mẫu phiếu đánh giá!"
			self.styles = '<style>.eval_effect_form_buttons button {display:none !important;}</style>'
		else:
			self.message = ''
			self.styles = ''
		self.user_code = staff.code
		self.position_id = staff.position
		self.quota_id = staff.position.quota_id.code
		self.number = staff.number
		self.company = staff.company_id
		
		if len(existed):
			self.message = u"Nhân viên này đã được tạo phiếu tháng %d/%d bởi %s" % (self.month, self.year, existed.create_uid.name)
			self.styles = '<style>.eval_effect_form_buttons button {display:none !important;}</style>'
	
	@api.multi
	def compute_order(self):
		for i, r in enumerate(self):
			r.order = len(self) - i
	
	order = fields.Integer(u'STT', compute=compute_order, store=False, readonly=False)
	name = fields.Char(u'Tên phiếu',readonly=True)
	template_id = fields.Many2one('eval.effect.template', string=u'Mã mẫu phiếu')
	template_ver = fields.Char(u'Phiên bản', related='template_id.version_name', store=True, readonly=True)
	# Json leader points data
	leader_points = fields.Char()
	leader_total = fields.Integer(u'QL đánh giá', readonly=True, default=0)
	# In-form, temporary, invisible points input data
	inform_points = fields.Char(store=False)
	month = fields.Selection([(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), 
							(7, 7), (8, 8), (9, 9), (10, 10), (11, 11), (12, 12)], 
							string=u'Tháng', default=_default_month)
	year = fields.Integer(u'Năm', default=_default_year)
	user = fields.Many2one('res.users', u'Họ tên')
	user_name = fields.Char(related='user.name', store=True)
	create_name = fields.Char(related='create_uid.name', store=True)
	company = fields.Many2one('res.company', string=u'Công ty', readonly=True)
	user_code = fields.Char(u'Mã nhân viên')
	position_id = fields.Many2one('eval.position', string=u'Chức vụ', readonly=True)
	quota_id = fields.Char(string=u'Ngạch', readonly=True)
	number = fields.Integer(string=u'Bậc', readonly=True)
	address = fields.Char(u'Địa chỉ làm việc')
	
	message = fields.Char(u'Ghi chú', store=False, readonly=True)
	styles = fields.Char(store=False, readonly=True)
	note = fields.Text(u'Ghi chú')
	active = fields.Boolean(default=False)
	achieved = fields.Boolean(default=False)
	
	@api.model
	@api.returns('self', lambda value: value)
	def create(self, vals):
		if 'active' in vals and vals['active'] == False and 'user' in vals:
			user = self.env['res.users'].sudo().browse(vals['user'])
			vals.update({
				'template_id': user.quota_id.effect_id.id,
				'position_id': user.position.id,
				'company': user.company_id.id,
				'quota_id': user.quota_id.code,
				'number': user.number,
				'user_code': user.code,
			})
			res = super(EvalEffectTicket, self).create(vals)
			return res.id
		raise Warning(u"Dữ liệu không hợp lệ, vui lòng kiểm tra lại!")

	@api.multi
	def write(self, vals):
		self.ensure_one()
		if 'achieved' in vals and len(vals) == 1:
			return super(EvalEffectTicket, self).write(vals)
		if 'inform_points' not in vals:
			raise Warning(u'Dữ liệu điểm đánh giá không hợp lệ! Vui lòng kiểm tra lại.')
			return
		pointsUserData = json.loads(vals['inform_points'])
		if len(pointsUserData):
			valid = True
			for i in pointsUserData:
				i.update({'id': int(i.get('id'))})
			pointsUserData = sorted(pointsUserData, key=itemgetter(u'id'), reverse=True)
			templateIndicators = self.env['eval.effect.template'].sudo().getIndicatorsByTemplateId(self.template_id.id)
			templateIndicators = sorted(templateIndicators, key=itemgetter(u'id'), reverse=True)
			pointsSysData = []
			totalPoint = 0
			errorInputIndex = []
			for index, indicator in enumerate(templateIndicators):
				try:
					userPoint = int(pointsUserData[index].get('point'))
				except:
					userPoint = 0
	
				totalPoint += userPoint
				userToken = hashlib.md5("%s%s%s%s" % (pointsUserData[index].get('max'), pointsUserData[index].get('id'), SECRET_KEY1, pointsUserData[index].get('id'))).hexdigest()
				if pointsUserData[index].get('token') == userToken and \
					indicator.get('token') == userToken and \
					userPoint <= indicator.get('point') and \
					userPoint >= 0:
					pointsSysData.append({ 'id': indicator.get('id'), 'point': userPoint })
				else:
					errorInputIndex.append(pointsUserData[index].get('id'))
					valid = False
			if not valid:
				return {
					'error': errorInputIndex
				}
			vals.update({
				'active': True,
				'leader_total': totalPoint, 
				'leader_points': json.dumps(pointsSysData)
			})
		return super(EvalEffectTicket, self).write(vals)

	@api.multi
	def schedule_delete_inactive(self):
		last_day = now_const - timedelta(hours=24)
		self.search([ ('active', '=', False), ('create_date', '<', last_day.strftime(DEFAULT_SERVER_DATETIME_FORMAT)) ]).unlink()


class EvalTrialTemplate (models.Model):
	_name = 'eval.trial.template'
	_description = u'Mẫu phiếu đánh giá thử việc'
	_order = 'create_date desc'

	def _get_version_name(self):
		count = self.search_count([]) + 1
		return 'Ver: %02d Date: %02d/%02d/%d' % (count, now_const.day, now_const.month, now_const.year)
	
	@api.onchange('name')
	def onchange_quota_id(self):
		self.version_name = self._get_version_name()
	
	name = fields.Char(string=u'Tên mẫu phiếu', required=True)
	version_name = fields.Char(string=u'Phiên bản', readonly=True)
	indicator_ids = fields.One2many('eval.trial.indicator', 'template_id', string=u'Chỉ tiêu đánh giá')
	state = fields.Selection([ ('draft', u'Chưa áp dụng'), ('applied', u'Đã áp dụng') ], string=u'Trạng thái', default='draft', readonly=True)
	using = fields.Boolean(u'Đang dùng?')
	
	@api.model
	def getIndicatorsByTemplateId(self, idx):
		record = self.browse(idx)
		if not record:
			return None
		def _getIndicators(indicators):
			res = []
			for indicator in indicators:
				if len(indicator.indicator_ids):
					for i in _getIndicators(indicator.indicator_ids):
						res.append(i)
				else:
					tmp = {
						'id': indicator.id,
						'point': indicator.point,
						'token': indicator.token or ''
					}
					res.append(tmp)
			return res
		return _getIndicators(record.indicator_ids)
	
	@api.multi
	def set_apply(self):
		self.ensure_one()
		self.state = 'applied'
		self.search([]).update({'using': False})
		self.using = True
		
	@api.multi
	def set_unapply(self):
		self.ensure_one()
		self.state = 'draft'
	
	@api.model
	def create(self, vals):
		version_name = self._get_version_name()
		vals.update({ 'version_name': version_name })
		return super(EvalTrialTemplate, self).create(vals)

	@api.model
	def unlink_template(self, idx):
		try:
			self.search([ ('id', '=', idx) ]).unlink()
			return True
		except:
			return False
		

class EvalTrialTicket (models.Model):
	_name = 'eval.trial.ticket'
	_description = u'Phiếu đánh giá thử việc'
	_order = 'create_date desc'
	
	@api.model
	def unlink_ticket(self, idx):
		try:
			self.search([ ('id', '=', idx) ]).unlink()
			return True
		except:
			return False

	@api.model
	def _get_default_template_id(self):
		templates = self.env['eval.trial.template'].search([ ('using', '=', True) ], limit=1)
		if len(templates):
			return templates[0].id
	
	@api.multi
	def compute_order(self):
		for i, r in enumerate(self):
			r.order = len(self) - i
	
	order = fields.Integer(u'STT', compute=compute_order, store=False, readonly=False)
	name = fields.Char(u'Tên phiếu',readonly=True)
	template_id = fields.Many2one('eval.trial.template', string=u'Mã mẫu phiếu', default=_get_default_template_id)
	template_ver = fields.Char(u'Phiên bản', related='template_id.version_name', store=True, readonly=True)
	# Json leader points data
	leader_points = fields.Char()
	leader_total = fields.Integer(u'QL đánh giá', readonly=True)
	# In-form, temporary, invisible points input data
	inform_points = fields.Char(store=False)
	create_uid = fields.Many2one('res.users', u'Tên quản lý', default=lambda self: self._uid)
	user = fields.Char(u'Tên NV thử việc')
	
	position_id = fields.Char(string=u'Công việc sẽ nhận')
	quota_id = fields.Char(string=u'Ngạch')
	quota_number = fields.Integer(string=u'Bậc')
	address = fields.Char(u'Địa điểm làm việc')
	date_from = fields.Date(u'Từ ngày')
	date_to = fields.Date(u'Đến ngày')
	year = fields.Integer(u'Năm', readonly=True)
	manager_approved = fields.Boolean(u'QL duyệt', default=False)
	approved_uid = fields.Many2one('res.users', u'Người duyệt')
	sum = fields.Selection([('1', u'Tốt'), ('2', u'Khá'), ('3', u'Trung bình')], u'Đánh giá')
	
	message = fields.Char(u'Ghi chú', store=False, readonly=True)
	styles = fields.Char(store=False, readonly=True)
	note = fields.Text(u'Ghi chú')
	#draft = fields.Boolean(compute='_compute_is_draft', store=False)
	achieved = fields.Boolean(default=False)
	
	@api.model
	def set_approve(self, idx):
		actor = self.env['res.users'].sudo().browse(self._uid)
		if actor.quota_id.can_approve_trial:
			record = self.browse(idx)
			if record:
				record.manager_approved = True
				record.approved_uid = self._uid
				return True
			return False
		return False
	
	@api.model
	@api.returns('self', lambda value: value)
	def create(self, vals):
		if 'inform_points' not in vals or 'template_id' not in vals:
			raise Warning(u'Dữ liệu điểm đánh giá không hợp lệ! Vui lòng kiểm tra lại.')
			return
		pointsUserData = json.loads(vals['inform_points'])
		if len(pointsUserData):
			valid = True
			for i in pointsUserData:
				i.update({'id': int(i.get('id'))})
			pointsUserData = sorted(pointsUserData, key=itemgetter(u'id'), reverse=True)
			templateIndicators = self.env['eval.trial.template'].sudo().getIndicatorsByTemplateId(vals['template_id'])
			templateIndicators = sorted(templateIndicators, key=itemgetter(u'id'), reverse=True)
			pointsSysData = []
			totalPoint = 0
			errorInputIndex = []
			for index, indicator in enumerate(templateIndicators):
				try:
					userPoint = int(pointsUserData[index].get('point'))
				except:
					userPoint = 0
	
				totalPoint += userPoint
				userToken = hashlib.md5("%s%s%s%s" % (pointsUserData[index].get('max'), pointsUserData[index].get('id'), SECRET_KEY1, pointsUserData[index].get('id'))).hexdigest()
				if pointsUserData[index].get('token') == userToken and \
					indicator.get('token') == userToken and \
					userPoint <= indicator.get('point') and \
					userPoint >= 0:
					pointsSysData.append({ 'id': indicator.get('id'), 'point': userPoint })
				else:
					errorInputIndex.append(pointsUserData[index].get('id'))
					valid = False
			if not valid:
				return {
					'error': errorInputIndex
				}
			vals.update({
				'leader_total': totalPoint, 
				'leader_points': json.dumps(pointsSysData)
			})
		d = datetime.datetime.strptime(vals['date_from'], '%Y-%m-%d')
		vals.update({'year': d.year})
		res = super(EvalTrialTicket, self).create(vals)
		return res.id

