# -*- coding: utf-8 -*-
try:
	import json
except ImportError:
	import simplejson as json
from odoo import http
import os


class ExcelExport(http.Controller):
	def effect_tickets_data(self, domain):
		records = http.request.env['eval.effect.ticket'].sudo().search(domain)
		tmp_path = os.path.abspath(__file__ + "/../../excel_templates")
		file_path = os.path.join(tmp_path, 'effect_ticket.xlsx')
		from openpyxl import load_workbook
		from openpyxl.writer.excel import save_virtual_workbook
		from openpyxl.styles import Border, Side, Alignment, NamedStyle
		wb = load_workbook(file_path)
		ws = wb.active

		row = 3
		thin_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
		alignment = Alignment(horizontal='right', vertical='center', text_rotation=0, wrap_text=False, shrink_to_fit=False, indent=0)
		normal = NamedStyle('normal')
		normal.border = thin_border
		import copy
		currency = copy.copy(normal)
		currency.alignment = alignment
		
		for i, r in enumerate(records):
			ws.cell(row=row, column=1).value, ws.cell(row=row, column=1).border = i + 1, thin_border
			ws.cell(row=row, column=2).value, ws.cell(row=row, column=2).border = r.year, thin_border
			ws.cell(row=row, column=3).value, ws.cell(row=row, column=3).border = r.month, thin_border
			ws.cell(row=row, column=4).value, ws.cell(row=row, column=4).border = r.user_name, thin_border
			ws.cell(row=row, column=5).value, ws.cell(row=row, column=5).border = r.create_uid.name, thin_border
			ws.cell(row=row, column=6).value, ws.cell(row=row, column=6).border = r.leader_total, thin_border
			ws.cell(row=row, column=7).value, ws.cell(row=row, column=7).border = r.create_date, thin_border
			ws.cell(row=row, column=8).value, ws.cell(row=row, column=8).border = r.template_id.name or '', thin_border
			ws.cell(row=row, column=9).value, ws.cell(row=row, column=9).border = r.template_ver or '', thin_border
			ws.cell(row=row, column=10).value, ws.cell(row=row, column=10).border = r.company.name or '', thin_border
			ws.cell(row=row, column=11).value, ws.cell(row=row, column=11).border = r.user_code or '', thin_border
			ws.cell(row=row, column=12).value, ws.cell(row=row, column=12).border = r.position_id.name or '', thin_border
			ws.cell(row=row, column=13).value, ws.cell(row=row, column=13).border = r.quota_id or '', thin_border
			ws.cell(row=row, column=14).value, ws.cell(row=row, column=14).border = r.number or '', thin_border
			ws.cell(row=row, column=15).value, ws.cell(row=row, column=15).border = r.address or '', thin_border
			ws.cell(row=row, column=16).value, ws.cell(row=row, column=16).border = r.note or '', thin_border
			row += 1

		return save_virtual_workbook(wb)

	@http.route('/web/export/effect_tickets_excel_export', type='http', auth="user")
	def effect_tickets_export(self, req, data, token):
		data = json.loads(data)
		return req.make_response(
			self.effect_tickets_data(
				data.get('domain', [])
			),
			headers=[
				('Content-Disposition', 'attachment; filename="tickets_export.xlsx"')
			],
			cookies={'fileToken': token}
		)

	def trial_tickets_data(self, domain):
		records = http.request.env['eval.trial.ticket'].sudo().search(domain)
		tmp_path = os.path.abspath(__file__ + "/../../excel_templates")
		file_path = os.path.join(tmp_path, 'trial_ticket.xlsx')
		from openpyxl import load_workbook
		from openpyxl.writer.excel import save_virtual_workbook
		from openpyxl.styles import Border, Side, Alignment, NamedStyle
		wb = load_workbook(file_path)
		ws = wb.active

		row = 3
		thin_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
		alignment = Alignment(horizontal='right', vertical='center', text_rotation=0, wrap_text=False, shrink_to_fit=False, indent=0)
		normal = NamedStyle('normal')
		normal.border = thin_border
		import copy
		currency = copy.copy(normal)
		currency.alignment = alignment
		
		for i, r in enumerate(records):
			ws.cell(row=row,  column=1).value, ws.cell(row=row, column=1).border = i + 1, thin_border
			ws.cell(row=row,  column=2).value, ws.cell(row=row, column=2).border = r.user, thin_border
			ws.cell(row=row,  column=3).value, ws.cell(row=row, column=3).border = r.date_from or '', thin_border
			ws.cell(row=row,  column=4).value, ws.cell(row=row, column=4).border = r.date_to or '', thin_border
			ws.cell(row=row,  column=5).value, ws.cell(row=row, column=5).border = r.year or '', thin_border
			ws.cell(row=row,  column=6).value, ws.cell(row=row, column=6).border = r.position_id or '', thin_border
			ws.cell(row=row,  column=7).value, ws.cell(row=row, column=7).border = r.create_uid.name, thin_border
			ws.cell(row=row,  column=8).value, ws.cell(row=row, column=8).border = r.quota_id or '', thin_border
			ws.cell(row=row,  column=9).value, ws.cell(row=row, column=9).border = r.quota_number or '', thin_border
			ws.cell(row=row, column=10).value, ws.cell(row=row, column=10).border = 'v' if r.manager_approved else '', thin_border
			ws.cell(row=row, column=11).value, ws.cell(row=row, column=11).border = r.template_id.name, thin_border
			ws.cell(row=row, column=12).value, ws.cell(row=row, column=12).border = r.template_ver, thin_border
			ws.cell(row=row, column=13).value, ws.cell(row=row, column=13).border = r.leader_total or '', thin_border
			ws.cell(row=row, column=14).value, ws.cell(row=row, column=14).border = r.address or '', thin_border
			ws.cell(row=row, column=15).value, ws.cell(row=row, column=15).border = u'Tốt' if r.sum == '1' else (u'Khá' if r.sum == '2' else u'Trung bình'), thin_border
			ws.cell(row=row, column=16).value, ws.cell(row=row, column=16).border = r.note or '', thin_border
			row += 1

		return save_virtual_workbook(wb)

	@http.route('/web/export/trial_tickets_excel_export', type='http', auth="user")
	def trial_tickets_export(self, req, data, token):
		data = json.loads(data)
		return req.make_response(
			self.trial_tickets_data( data.get('domain', []) ),
			headers=[('Content-Disposition', 'attachment; filename="tickets_export.xlsx"')],
			cookies={'fileToken': token}
		)

	def incorrect_tickets_data(self, domain):
		records = http.request.env['eval.incorrect.ticket'].sudo().search(domain)
		tmp_path = os.path.abspath(__file__ + "/../../excel_templates")
		file_path = os.path.join(tmp_path, 'incorrect_ticket.xlsx')
		from openpyxl import load_workbook
		from openpyxl.writer.excel import save_virtual_workbook
		from openpyxl.styles import Border, Side, Alignment, NamedStyle
		wb = load_workbook(file_path)
		ws = wb.active

		row = 3
		thin_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
		alignment = Alignment(horizontal='right', vertical='center', text_rotation=0, wrap_text=False, shrink_to_fit=False, indent=0)
		normal = NamedStyle('normal')
		normal.border = thin_border
		import copy
		currency = copy.copy(normal)
		currency.alignment = alignment
		
		for i, r in enumerate(records):
			ws.cell(row=row,  column=1).value, ws.cell(row=row, column=1).border = i + 1, thin_border
			ws.cell(row=row,  column=2).value, ws.cell(row=row, column=2).border = r.date or '', thin_border
			ws.cell(row=row,  column=3).value, ws.cell(row=row, column=3).border = r.create_uid.name or '', thin_border
			ws.cell(row=row,  column=4).value, ws.cell(row=row, column=4).border = r.project or '', thin_border
			ws.cell(row=row,  column=5).value, ws.cell(row=row, column=5).border = r.step or '', thin_border
			ws.cell(row=row,  column=6).value, ws.cell(row=row, column=6).border = r.content or '', thin_border
			ws.cell(row=row,  column=7).value, ws.cell(row=row, column=7).border = r.number or '', thin_border
			ws.cell(row=row,  column=8).value, ws.cell(row=row, column=8).border = r.respon or '', thin_border
			ws.cell(row=row,  column=9).value, ws.cell(row=row, column=9).border = '✔' if r.is_serious == True else '', thin_border
			ws.cell(row=row,  column=10).value, ws.cell(row=row, column=10).border = '✔' if r.is_repeat == True else '', thin_border
			ws.cell(row=row,  column=11).value, ws.cell(row=row, column=11).border = r.category_id.name or '', thin_border
			ws.cell(row=row,  column=12).value, ws.cell(row=row, column=12).border = r.solution or '', thin_border
			row += 1

		return save_virtual_workbook(wb)

	@http.route('/web/export/incorrect_tickets_excel_export', type='http', auth="user")
	def incorrect_tickets_export(self, req, data, token):
		data = json.loads(data)
		return req.make_response(
			self.incorrect_tickets_data( data.get('domain', []) ),
			headers=[('Content-Disposition', 'attachment; filename="tickets_export.xlsx"')],
			cookies={'fileToken': token}
		)
