# -*- coding: utf-8 -*-
from openpyxl.utils.cell import get_column_letter
try:
	import json
except ImportError:
	import simplejson as json
from odoo import http
from odoo.http import parser as my_parser
import os
from openpyxl import load_workbook
from openpyxl.writer.excel import save_virtual_workbook
from openpyxl.styles import Border, Side
from io import BytesIO
from openpyxl.worksheet.datavalidation import DataValidation


def isfloat(x):
	try:
		float(x)
	except ValueError:
		return False
	else:
		return True

def isint(x):
	try:
		a = float(x)
		b = int(a)
	except ValueError:
		return False
	else:
		return a == b

def set_border(ws, cell_range):
	rows = ws[cell_range]
	side = Side(border_style='thin', color="FF000000")

	rows = list(rows)  # we convert iterator to list for simplicity, but it's not memory efficient solution
	max_y = len(rows) - 1  # index of the last row
	for pos_y, cells in enumerate(rows):
		max_x = len(cells) - 1  # index of the last cell
		for pos_x, cell in enumerate(cells):
			border = Border(
				left=cell.border.left,
				right=cell.border.right,
				top=cell.border.top,
				bottom=cell.border.bottom
			)
			if pos_x == 0:
				border.left = side
			if pos_x == max_x:
				border.right = side
			if pos_y == 0:
				border.top = side
			if pos_y == max_y:
				border.bottom = side

			# set new border only if it's one of the edge cells
			if pos_x == 0 or pos_x == max_x or pos_y == 0 or pos_y == max_y:
				cell.border = border


class ProjectExcelExport(http.Controller):
	
	# Xuất ds phiếu cân
	def project_shipment_weight_data(self, domain):
		fields = ['number','descx', 'datex','weight', 'write_date', 'write_uid']
		records = http.request.env['project.shipment.weight'].sudo().search_read(domain, fields)
		tmp_path = os.path.abspath(__file__ + "/../../excel_templates")
		file_path = os.path.join(tmp_path, 'project_project_shipment_weight.xlsx')
		wb = load_workbook(file_path)
		ws = wb.active
		row = 3
		thin_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
		for i, r in enumerate(records):
			ws.cell(row=row, column=1).value, ws.cell(row=row, column=1).border = i + 1, thin_border
			ws.cell(row=row, column=2).value, ws.cell(row=row, column=2).border = r['number'] or '', thin_border
			ws.cell(row=row, column=3).value, ws.cell(row=row, column=3).border = r['descx'] or '', thin_border
			ws.cell(row=row, column=4).value, ws.cell(row=row, column=4).border = r['datex'] or '', thin_border
			ws.cell(row=row, column=5).value, ws.cell(row=row, column=5).border = r['weight'] or '', thin_border
			ws.cell(row=row, column=6).value, ws.cell(row=row, column=6).border = r['write_date'] or '', thin_border
			ws.cell(row=row, column=7).value, ws.cell(row=row, column=7).border = r['write_uid'][1] if r['write_uid'] else '', thin_border
			row += 1
		return save_virtual_workbook(wb)

	@http.route('/web/export/project_shipment_weight_export', type='http', auth="user")
	def project_shipment_weight_export(self, req, data, token):
		data = json.loads(data)
		return req.make_response(
			self.project_shipment_weight_data(
				data.get('domain', []),
			),
			headers=[
				('Content-Disposition', 'attachment; filename="Projects_Shipment_Weight.xlsx"')
			],
			cookies={'fileToken': token}
		)

	# Xuất dữ liệu theo dõi sản xuất
	def project_manufact_data(self, domain):
		# Mã tổng mục
		total_id = my_parser.attr_read("project_process_total_id")
		# Lấy ds tiêu chí cần xuất ra
		crites = http.request.env['project.process.criteria'].sudo().search_read([('state', '=', '1')], ['fname'])
		crites = [x['fname'] for x in crites]

		# Lấy ds tiến trình
		process = http.request.env['project.manufact.process'].sudo().search_read([('state', '=', '1')], ['name'])
		process = [u'[%s]%s' % (x['id'], x['name']) for x in process]
		process_str = ''
		for item in process:
			process_str += item + ','

		# Lấy ds nhà máy
		factory = http.request.env['project.factory'].sudo().search_read([('state', '=', '1')], ['name'])
		factory = [u'[%s]%s' % (x['id'], x['name']) for x in factory]
		factory_str = ''
		for item in factory:
			factory_str += item + ','

		# Ds cột cần ra excel
		fields = ['name', 'unit', 'number', 'longx', 'category', 'factory', 'process', 'note', 'is_total'] + crites
		records = http.request.env['project.project.manufact'].sudo().search_read(domain, fields)
		tmp_path = os.path.abspath(__file__ + "/../../excel_templates")
		file_path = os.path.join(tmp_path, 'project_manufact1.xlsx')
		wb = load_workbook(file_path)
		ws = wb.active

		# Create a data-validation object with list validation
		dv = DataValidation(type="list", formula1='"%s"' % (process_str), allow_blank=True)
		# Add the data-validation object to the worksheet
		ws.add_data_validation(dv)
		dv.add('H5:H1048576')

		# Create a data-validation object with list validation
		dv2 = DataValidation(type="list", formula1='"%s"' % (factory_str), allow_blank=True)
		# Add the data-validation object to the worksheet
		ws.add_data_validation(dv2)
		dv2.add('G5:G1048576')
		
		# Đổ lại viền ô tên tiêu chí
		for i in range (1, 2*len(crites) + 1):
			if i % 2 == 0:
				continue
			set_border(ws, "%s%s:%s%s" % (get_column_letter(i + 8), 2, get_column_letter(i + 9), 2))
		# Đổ lại viền ô 'Ghi chú'
		set_border(ws, "%s%s:%s%s" % (get_column_letter(9 + 2*len(crites)), 2, get_column_letter(9 + 2*len(crites)), 3))
		
		row = 5
		thin_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))

		for i, r in enumerate(records):
			ws.cell(row=row, column=1).value, ws.cell(row=row, column=1).border = i + 1, thin_border
			ws.cell(row=row, column=2).value, ws.cell(row=row, column=2).border = r['name'] or '', thin_border
			ws.cell(row=row, column=3).value, ws.cell(row=row, column=3).border = r['unit'] or '', thin_border
			ws.cell(row=row, column=4).value, ws.cell(row=row, column=4).border = r['number'] if r['number'] and r['process'] and r['process'][0] != total_id else '', thin_border
			ws.cell(row=row, column=5).value, ws.cell(row=row, column=5).border = r['longx'] or '', thin_border
			ws.cell(row=row, column=6).value, ws.cell(row=row, column=6).border = r['category'] or '', thin_border
			ws.cell(row=row, column=7).value, ws.cell(row=row, column=7).border = '[%s]%s' % (r['factory'][0], r['factory'][1]) if r['factory'] else '', thin_border
			ws.cell(row=row, column=8).value, ws.cell(row=row, column=8).border = u'[%s]%s' % (r['process'][0], r['process'][1]) if r['process'] else '', thin_border
			sub_col = 9
			for c in crites:
				if r['process'] and r['process'][0] != total_id:
					ws.cell(row=row, column=sub_col).value, ws.cell(row=row, column=sub_col).border = r[c] or 0, thin_border
					ws.cell(row=row, column=sub_col + 1).value, ws.cell(row=row, column=sub_col + 1).border, ws.cell(row=row, column=sub_col + 1).number_format = '=IF(D%s<>0, %s%s/D%s, 0)' % (row, get_column_letter(sub_col), row, row) if r['number'] else 0, thin_border, '0%'
				else:
					ws.cell(row=row, column=sub_col).border = thin_border
					ws.cell(row=row, column=sub_col + 1).border = thin_border
				sub_col += 2
			ws.cell(row=row, column=sub_col).value, ws.cell(row=row, column=sub_col).border = r['note'] or '', thin_border
			row += 1

		for i in range(len(records) + 1, 1000):
			ws.cell(row=row, column=1).value, ws.cell(row=row, column=1).border = i, thin_border
			ws.cell(row=row, column=2).border = thin_border
			ws.cell(row=row, column=3).border = thin_border
			ws.cell(row=row, column=4).border = thin_border
			ws.cell(row=row, column=5).border = thin_border
			ws.cell(row=row, column=6).border = thin_border
			ws.cell(row=row, column=7).border = thin_border
			ws.cell(row=row, column=8).value, ws.cell(row=row, column=8).border = '', thin_border
			sub_col = 9
			for c in crites:
				ws.cell(row=row, column=sub_col).border = thin_border
				ws.cell(row=row, column=sub_col + 1).value, ws.cell(row=row, column=sub_col + 1).border, ws.cell(row=row, column=sub_col + 1).number_format = '=IF(D%s<>0, %s%s/D%s, 0)' % (row, get_column_letter(sub_col), row, row), thin_border, '0%'
				sub_col += 2
			ws.cell(row=row, column=sub_col).border = thin_border
			row += 1
		return save_virtual_workbook(wb)
	@http.route('/web/export/project_manufact_excel_export', type='http', auth="user")
	def project_manufact_export(self, req, data, token):
		data = json.loads(data)
		return req.make_response(
			self.project_manufact_data(
				data.get('domain', [])
			),
			headers=[
				('Content-Disposition', 'attachment; filename="Projects_Manufact.xlsx"')
			],
			cookies={'fileToken': token}
		)
	
	# Xuất mẫu nhập theo dõi sản xuất
	def project_manufact_template(self, number):
		# Lấy ds tiêu chí cần xuất ra
		crites = http.request.env['project.process.criteria'].sudo().search_read([('state', '=', '1')], ['fname'])
		crites = [x['fname'] for x in crites]
		# Lấy ds tiến trình
		process = http.request.env['project.manufact.process'].sudo().search_read([('state', '=', '1')], ['name'])
		process = [u'[%s]%s' % (x['id'], x['name']) for x in process]
		process_str = ''
		for item in process:
			process_str += item + ','

		# Lấy ds nhà máy
		factory = http.request.env['project.factory'].sudo().search_read([('state', '=', '1')], ['name'])
		factory = [u'[%s]%s' % (x['id'], x['name']) for x in factory]
		factory_str = ''
		for item in factory:
			factory_str += item + ','

		tmp_path = os.path.abspath(__file__ + "/../../excel_templates")
		file_path = os.path.join(tmp_path, 'project_manufact1.xlsx')
		wb = load_workbook(file_path)
		ws = wb.active

		# Create a data-validation object with list validation
		dv = DataValidation(type="list", formula1='"%s"' % (process_str), allow_blank=True)

		# Add the data-validation object to the worksheet
		ws.add_data_validation(dv)
		dv.add('H5:H%s' %(4 + number))

		# Create a data-validation object with list validation
		dv2 = DataValidation(type="list", formula1='"%s"' % (factory_str), allow_blank=True)
		# Add the data-validation object to the worksheet
		ws.add_data_validation(dv2)
		dv2.add('G5:G1048576')
		
		# Đổ lại viền ô tên tiêu chí
		for i in range (1, 2*len(crites) + 1):
			if i % 2 == 0:
				continue
			set_border(ws, "%s%s:%s%s" % (get_column_letter(i + 8), 2, get_column_letter(i + 9), 2))
		# Đổ lại viền ô 'Ghi chú'
		set_border(ws, "%s%s:%s%s" % (get_column_letter(9 + 2*len(crites)), 2, get_column_letter(9 + 2*len(crites)), 3))
		# Dòng ghi chú hướng dẫn nhập liệu
		ws['A4'].value = u"Dữ liệu nhập vào phải đúng theo cột để tránh lỗi trong quá trình đọc vào hệ thống. Nhập từ dòng dưới, không bỏ trống dòng!"
		
		row = 5
		thin_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
		
		for i in range(0, number):
			ws.cell(row=row, column=1).value, ws.cell(row=row, column=1).border = i + 1, thin_border
			ws.cell(row=row, column=2).border = thin_border
			ws.cell(row=row, column=3).border = thin_border
			ws.cell(row=row, column=4).border = thin_border
			ws.cell(row=row, column=5).border = thin_border
			ws.cell(row=row, column=6).border = thin_border
			ws.cell(row=row, column=7).border = thin_border
			ws.cell(row=row, column=8).value, ws.cell(row=row, column=8).border = '', thin_border
			sub_col = 9
			for c in crites:
				ws.cell(row=row, column=sub_col).border = thin_border
				ws.cell(row=row, column=sub_col + 1).value, ws.cell(row=row, column=sub_col + 1).border, ws.cell(row=row, column=sub_col + 1).number_format = '=%s%s/D%s' % (get_column_letter(sub_col), row, row), thin_border, '0%'
				sub_col += 2
			ws.cell(row=row, column=sub_col).border = thin_border
			row += 1
		return save_virtual_workbook(wb)
	@http.route('/web/export/project_manufact_template_excel_export', type='http', auth="user")
	def project_manufact_template_export(self, req, data, token):
		data = json.loads(data)
		return req.make_response(
			self.project_manufact_template(
				data.get('number_of_rows', 0)
			),
			headers=[
				('Content-Disposition', 'attachment; filename="Projects_Manufact_Template.xlsx"')
			],
			cookies={'fileToken': token}
		)

	# Nhập liệu theo dõi sản xuất
	@http.route('/web/project/project_import_excel', type='http', auth="user")
	def project_import_excel(self, req, **kwargs):
		# Nhận dữ liệu gửi lên
		filex = None
		pid = None
		for field_name, field_value in kwargs.items():
			if field_name == 'file':
				filex = field_value
			elif field_name == 'pid':
				pid = int(field_value)
		# Xử lý dữ liệu
		if filex and pid:
			try:
				wb = load_workbook(filename=BytesIO(filex.read()))
			except:
				return req.make_response("-1")
			ws = wb.active
			# Lấy ds tiêu chí cần nhập vào
			crites = http.request.env['project.process.criteria'].sudo().search_read([('state', '=', '1')], ['fname'])
			crites = [x['fname'] for x in crites]
			# Lấy ds tiến trình
			#process = http.request.env['project.manufact.process'].sudo().search([('state', '=', '1')])
			# Khớp tiến trình và tiêu chí tiến trình
			process_crites_rel = http.parser.attr_read("project_process_crite_rel")
			# Id của tiến trình 'Tổng mục'
			process_total_id = http.parser.attr_read("project_process_total_id")
			# Dòng đầu tiên
			row0 = 5
			# Mảng chứa các bản ghi sẽ tạo
			to_import_rows = []
			# Đọc từng dòng
			while True:
				vals = {}
				# Đọc tên
				name = ws.cell(row=row0, column=2).value
				if name is not None:
					vals['name'] = str(name)
				# Đọc đơn vị
				unit = ws.cell(row=row0, column=3).value
				if unit is not None:
					vals['unit'] = str(unit)
				# Đọc số lượng
				number = ws.cell(row=row0, column=4).value
				if number is not None:
					number = str(number)
					if isint(number):
						vals['number'] = int(number)
				# Đọc độ dài
				longx = ws.cell(row=row0, column=5).value
				if longx is not None:
					vals['longx'] = str(longx)
				# Đọc khu vực/hạng mục
				cat = ws.cell(row=row0, column=6).value
				if cat is not None:
					vals['category'] = str(cat)
				# Đọc nhà máy
				fact = ws.cell(row=row0, column=7).value
				fact_str = str(fact)
				if fact is not None and fact_str.count(']') and fact_str[0] == '[':
					idx = ''
					for x in fact_str:
						if x == ']':
							break
						else:
							idx += x
					if isint(idx[1:]):
						idx = int(idx[1:])
						vals['factory'] = idx
				sub_col = 8
				# Đọc tiến trình
				process = ws.cell(row=row0, column=8).value
				process_str = str(process)
				if process is not None and process_str.count(']') and process_str[0] == '[':
					idx = ''
					for x in process_str:
						if x == ']':
							break
						else:
							idx += x
					if isint(idx[1:]):
						idx = int(idx[1:])
						vals['process'] = idx
						# Nếu là tổng mục
						if idx == process_total_id:
							vals['is_total'] = True
						else:
							# Đọc các chỉ tiêu nếu tiến trình đã xác định và không phải là tổng mục
							sub_col = 9
							for c in crites:
								# Chỉ đọc các chỉ tiêu cần thiết đối với tiến trình của nó
								idx = str(idx)
								if idx in process_crites_rel and c in process_crites_rel[idx]:
									v = str(ws.cell(row=row0, column=sub_col).value)
									if len(v) and isint(v):
										vals[c] = int(v)
								sub_col += 2
				# Đọc ghi chú
				note = ws.cell(row=row0, column=sub_col).value
				if note is not None:
					vals['note'] = str(note)

				# Kiểm tra dữ liệu hợp lệ hoặc thoát lặp
				print vals
				if 'name' not in vals or 'process' not in vals or ('number' not in vals and 'is_total' not in vals):
					break
				# Điền giá trị mặc định nếu chưa có
				vals['unit'] = vals['unit'] if 'unit' in vals else ''
				vals['longx'] = vals['longx'] if 'longx' in vals else ''
				vals['category'] = vals['category'] if 'category' in vals else ''
				vals['factory'] = vals['factory'] if 'factory' in vals else False
				vals['note'] = vals['note'] if 'note' in vals else ''
				to_import_rows.append(vals)
				# Chuyển xuống dòng tiếp dưới
				row0 += 1
			
			# Số dòng đã ghi thành công
			if len(to_import_rows):
				imported_rows = 0
				manufact_obj = http.request.env['project.project.manufact']
				# Xoá các bản ghi cũ
				manufact_obj.clean_old(pid)
				# Lấy order0 của bản ghi dưới cùng -> nhập các bản ghi mới phía sau
				# last_record = manufact_obj.search_read([('project_id', '=', pid)], ['next_seq'], limit=1, order='seq desc')
				# if len(last_record) == 1:
				# 	last_record_next_seq = last_record[0]['next_seq']
				# else:
				# 	last_record_next_seq = '0001'
				last_record_next_seq = '0001'
				for r in to_import_rows:
					# Đặt mã dự án
					r['project_id'] = pid
					# Đặt 'seq' và 'next_seq' cho bản ghi
					r['seq'] = last_record_next_seq
					# Số cộng
					add_num = '0' * (len(last_record_next_seq) - 1) + '1'
					# Cờ nhớ (tổng > 9)
					remember = False
					# Chuỗi 'next_seq'
					next_seq = ''
					# Độ dài 'seq' TRỪ 1
					L = len(last_record_next_seq) - 1
					# Thực hiện cộng để lấy 'next_seq'
					for i in range(len(last_record_next_seq)):
						x = int(last_record_next_seq[L-i]) + int(add_num[L-i])
						if remember:
							x += 1
							remember = False
						if x > 9:
							remember = True
						x = str(x)
						next_seq = x[len(x) - 1] + next_seq
					r['next_seq'] = '1' + next_seq if remember else next_seq
						
					res = manufact_obj.create(r)
					if res and res.id > 0:
						last_record_next_seq = r['next_seq']
						imported_rows += 1
				return req.make_response("%s" % (imported_rows))
			else:
				return req.make_response("0")
	
	# Xuất ds dự án
	def project_project_data(self, domain, is_internal):
		project_types = dict(http.request.env['project.project']._fields['project_type'].selection)
		if is_internal:
			fields = 'code,year,name,typex,location,project_type,project_desc,project_buyer,project_seller,chairman_str,follower_str,status_id,create_date,write_date,note'.split(',')
		else:
			fields = 'code,year,name,typex,location,investor_id,contractor_id,chairman_str,follower_str,status_id,create_date,write_date,note'.split(',')
		records = http.request.env['project.project'].sudo().search_read(domain, fields)
		tmp_path = os.path.abspath(__file__ + "/../../excel_templates")
		file_path = os.path.join(tmp_path, 'project_project_internal.xlsx') if is_internal else os.path.join(tmp_path, 'project_project_customer.xlsx')
		wb = load_workbook(file_path)
		ws = wb.active

		row = 3
		thin_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
		if is_internal:
			for i, r in enumerate(records):
				ws.cell(row=row, column=1).value, ws.cell(row=row, column=1).border = i + 1, thin_border
				ws.cell(row=row, column=2).value, ws.cell(row=row, column=2).border = r['code'] or '', thin_border
				ws.cell(row=row, column=3).value, ws.cell(row=row, column=3).border = r['year'] or '', thin_border
				ws.cell(row=row, column=4).value, ws.cell(row=row, column=4).border = r['name'] or '', thin_border
				ws.cell(row=row, column=5).value, ws.cell(row=row, column=5).border = r['typex'] or '', thin_border
				ws.cell(row=row, column=6).value, ws.cell(row=row, column=6).border = r['location'] or '', thin_border
				ws.cell(row=row, column=7).value, ws.cell(row=row, column=7).border = project_types.get(r['project_type']) if r['project_type'] else '', thin_border
				ws.cell(row=row, column=8).value, ws.cell(row=row, column=8).border = r['project_desc'] or '', thin_border
				ws.cell(row=row, column=9).value, ws.cell(row=row, column=9).border = r['project_buyer'][1] if r['project_buyer'] else '', thin_border
				ws.cell(row=row, column=10).value, ws.cell(row=row, column=10).border = r['project_seller'][1] if r['project_seller'] else '', thin_border
				ws.cell(row=row, column=11).value, ws.cell(row=row, column=11).border = r['chairman_str'] or '', thin_border
				ws.cell(row=row, column=12).value, ws.cell(row=row, column=12).border = r['follower_str'] or '', thin_border
				ws.cell(row=row, column=13).value, ws.cell(row=row, column=13).border = r['status_id'][1] if r['status_id'] else '', thin_border
				ws.cell(row=row, column=14).value, ws.cell(row=row, column=14).border = r['create_date'] or '', thin_border
				ws.cell(row=row, column=15).value, ws.cell(row=row, column=15).border = r['write_date'] or '', thin_border
				ws.cell(row=row, column=16).value, ws.cell(row=row, column=16).border = r['note'] or '', thin_border
				row += 1
		else:
			for i, r in enumerate(records):
				ws.cell(row=row, column=1).value, ws.cell(row=row, column=1).border = i + 1, thin_border
				ws.cell(row=row, column=2).value, ws.cell(row=row, column=2).border = r['code'] or '', thin_border
				ws.cell(row=row, column=3).value, ws.cell(row=row, column=3).border = r['year'] or '', thin_border
				ws.cell(row=row, column=4).value, ws.cell(row=row, column=4).border = r['name'] or '', thin_border
				ws.cell(row=row, column=5).value, ws.cell(row=row, column=5).border = r['typex'] or '', thin_border
				ws.cell(row=row, column=6).value, ws.cell(row=row, column=6).border = r['location'] or '', thin_border
				ws.cell(row=row, column=7).value, ws.cell(row=row, column=7).border = r['investor_id'][1] if r['investor_id'] else '', thin_border
				ws.cell(row=row, column=8).value, ws.cell(row=row, column=8).border = r['contractor_id'][1] if r['contractor_id'] else '', thin_border
				ws.cell(row=row, column=9).value, ws.cell(row=row, column=9).border = r['chairman_str'] or '', thin_border
				ws.cell(row=row, column=10).value, ws.cell(row=row, column=10).border = r['follower_str'] or '', thin_border
				ws.cell(row=row, column=11).value, ws.cell(row=row, column=11).border = r['status_id'][1] if r['status_id'] else '', thin_border
				ws.cell(row=row, column=12).value, ws.cell(row=row, column=12).border = r['create_date'] or '', thin_border
				ws.cell(row=row, column=13).value, ws.cell(row=row, column=13).border = r['write_date'] or '', thin_border
				ws.cell(row=row, column=14).value, ws.cell(row=row, column=14).border = r['note'] or '', thin_border
				row += 1
		return save_virtual_workbook(wb)
	@http.route('/web/export/project_project_export', type='http', auth="user")
	def project_project_export(self, req, data, token):
		data = json.loads(data)
		return req.make_response(
			self.project_project_data(
				data.get('domain', []),
				data.get('is_internal', False),
			),
			headers=[
				('Content-Disposition', 'attachment; filename="Projects_Project.xlsx"')
			],
			cookies={'fileToken': token}
		)
		
	
	# Xuất ds thanh toán
	def project_pay_data(self, domain, is_internal):
		project_types = dict(http.request.env['project.project']._fields['project_type'].selection)
		if is_internal:
			fields = 'code,year,project_type,name,project_desc,project_buyer,project_seller,follower_str,status_id,pay_status_id,amount,paid,wait,write_date,note'.split(',')
		else:
			fields = 'code,year,name,typex,investor_fname,contractor_fname,chairman_str,follower_str,status_id,pay_status_id,amount,paid,wait,write_date,note'.split(',')
		records = http.request.env['project.project'].sudo().search_read(domain, fields)
		tmp_path = os.path.abspath(__file__ + "/../../excel_templates")
		file_path = os.path.join(tmp_path, 'project_pay_internal.xlsx') if is_internal else os.path.join(tmp_path, 'project_pay_customer.xlsx')
		wb = load_workbook(file_path)
		ws = wb.active

		row = 3
		thin_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
		if is_internal:
			for i, r in enumerate(records):
				ws.cell(row=row, column=1).value, ws.cell(row=row, column=1).border = i + 1, thin_border
				ws.cell(row=row, column=2).value, ws.cell(row=row, column=2).border = r['code'] or '', thin_border
				ws.cell(row=row, column=3).value, ws.cell(row=row, column=3).border = r['year'] or '', thin_border
				ws.cell(row=row, column=4).value, ws.cell(row=row, column=4).border = project_types.get(r['project_type']) if r['project_type'] else '', thin_border
				ws.cell(row=row, column=5).value, ws.cell(row=row, column=5).border = r['name'] or '', thin_border
				ws.cell(row=row, column=6).value, ws.cell(row=row, column=6).border = r['project_desc'] or '', thin_border
				ws.cell(row=row, column=7).value, ws.cell(row=row, column=7).border = r['project_buyer'][1] if r['project_buyer'] else '', thin_border
				ws.cell(row=row, column=8).value, ws.cell(row=row, column=8).border = r['project_seller'][1] if r['project_seller'] else '', thin_border
				ws.cell(row=row, column=9).value, ws.cell(row=row, column=9).border = r['follower_str'] or '', thin_border
				ws.cell(row=row, column=10).value, ws.cell(row=row, column=10).border = r['status_id'][1] if r['status_id'] else '', thin_border
				ws.cell(row=row, column=11).value, ws.cell(row=row, column=11).border = r['pay_status_id'][1] if r['pay_status_id'] else '', thin_border
				ws.cell(row=row, column=12).value, ws.cell(row=row, column=12).border = r['amount'] or '', thin_border
				ws.cell(row=row, column=13).value, ws.cell(row=row, column=13).border = r['paid'] or '', thin_border
				ws.cell(row=row, column=14).value, ws.cell(row=row, column=14).border = r['wait'] or '', thin_border
				ws.cell(row=row, column=15).value, ws.cell(row=row, column=15).border = r['write_date'] or '', thin_border
				ws.cell(row=row, column=16).value, ws.cell(row=row, column=16).border = r['note'] or '', thin_border
				row += 1
		else:
			for i, r in enumerate(records):
				ws.cell(row=row, column=1).value, ws.cell(row=row, column=1).border = i + 1, thin_border
				ws.cell(row=row, column=2).value, ws.cell(row=row, column=2).border = r['code'] or '', thin_border
				ws.cell(row=row, column=3).value, ws.cell(row=row, column=3).border = r['year'] or '', thin_border
				ws.cell(row=row, column=4).value, ws.cell(row=row, column=4).border = r['name'] or '', thin_border
				ws.cell(row=row, column=5).value, ws.cell(row=row, column=5).border = r['typex'] or '', thin_border
				ws.cell(row=row, column=6).value, ws.cell(row=row, column=6).border = r['investor_fname'] or '', thin_border
				ws.cell(row=row, column=7).value, ws.cell(row=row, column=7).border = r['contractor_fname'] or '', thin_border
				ws.cell(row=row, column=8).value, ws.cell(row=row, column=8).border = r['chairman_str'] or '', thin_border
				ws.cell(row=row, column=9).value, ws.cell(row=row, column=9).border = r['follower_str'] or '', thin_border
				ws.cell(row=row, column=10).value, ws.cell(row=row, column=10).border = r['status_id'][1] if r['status_id'] else '', thin_border
				ws.cell(row=row, column=11).value, ws.cell(row=row, column=11).border = r['pay_status_id'][1] if r['pay_status_id'] else '', thin_border
				ws.cell(row=row, column=12).value, ws.cell(row=row, column=12).border = r['amount'] or '', thin_border
				ws.cell(row=row, column=13).value, ws.cell(row=row, column=13).border = r['paid'] or '', thin_border
				ws.cell(row=row, column=14).value, ws.cell(row=row, column=14).border = r['wait'] or '', thin_border
				ws.cell(row=row, column=15).value, ws.cell(row=row, column=15).border = r['write_date'] or '', thin_border
				ws.cell(row=row, column=16).value, ws.cell(row=row, column=16).border = r['note'] or '', thin_border
				row += 1
		return save_virtual_workbook(wb)
	@http.route('/web/export/project_pay_export', type='http', auth="user")
	def project_pay_export(self, req, data, token):
		data = json.loads(data)
		return req.make_response(
			self.project_pay_data(
				data.get('domain', []),
				data.get('is_internal', False),
			),
			headers=[
				('Content-Disposition', 'attachment; filename="Projects_Pay.xlsx"')
			],
			cookies={'fileToken': token}
		)
		
	
	# Xuất ds Phiếu giao việc
	def project_project_job_data(self, domain):
		fields = 'create_uid,typex,create_date,write_uid,write_date'.split(',')
		records = http.request.env['project.project.job'].sudo().search_read(domain, fields)
		tmp_path = os.path.abspath(__file__ + "/../../excel_templates")
		file_path = os.path.join(tmp_path, 'project_project_job.xlsx')
		wb = load_workbook(file_path)
		ws = wb.active

		row = 3
		thin_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
		for i, r in enumerate(records):
			ws.cell(row=row, column=1).value, ws.cell(row=row, column=1).border = i + 1, thin_border
			ws.cell(row=row, column=2).value, ws.cell(row=row, column=2).border = r['create_uid'][1] if r['create_uid'] else '', thin_border
			ws.cell(row=row, column=3).value, ws.cell(row=row, column=3).border = r['typex'] or '', thin_border
			ws.cell(row=row, column=4).value, ws.cell(row=row, column=4).border = r['create_date'] or '', thin_border
			ws.cell(row=row, column=5).value, ws.cell(row=row, column=5).border = r['write_uid'][1] if r['write_uid'] else '', thin_border
			ws.cell(row=row, column=6).value, ws.cell(row=row, column=6).border = r['write_date'] or '', thin_border
			row += 1
		return save_virtual_workbook(wb)
	@http.route('/web/export/project_project_job_export', type='http', auth="user")
	def project_project_job_export(self, req, data, token):
		data = json.loads(data)
		return req.make_response(
			self.project_project_job_data(
				data.get('domain', []),
			),
			headers=[
				('Content-Disposition', 'attachment; filename="Projects_Job.xlsx"')
			],
			cookies={'fileToken': token}
		)
	
	# Xuất ds Chi phí công trường
	def project_project_costs_data(self, domain):
		fields = 'descx,datex,income,outcome,bill_num,bill_note,create_uid,approved_uid'.split(',')
		records = http.request.env['project.project.costs'].sudo().search_read(domain, fields)
		tmp_path = os.path.abspath(__file__ + "/../../excel_templates")
		file_path = os.path.join(tmp_path, 'project_project_costs.xlsx')
		wb = load_workbook(file_path)
		ws = wb.active

		row = 3
		thin_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
		for i, r in enumerate(records):
			ws.cell(row=row, column=1).value, ws.cell(row=row, column=1).border = i + 1, thin_border
			ws.cell(row=row, column=2).value, ws.cell(row=row, column=2).border = r['descx'] or '', thin_border
			ws.cell(row=row, column=3).value, ws.cell(row=row, column=3).border = r['datex'] or '', thin_border
			ws.cell(row=row, column=4).value, ws.cell(row=row, column=4).border = r['income'] or '', thin_border
			ws.cell(row=row, column=5).value, ws.cell(row=row, column=5).border = r['outcome'] or '', thin_border
			ws.cell(row=row, column=6).value, ws.cell(row=row, column=6).border = r['bill_num'] or '', thin_border
			ws.cell(row=row, column=7).value, ws.cell(row=row, column=7).border = r['bill_note'] or '', thin_border
			ws.cell(row=row, column=8).value, ws.cell(row=row, column=8).border = r['create_uid'][1] if r['create_uid'] else '', thin_border
			ws.cell(row=row, column=9).value, ws.cell(row=row, column=9).border = r['approved_uid'] or '', thin_border
			row += 1
		return save_virtual_workbook(wb)
	@http.route('/web/export/project_project_costs_export', type='http', auth="user")
	def project_project_costs_export(self, req, data, token):
		data = json.loads(data)
		return req.make_response(
			self.project_project_costs_data(
				data.get('domain', []),
			),
			headers=[
				('Content-Disposition', 'attachment; filename="Projects_Costs.xlsx"')
			],
			cookies={'fileToken': token}
		)
	
	# Xuất ds Xuất hàng
	def project_project_shipment_data(self, domain):
		fields = 'number,datex,transport,car_plate,driver,driver_phone,fromx,to,descx,location,write_uid,write_date'.split(',')
		records = http.request.env['project.project.shipment'].sudo().search_read(domain, fields)
		tmp_path = os.path.abspath(__file__ + "/../../excel_templates")
		file_path = os.path.join(tmp_path, 'project_project_shipment.xlsx')
		wb = load_workbook(file_path)
		ws = wb.active

		row = 3
		thin_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
		for i, r in enumerate(records):
			ws.cell(row=row, column=1).value, ws.cell(row=row, column=1).border = i + 1, thin_border
			ws.cell(row=row, column=2).value, ws.cell(row=row, column=2).border = r['number'] or '', thin_border
			ws.cell(row=row, column=3).value, ws.cell(row=row, column=3).border = r['datex'] or '', thin_border
			ws.cell(row=row, column=4).value, ws.cell(row=row, column=4).border = r['transport'] or '', thin_border
			ws.cell(row=row, column=5).value, ws.cell(row=row, column=5).border = r['car_plate'] or '', thin_border
			ws.cell(row=row, column=6).value, ws.cell(row=row, column=6).border = r['driver'] or '', thin_border
			ws.cell(row=row, column=7).value, ws.cell(row=row, column=7).border = r['driver_phone'] or '', thin_border
			ws.cell(row=row, column=8).value, ws.cell(row=row, column=8).border = r['fromx'][1] if r['fromx'] else '', thin_border
			ws.cell(row=row, column=9).value, ws.cell(row=row, column=9).border = r['to'] or '', thin_border
			ws.cell(row=row, column=10).value, ws.cell(row=row, column=10).border = r['descx'] or '', thin_border
			ws.cell(row=row, column=11).value, ws.cell(row=row, column=11).border = r['location'] or '', thin_border
			ws.cell(row=row, column=12).value, ws.cell(row=row, column=12).border = r['write_uid'][1] if r['write_uid'] else '', thin_border
			ws.cell(row=row, column=13).value, ws.cell(row=row, column=13).border = r['write_date'] or '', thin_border
			row += 1
		return save_virtual_workbook(wb)
	@http.route('/web/export/project_project_shipment_export', type='http', auth="user")
	def project_project_shipment_export(self, req, data, token):
		data = json.loads(data)
		return req.make_response(
			self.project_project_shipment_data(
				data.get('domain', []),
			),
			headers=[
				('Content-Disposition', 'attachment; filename="Projects_Shipment.xlsx"')
			],
			cookies={'fileToken': token}
		)

	# Xuất ds Quy cách vật liệu
	def project_project_material_data(self, domain):
		fields = 'material,style,category,original,color,write_uid,write_date,state,approved_date'.split(',')
		records = http.request.env['project.project.material'].sudo().search_read(domain, fields)
		tmp_path = os.path.abspath(__file__ + "/../../excel_templates")
		file_path = os.path.join(tmp_path, 'project_project_material.xlsx')
		wb = load_workbook(file_path)
		ws = wb.active

		row = 3
		thin_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
		for i, r in enumerate(records):
			ws.cell(row=row, column=1).value, ws.cell(row=row, column=1).border = i + 1, thin_border
			ws.cell(row=row, column=2).value, ws.cell(row=row, column=2).border = r['material'] or '', thin_border
			ws.cell(row=row, column=3).value, ws.cell(row=row, column=3).border = r['style'] or '', thin_border
			ws.cell(row=row, column=4).value, ws.cell(row=row, column=4).border = r['category'] or '', thin_border
			ws.cell(row=row, column=5).value, ws.cell(row=row, column=5).border = r['original'] or '', thin_border
			ws.cell(row=row, column=6).value, ws.cell(row=row, column=6).border = r['color'] or '', thin_border
			ws.cell(row=row, column=7).value, ws.cell(row=row, column=7).border = r['write_uid'][1] if r['write_uid'] else '', thin_border
			ws.cell(row=row, column=8).value, ws.cell(row=row, column=8).border = r['write_date'] or '', thin_border
			ws.cell(row=row, column=9).value, ws.cell(row=row, column=9).border = r['state'] or '', thin_border
			ws.cell(row=row, column=10).value, ws.cell(row=row, column=10).border = r['approved_date'] or '', thin_border
			row += 1
		return save_virtual_workbook(wb)
	@http.route('/web/export/project_project_material_export', type='http', auth="user")
	def project_project_material_export(self, req, data, token):
		data = json.loads(data)
		return req.make_response(
			self.project_project_material_data(
				data.get('domain', []),
			),
			headers=[
				('Content-Disposition', 'attachment; filename="Projects_Material.xlsx"')
			],
			cookies={'fileToken': token}
		)

	# Xuất ds Bản vẽ
	def project_project_drawing_data(self, domain):
		fields = 'name,typex,category,version,version_date,state,write_uid,write_date,note'.split(',')
		records = http.request.env['project.project.drawing'].sudo().search_read(domain, fields)
		tmp_path = os.path.abspath(__file__ + "/../../excel_templates")
		file_path = os.path.join(tmp_path, 'project_project_drawing.xlsx')
		wb = load_workbook(file_path)
		ws = wb.active

		row = 3
		thin_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
		for i, r in enumerate(records):
			ws.cell(row=row, column=1).value, ws.cell(row=row, column=1).border = i + 1, thin_border
			ws.cell(row=row, column=2).value, ws.cell(row=row, column=2).border = r['name'] or '', thin_border
			ws.cell(row=row, column=3).value, ws.cell(row=row, column=3).border = r['typex'] or '', thin_border
			ws.cell(row=row, column=4).value, ws.cell(row=row, column=4).border = r['category'] or '', thin_border
			ws.cell(row=row, column=5).value, ws.cell(row=row, column=5).border = r['version'] or '', thin_border
			ws.cell(row=row, column=6).value, ws.cell(row=row, column=6).border = r['version_date'] or '', thin_border
			ws.cell(row=row, column=7).value, ws.cell(row=row, column=7).border = r['state'] or '', thin_border
			ws.cell(row=row, column=8).value, ws.cell(row=row, column=8).border = r['write_uid'][1] if r['write_uid'] else '', thin_border
			ws.cell(row=row, column=9).value, ws.cell(row=row, column=9).border = r['write_date'] or '', thin_border
			ws.cell(row=row, column=10).value, ws.cell(row=row, column=10).border = r['note'] or '', thin_border
			row += 1
		return save_virtual_workbook(wb)
	@http.route('/web/export/project_project_drawing_export', type='http', auth="user")
	def project_project_drawing_export(self, req, data, token):
		data = json.loads(data)
		return req.make_response(
			self.project_project_drawing_data(
				data.get('domain', []),
			),
			headers=[
				('Content-Disposition', 'attachment; filename="Projects_Drawing.xlsx"')
			],
			cookies={'fileToken': token}
		)

	# Xuất ds Chi tiết thanh toán
	def project_pay_detail_data(self, domain, is_internal):
		fields = 'descx,contract,chairman_str,follower_str,bill_date,value,paid,bill,note'.split(',')
		records = http.request.env['project.project.paydetail'].sudo().search_read(domain, fields)
		tmp_path = os.path.abspath(__file__ + "/../../excel_templates")
		file_path = os.path.join(tmp_path, 'project_pay_details_internal.xlsx') if is_internal else os.path.join(tmp_path, 'project_pay_details_customer.xlsx')
		wb = load_workbook(file_path)
		ws = wb.active

		row = 3
		thin_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
		if is_internal:
			for i, r in enumerate(records):
				ws.cell(row=row, column=1).value, ws.cell(row=row, column=1).border = i + 1, thin_border
				ws.cell(row=row, column=2).value, ws.cell(row=row, column=2).border = r['descx'] or '', thin_border
				ws.cell(row=row, column=3).value, ws.cell(row=row, column=3).border = r['contract'] or '', thin_border
				ws.cell(row=row, column=4).value, ws.cell(row=row, column=4).border = r['follower_str'] or '', thin_border
				ws.cell(row=row, column=5).value, ws.cell(row=row, column=5).border = r['bill_date'] or '', thin_border
				ws.cell(row=row, column=6).value, ws.cell(row=row, column=6).border = r['value'] or '', thin_border
				ws.cell(row=row, column=7).value, ws.cell(row=row, column=7).border = r['paid'] or '', thin_border
				ws.cell(row=row, column=8).value, ws.cell(row=row, column=8).border = r['bill'] or '', thin_border
				ws.cell(row=row, column=9).value, ws.cell(row=row, column=9).border = r['note'] or '', thin_border
				row += 1
		else:
			for i, r in enumerate(records):
				ws.cell(row=row, column=1).value, ws.cell(row=row, column=1).border = i + 1, thin_border
				ws.cell(row=row, column=2).value, ws.cell(row=row, column=2).border = r['descx'] or '', thin_border
				ws.cell(row=row, column=3).value, ws.cell(row=row, column=3).border = r['contract'] or '', thin_border
				ws.cell(row=row, column=4).value, ws.cell(row=row, column=4).border = r['chairman_str'] or '', thin_border
				ws.cell(row=row, column=5).value, ws.cell(row=row, column=5).border = r['follower_str'] or '', thin_border
				ws.cell(row=row, column=6).value, ws.cell(row=row, column=6).border = r['bill_date'] or '', thin_border
				ws.cell(row=row, column=7).value, ws.cell(row=row, column=7).border = r['value'] or '', thin_border
				ws.cell(row=row, column=8).value, ws.cell(row=row, column=8).border = r['paid'] or '', thin_border
				ws.cell(row=row, column=9).value, ws.cell(row=row, column=9).border = r['bill'] or '', thin_border
				ws.cell(row=row, column=10).value, ws.cell(row=row, column=10).border = r['note'] or '', thin_border
				row += 1
		return save_virtual_workbook(wb)
	@http.route('/web/export/project_pay_detail_export', type='http', auth="user")
	def project_pay_detail_export(self, req, data, token):
		data = json.loads(data)
		return req.make_response(
			self.project_pay_detail_data(
				data.get('domain', []),
				data.get('is_internal', False)
			),
			headers=[
				('Content-Disposition', 'attachment; filename="Projects_Pay_Details.xlsx"')
			],
			cookies={'fileToken': token}
		)

	# Xuất ds Dự kiến thanh toán
	def project_pay_expected_data(self, domain):
		fields = 'descx,follower,expected_date,expecten_pay,pay_date,pay_value,bill,note,pay_status'.split(',')
		records = http.request.env['project.project.payexpected'].sudo().search_read(domain, fields)
		tmp_path = os.path.abspath(__file__ + "/../../excel_templates")
		file_path = os.path.join(tmp_path, 'project_pay_expected.xlsx')
		wb = load_workbook(file_path)
		ws = wb.active

		row = 3
		thin_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
		for i, r in enumerate(records):
			ws.cell(row=row, column=1).value, ws.cell(row=row, column=1).border = i + 1, thin_border
			ws.cell(row=row, column=2).value, ws.cell(row=row, column=2).border = r['descx'] or '', thin_border
			ws.cell(row=row, column=3).value, ws.cell(row=row, column=3).border = r['follower'][1] if r['follower'] else '', thin_border
			ws.cell(row=row, column=4).value, ws.cell(row=row, column=4).border = r['expected_date'] or '', thin_border
			ws.cell(row=row, column=5).value, ws.cell(row=row, column=5).border = r['expecten_pay'] or '', thin_border
			ws.cell(row=row, column=6).value, ws.cell(row=row, column=6).border = r['pay_date'] or '', thin_border
			ws.cell(row=row, column=7).value, ws.cell(row=row, column=7).border = r['pay_value'] or '', thin_border
			ws.cell(row=row, column=8).value, ws.cell(row=row, column=8).border = r['bill'] or '', thin_border
			ws.cell(row=row, column=9).value, ws.cell(row=row, column=9).border = r['note'] or '', thin_border
			ws.cell(row=row, column=10).value, ws.cell(row=row, column=10).border = r['pay_status'] or '', thin_border
			row += 1
		return save_virtual_workbook(wb)
	@http.route('/web/export/project_pay_expected_export', type='http', auth="user")
	def project_pay_expected_export(self, req, data, token):
		data = json.loads(data)
		return req.make_response(
			self.project_pay_expected_data(
				data.get('domain', [])
			),
			headers=[
				('Content-Disposition', 'attachment; filename="Projects_Pay_Expected.xlsx"')
			],
			cookies={'fileToken': token}
		)
