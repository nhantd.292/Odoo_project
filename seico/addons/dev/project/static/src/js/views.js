odoo.define('project.views', function (require) {
"use strict";
var core = require('web.core'),
    View = require('web.View'),
    FormView = require('web.FormView'),
    ListView = require('web.ListView'),
    Model = require('web.DataModel'),
    ActionManager = require('web.ActionManager'),
    Pager = require('web.Pager'),
    framework = require('web.framework'),
    form_widgets = require('web.form_widgets'),
    session = require('web.session'),
    QWeb = core.qweb;


ListView.Column.include({
    heading: function () {
        if (this.modifiers.raw_heading) {
            return this.string;
        }
        return _.escape(this.string);
    },
});

View.include({
    init: function(){
        var self = this;
        this._super.apply(this, arguments);
        if (FIRST_INIT) {
            FIRST_INIT = false;
            new Model('res.users').call('get_login_info', []).then(function(r){
                $('body>nav').after(QWeb.render("project.LoginUserInfo", {data : r}));
            });
            new Model('project.project').call('get_constants', []).then(function(res) {
                // console.log(res)
                PROJECT_PROCESS_TOTAL_ID    = res.process_total_id;
                PROJECT_STATUS_END_ID       = res.status_end_id;
                COMPANIES_DATA              = res.companies_data;
                PROJECT_STATUS_LIST         = res.project_status;
                PROJECT_PAY_STATUS_LIST     = res.pay_status;
                PROJECT_TYPE_LIST           = res.project_type;
                PROJECT_PROCESS_LIST        = res.project_processes;
                COUNT_INTERNAL_PROJECT      = res.count_internal;
                COUNT_I_PAY                 = res.count_i_pay;
                COUNT_C_PAY                 = res.count_c_pay;
                BACKEND_MENU_IDS            = res.backend_menu_ids;
            }).done(function(){
                var y = new Model('res.users').call('get_staff_role', []).then(function(res) {
                    // console.log(CAN_EDIT_PROJECT)
                    IS_ADMIN                    = session.is_admin;
                    CAN_EDIT_PROJECT            = IS_ADMIN || res.can_edit_project;
                    CAN_VIEW_PAY                = IS_ADMIN || res.can_view_pay;
                    PROJECT_CAN_VIEW_I_PAY      = CAN_VIEW_PAY || (!res.force_cant_view_pay && COUNT_I_PAY > 0);
                    PROJECT_CAN_VIEW_C_PAY      = CAN_VIEW_PAY || (!res.force_cant_view_pay && COUNT_C_PAY > 0);
                    CAN_EDIT_PAY_PAID           = IS_ADMIN || res.can_edit_pay_paid;
                    CAN_DEL_PAY_NEXT            = IS_ADMIN || res.can_del_pay_next;
                    CAN_VIEW_INTERNAL_PROJECT   = IS_ADMIN || COUNT_INTERNAL_PROJECT > 0;
                    CAN_VIEW_ALL_INTERNAL_PROJECT= IS_ADMIN || res.can_view_internal_project;
                    CAN_ACCESS_BACKEND          = IS_ADMIN || res.can_access_backend;
                    CAN_QUICK_ADD_PARTNER       = IS_ADMIN || res.can_quick_add_partner;

                    if (!CAN_ACCESS_BACKEND) {
                        $.each(BACKEND_MENU_IDS, function(index, value){
                            $('ul.o_menu_sections > li > a[data-menu="'+ value +'"]').remove();
                        });
                    }
                });
                return $.when(y);
            });
        }
    },
});

ListView.include({
    keyup_ESCAPE: function (e) {
        return false;
    }
});


var ProjectShipmentDetailPopup = FormView.extend({
    init: function(){
        this._super.apply(this, arguments);
        this.options.initial_mode = "edit";
    },
    on_button_cancel: function() {
        var self = this;
        var need_reload = this.$el.is('.oe_form_dirty');
        this.can_be_discarded().then(function() {
            $.when.apply(null, self.render_value_defs).then(function(){
                self.trigger('on_button_cancel');
                if (need_reload) {
                    self.trigger('load_record', self.datarecord);
                }
            });
        });
        return false;
    },
    on_button_save: function() {
        var self = this;
        framework.blockUI();
        return this.save().then(function(result) {
            self.trigger("save", result);
            return self.reload().then(function() {
                framework.unblockUI();
            });
        });
    },
});
core.view_registry.add('project_shipment_popup', ProjectShipmentDetailPopup);

var ProjectJobDetailPopup = FormView.extend({
    init: function(){
        this._super.apply(this, arguments);
        this.options.initial_mode = "edit";
    },
    on_button_cancel: function() {
        var self = this;
        var need_reload = this.$el.is('.oe_form_dirty');
        this.can_be_discarded().then(function() {
            $.when.apply(null, self.render_value_defs).then(function(){
                self.trigger('on_button_cancel');
                if (need_reload) {
                    self.trigger('load_record', self.datarecord);
                }
            });
        });
        return false;
    },
    on_button_save: function() {
        var self = this;
        framework.blockUI();
        return this.save().then(function(result) {
            self.trigger("save", result);
            return self.reload().then(function() {
                framework.unblockUI();
            });
        });
    },
});
core.view_registry.add('project_job_popup', ProjectJobDetailPopup);

var ProjectMaterialDetailPopup = FormView.extend({
    init: function(){
        this._super.apply(this, arguments);
        this.options.initial_mode = "edit";
    },
    on_button_cancel: function() {
        var self = this;
        var need_reload = this.$el.is('.oe_form_dirty');
        this.can_be_discarded().then(function() {
            $.when.apply(null, self.render_value_defs).then(function(){
                self.trigger('on_button_cancel');
                if (need_reload) {
                    self.trigger('load_record', self.datarecord);
                }
            });
        });
        return false;
    },
    on_button_save: function() {
        var self = this;
        framework.blockUI();
        return this.save().then(function(result) {
            self.trigger("save", result);
            return self.reload().then(function() {
                framework.unblockUI();
            });
        });
    },
});
core.view_registry.add('project_material_popup', ProjectMaterialDetailPopup);

var ProjectDrawingDetailPopup = FormView.extend({
    init: function(){
        this._super.apply(this, arguments);
        this.options.initial_mode = "edit";
    },
    on_button_cancel: function() {
        var self = this;
        var need_reload = this.$el.is('.oe_form_dirty');
        this.can_be_discarded().then(function() {
            $.when.apply(null, self.render_value_defs).then(function(){
                self.trigger('on_button_cancel');
                if (need_reload) {
                    self.trigger('load_record', self.datarecord);
                }
            });
        });
        return false;
    },
    on_button_save: function() {
        var self = this;
        framework.blockUI();
        return this.save().then(function(result) {
            self.trigger("save", result);
            return self.reload().then(function() {
                framework.unblockUI();
            });
        });
    },
});
core.view_registry.add('project_drawing_popup', ProjectDrawingDetailPopup);

var ProjectCostsDetailPopup = FormView.extend({
    init: function(){
        this._super.apply(this, arguments);
        this.options.initial_mode = "edit";
    },
    on_button_cancel: function() {
        var self = this;
        var need_reload = this.$el.is('.oe_form_dirty');
        this.can_be_discarded().then(function() {
            $.when.apply(null, self.render_value_defs).then(function(){
                self.trigger('on_button_cancel');
                if (need_reload) {
                    self.trigger('load_record', self.datarecord);
                }
            });
        });
        return false;
    },
    on_button_save: function() {
        var self = this;
        framework.blockUI();
        return this.save().then(function(result) {
            self.trigger("save", result);
            return self.reload().then(function() {
                framework.unblockUI();
            });
        });
    },
});
core.view_registry.add('project_costs_popup', ProjectCostsDetailPopup);

var ProjectTimelineDetailPopup = FormView.extend({
    init: function(){
        this._super.apply(this, arguments);
        this.options.initial_mode = "edit";
    },
    on_button_cancel: function() {
        var self = this;
        var need_reload = this.$el.is('.oe_form_dirty');
        this.can_be_discarded().then(function() {
            $.when.apply(null, self.render_value_defs).then(function(){
                self.trigger('on_button_cancel');
                if (need_reload) {
                    self.trigger('load_record', self.datarecord);
                }
            });
        });
        return false;
    },
    on_button_save: function() {
        var self = this;
        framework.blockUI();
        return this.save().then(function(result) {
            self.trigger("save", result);
            return self.reload().then(function() {
                framework.unblockUI();
            });
        });
    },
});
core.view_registry.add('project_timeline_popup', ProjectTimelineDetailPopup);


var ProjectPayDetailPopup = FormView.extend({
    init: function(){
        this._super.apply(this, arguments);
        this.options.initial_mode = "edit";
    },
    on_button_save: function() {
        var self = this;
        framework.blockUI();
        return this.save().then(function(result) {
            self.trigger("save", result);
            return self.reload().then(function() {
                framework.unblockUI();
            });
        });
    },
    on_button_cancel: function() {
        var self = this;
        var need_reload = this.$el.is('.oe_form_dirty');
        this.can_be_discarded().then(function() {
            $.when.apply(null, self.render_value_defs).then(function(){
                self.trigger('on_button_cancel');
                if (need_reload) {
                    self.trigger('load_record', self.datarecord);
                }
            });
        });
        return false;
    },
    do_show: function (options) {
        var self = this;
        $.when(this._super.apply(this, arguments)).done(function(){
            if (self.session.uid !== 1
                && !CAN_VIEW_PAY
                && self.datarecord.existed_role_uids.indexOf(',' + self.session.uid + ',') === -1) {
                alert("Không thể truy cập bản ghi này.\nCó thể nhân sự nội bộ đã thay đổi sau khi danh sách được tải.\nBáo với quản trị viên nếu bạn cho rằng có lỗi!");
                // return self.do_action("project.action_project_master", {clear_breadcrumbs:true});
                self.destroy();
                $('div.modal.in').remove();
                $('div.modal-backdrop.in').remove();
                $('body').removeClass('modal-open');
            }
            CAN_EDIT_PAY_NEXT = self.session.uid === 1 || CAN_EDIT_PROJECT || (self.datarecord.muids && self.datarecord.muids.indexOf(self.session.uid) !== -1);
            $('.modal.in').on('hide.bs.modal', function(e){
                e.preventDefault();
                e.stopImmediatePropagation();
                self.can_be_discarded().then(function() {
                    self.destroy();
                    $('div.modal.in').remove();
                    $('div.modal-backdrop.in').remove();
                    $('body').removeClass('modal-open');
                    $('title').click();
                });
            });
        });
    }
});
core.view_registry.add('project_pay_popup', ProjectPayDetailPopup);


var ProjectProjectDetailForm = FormView.extend({
    className: "o_form_view v_project_detail_form",
    searchable: false,
    init: function(){
        this._super.apply(this, arguments);
        this.options.initial_mode = "edit";
        this.options.action_buttons = false;
        this.options.pager = false;
        this.confirmed_discard = false;
        this._to_init = true;
    },
    willStart: function() {
        var z = new Model('project.manufact.process').call('get_process_crite_rel', []).then(function(res) {
            PROJECT_PROCESS_CRITE_REL = res;
        });
        return $.when(this._super.apply(this, arguments), z);
    },
    on_button_save: function(reload) {
        var self = this;
        framework.blockUI();
        return this.save().always(function(result) {
            self.trigger("save", result);
            if (reload) {
                 return self.reload().then(function() {
                    self.$el.removeClass('oe_form_dirty oe_form_touched');
                    framework.unblockUI();
                });
            }
            else {
                self.$el.removeClass('oe_form_dirty oe_form_touched');
                framework.unblockUI();
            }
        });
    },
    on_button_cancel: function() {
        var self = this;
        this.can_be_discarded().done(function() {
            framework.blockUI();
            self.$el.removeClass('oe_form_dirty oe_form_touched');
            $.when.apply(null, self.render_value_defs).always(function(){
                self.trigger('load_record', self.datarecord);
                self.trigger('on_button_cancel');
                framework.unblockUI();
            });
        });
        return false;
    },
    show_tab: function(tabstr){
        $('a[role="tab"][data-str="'+ tabstr +'"]').parent().removeClass('o_form_invisible');
    },
    remove_tab: function(tabstr){
        var tab = $('a[role="tab"][data-str="'+ tabstr +'"]');
        var content_tab_id = tab.attr('href');
        $('div'+ content_tab_id).remove();
        tab.parent().remove();
    },
    do_show: function () {
        var self = this;
        if (this._to_init === true) {
            this._to_init = false;
            $.when(this._super.apply(this, arguments)).then(function(){
                if (self.session.uid !== 1
                    && !CAN_EDIT_PROJECT
                    && self.datarecord.existed_role_uids.indexOf(',' + self.session.uid + ',') === -1) {
                    alert("Không thể truy cập bản ghi này.\nCó thể nhân sự nội bộ đã thay đổi sau khi danh sách được tải.\nBáo với quản trị viên nếu bạn cho rằng có lỗi!");
                    return self.do_action("project.action_project_master", {clear_breadcrumbs:true});
                }
                PROJECT_ID = self.datarecord.id;
                PROJECT_CURRENT_ROLE = self.datarecord.role;
                PROJECT_EDITABLE = self.datarecord.status_id[0] != PROJECT_STATUS_END_ID;
                if (self.session.is_superuser) {
                    self.show_tab("Phiếu giao việc");
                    self.show_tab("Quy cách vật liệu");
                    self.show_tab("Bản vẽ");
                    self.show_tab("Timeline sản xuất");
                    self.show_tab("Theo dõi sản xuất");
                    self.show_tab("Xuất hàng");
                    self.show_tab("Chi phí công trường");
                }
                else {
                    // Ẩn/hiện tab "Phiếu giao việc"
                    if ((PROJECT_CURRENT_ROLE | ROLE_VIEW_JOB) === PROJECT_CURRENT_ROLE) self.show_tab("Phiếu giao việc");
                    else self.remove_tab("Phiếu giao việc");
                    // Ẩn/hiện tab "Quy cách vật liệu"
                    if ((PROJECT_CURRENT_ROLE | ROLE_VIEW_MATERIAL) === PROJECT_CURRENT_ROLE) self.show_tab("Quy cách vật liệu");
                    else self.remove_tab("Quy cách vật liệu");
                    // Ẩn/hiện tab "Bản vẽ"
                    if ((PROJECT_CURRENT_ROLE | ROLE_VIEW_DRAWING) === PROJECT_CURRENT_ROLE) self.show_tab("Bản vẽ");
                    else self.remove_tab("Bản vẽ");
                    // Ẩn/hiện tab "Timeline sản xuất"
                    if ((PROJECT_CURRENT_ROLE | ROLE_VIEW_TIMELINE) === PROJECT_CURRENT_ROLE) self.show_tab("Timeline sản xuất");
                    else self.remove_tab("Timeline sản xuất");
                    // Ẩn/hiện tab "Theo dõi sản xuất"
                    if ((PROJECT_CURRENT_ROLE | ROLE_VIEW_MANUFACT) === PROJECT_CURRENT_ROLE) self.show_tab("Theo dõi sản xuất");
                    else self.remove_tab("Theo dõi sản xuất");
                    // Ẩn/hiện tab "Xuất hàng"
                    if ((PROJECT_CURRENT_ROLE | ROLE_VIEW_SHIPMENT) === PROJECT_CURRENT_ROLE) self.show_tab("Xuất hàng");
                    else self.remove_tab("Xuất hàng");
                    // Ẩn/hiện tab "Chi phí công trường"
                    if ((PROJECT_CURRENT_ROLE | ROLE_VIEW_COSTS) === PROJECT_CURRENT_ROLE) self.show_tab("Chi phí công trường");
                    else self.remove_tab("Chi phí công trường");
                }

                self.act_bc = $('ol.breadcrumb li.active');
                self.origin_bc = self.act_bc.text();
                self.act_bc.text(self.origin_bc + ' / Thông tin chung');

                self.$el.find('.o_form_sheet > div > ul.nav-tabs li > a').unbind('click').click(function(e){
                    var t = this;
                    if (!self.confirmed_discard) {
                        var is_dirty = self.$el.is('.oe_form_dirty');
                        e.preventDefault();
                        e.stopPropagation();
                        self.can_be_discarded().then(function() {
                            if (is_dirty) {
                                self.reload().then(function() {
                                    self.confirmed_discard = true;
                                    $(t).trigger('click');
                                });
                            }
                            else {
                                self.confirmed_discard = true;
                                $(t).trigger('click');
                            }
                        });
                    }
                    else {
                        self.confirmed_discard = false;
                        self.act_bc.text(self.origin_bc + ' / ' + $(t).text());
                        return;
                    }
                });
                // Nếu chưa có nút back về dự án thì chèn thủ công
                if ($('ol.breadcrumb li.o_back_button').length === 0) {
                    self.act_bc.before('<li class="o_back_button"><a class="v_backto_project">Dự án</a></li>');
                    $('.o_back_button').click(function(){
                        self.do_action("project.action_project_master", {clear_breadcrumbs:true});
                    });
                }

                if (self.session.is_superuser && PROJECT_EDITABLE) {
                    self.$el.find('.v_project_save_btns a.v_save_btn').click(function(){
                        self.on_button_save();
                    });
                    self.$el.find('.v_project_save_btns a.v_cancel_btn').click(function(){
                        self.on_button_cancel();
                    });
                }
                else {
                    self.$el.find('.v_project_save_btns').remove();
                }
            });
        }
    },
});
core.view_registry.add('project_project_detail', ProjectProjectDetailForm);


var ProjectProjectPopupForm = FormView.extend({
    init: function(){
        this._super.apply(this, arguments);
        this.options.initial_mode = "edit";
    },
    on_button_cancel: function() {
        var self = this;
        this.can_be_discarded().then(function() {
            self.destroy();
            $('button[data-dismiss="modal"]').click();
            self.trigger('on_button_cancel');
            $('title').click();
        });
        return false;
    },
    on_button_save: function() {
        var self = this;
        if (this.is_disabled) {
            return;
        }
        return this.save().then(function(result) {
            self.trigger("save", result);
            self.destroy();
            $('button[data-dismiss="modal"]').click();
            $('head').click();
        });
    },
    do_show: function (options) {
        var self = this;
        $.when(this._super.apply(this, arguments)).done(function(){
            $('.modal.in').on('hide.bs.modal', function(e){
                e.preventDefault();
                e.stopImmediatePropagation();
                self.can_be_discarded().then(function() {
                    self.destroy();
                    $('div.modal.in').remove();
                    $('div.modal-backdrop.in').remove();
                    $('body').removeClass('modal-open');
                    $('title').click();
                });
            });
        });
    }
});
core.view_registry.add('project_project_popup', ProjectProjectPopupForm);


var ProjectMasterForm = FormView.extend({
    init: function(){
        var self = this;
        this._super.apply(this, arguments);
        this.options.initial_mode = "edit";
        this.options.action_buttons = false;
        this.options.pager = false;
        $('head').unbind("click").click(function(e){
            self.on_button_save();
        });
    },
    do_show: function(){
        var self = this;
        return $.when(this._super.apply(this, arguments)).then(function(){
            if (!CAN_ACCESS_BACKEND && BACKEND_MENU_IDS !== undefined) {
                $.each(BACKEND_MENU_IDS, function(index, value){
                    $('ul.o_menu_sections > li > a[data-menu="'+ value +'"]').remove();
                });
            }
            $('head').unbind('click').click(function(){
                self.on_button_save();
            });
        });
    },
    on_button_save: function() {
        var self = this;
        return this.save().then(function(result) {
            self.trigger("save", result);
            return self.reload().then(function() {
                core.bus.trigger('form_view_saved', self);
            }).always(function() {
                self.enable_button();
            });
        }).fail(function(){
            self.enable_button();
        });
    },
});
core.view_registry.add('project_master', ProjectMasterForm);


ActionManager.include({
    do_load_state: function(state, warm) {
        var self = this;
        var action_loaded;
        if (state.action) {
            if (_.isString(state.action) && core.action_registry.contains(state.action)) {
                var action_client = {
                    type: "ir.actions.client",
                    tag: state.action,
                    params: state,
                    _push_me: state._push_me,
                };
                if (warm) {
                    this.null_action();
                }
                action_loaded = this.do_action(action_client);
            } else {
                var run_action = (!this.inner_widget || !this.inner_widget.action) || this.inner_widget.action.id !== state.action;
                if (run_action) {
                    var add_context = {};
                    if (state.active_id) {
                        add_context.active_id = state.active_id;
                    }
                    if (state.active_ids) {
                        // The jQuery BBQ plugin does some parsing on values that are valid integers.
                        // It means that if there's only one item, it will do parseInt() on it,
                        // otherwise it will keep the comma seperated list as string.
                        add_context.active_ids = state.active_ids.toString().split(',').map(function(id) {
                            return parseInt(id, 10) || id;
                        });
                    } else if (state.active_id) {
                        add_context.active_ids = [state.active_id];
                    }
                    add_context.params = state;
                    if (warm) {
                        this.null_action();
                    }
                    action_loaded = this.do_action(state.action, {
                        additional_context: add_context,
                        res_id: state.id,
                        view_type: state.view_type,
                    });
                }
            }
        } else if (state.model && state.id) {
            // TODO handle context & domain ?
            if (warm) {
                this.null_action();
            }
            var action = {
                res_model: state.model,
                res_id: state.id,
                type: 'ir.actions.act_window',
                views: [[_.isNumber(state.view_id) ? state.view_id : false, state.view_type !== undefined ? state.view_type : "form"]]
            };
            action_loaded = this.do_action(action);
        } else if (state.sa) {
            // load session action
            if (warm) {
                this.null_action();
            }
            action_loaded = this.rpc('/web/session/get_session_action',  {key: state.sa}).then(function(action) {
                if (action) {
                    return self.do_action(action);
                }
            });
        }

        return $.when(action_loaded || null).done(function() {
            if (self.inner_widget && self.inner_widget.do_load_state) {
                return self.inner_widget.do_load_state(state, warm);
            }
        });
    }
});
});
