// Quyền truy cập thông tin dự án hiện tại
var PROJECT_CURRENT_ROLE = 0;

var IS_ADMIN = false,							// Tác nhân có phải là admin không?
	CAN_VIEW_INTERNAL_PROJECT = false,			// Tác nhân có thể xem dự án nội bộ có liên quan không?
	CAN_VIEW_ALL_INTERNAL_PROJECT = false,		// Tác nhân có thể xem tất cả dự án nội bộ không?
	COUNT_INTERNAL_PROJECT = 0,					// Số dự án nội bộ mà tác nhân thuộc nhân sự nội bộ => xem dự án nội bộ
	CAN_EDIT_PROJECT = false,					// Tác nhân có thể thêm/sửa bản ghi Dự án/công việc không
	FORCE_CANT_VIEW_PAY = false,				// Tác nhân KHÔNG thể xem tab Thanh toán
	CAN_VIEW_PAY = false,						// Tác nhân có thể xem tab Thanh toán không
	PROJECT_CAN_VIEW_PAY = false,				// Tác nhân có thể xem tab Thanh toán không
	COUNT_I_PAY = 0,							// Số dự án nội bộ mà tác nhân thuộc nhân sự nội bộ => xem thanh toán nội bộ
	COUNT_C_PAY = 0,							// Số dự án khách hàng mà tác nhân thuộc nhân sự nội bộ => xem thanh toán khách hàng
	CAN_EDIT_PAY_PAID = false,					// Tác nhân có thể sửa bảng Chi tiết thanh toán không
	CAN_EDIT_PAY_NEXT = false,					// Tác nhân có thể thêm/sửa bản ghi Dự kiến thanh toán không
	CAN_DEL_PAY_NEXT = false,					// Tác nhân có thể xoá bản ghi Dự kiến thanh toán không
	CAN_QUICK_ADD_PARTNER = undefined,			// Tác nhân có thể thêm nhanh Thầu chính, Chủ đtư, TVGS ở popup dự án ko?
	PROJECT_CAN_VIEW_I_PAY = false,				// 
	PROJECT_CAN_VIEW_C_PAY = false;				// 

var ROLE_VIEW_JOB = 1,							// Xem bảng Phiếu giao việc
	ROLE_EDIT_JOB = 2,							// Sửa bản ghi Phiếu giao việc
	ROLE_DEL_JOB = 4,							// Xoá bản ghi Phiếu giao việc

	ROLE_VIEW_MATERIAL = 8,						// Xem bảng Quy cách vật liệu
	ROLE_EDIT_MATERIAL = 16,					// Sửa bản ghi Quy cách vật liệu
	ROLE_DEL_MATERIAL = 32,						// Xoá bản ghi Quy cách vật liệu

	ROLE_VIEW_DRAWING = 64,						// Xem bảng Bản vẽ
	ROLE_EDIT_DRAWING = 128,					// Sửa bản ghi Bản vẽ
	ROLE_DEL_DRAWING = 256,						// Xoá bản ghi Bản vẽ

	ROLE_VIEW_MANUFACT = 512,					// Xem bảng Theo dõi sản xuất
	ROLE_EDIT_MANUFACT = 1024,					// Sửa bản ghi Theo dõi sản xuất
	ROLE_DEL_MANUFACT = 2048,					// Xoá bản ghi Theoi dõi sản xuất

	ROLE_VIEW_SHIPMENT = 4096,					// Xem bảng Xuất hàng
	ROLE_EDIT_SHIPMENT = 8192,					// Sửa bản ghi Xuất hàng
	ROLE_DEL_SHIPMENT = 16384,					// Xoá bản ghi Xuất hàng

	ROLE_VIEW_COSTS = 32768,					// Xem bảng Chi phí công trường
	ROLE_EDIT_COSTS = 65536,					// Sửa bản ghi Chi phí công trường
	ROLE_DEL_COSTS = 131072,					// Xoá bản ghi Chi phí công trường

	ROLE_VIEW_TIMELINE = 262144,				// Xem bảng Timeline sản xuất
	ROLE_EDIT_TIMELINE = 524288,				// Sửa bản ghi Timeline sản xuất
	ROLE_DEL_TIMELINE = 1048576;				// Xoá bản ghi Timeline sản xuất

var FIRST_INIT = true;							// Khởi tạo page
var PROJECT_ID = undefined,						// Mã dự án đang mở
	SHIPMENT_ID = undefined,					// Mã phiếu xuất hàng đang mở
	PROJECT_EDITABLE = false,					// Dự án đang mở có được sửa không (chưa kết thúc)
	PROJECT_PROCESS_TOTAL_ID = undefined,		// Mã bản ghi Tổng mục
	PROJECT_STATUS_END_ID = undefined,			// Mã bản ghi trạng thái dự án 'Kết thúc'
	PROJECT_PROCESS_CRITE_REL = undefined,		// Mảng ràng buộc giữa Tiến trình & Tiêu chí
	PROJECT_PROCESS_NUMBER_CURRENT = undefined,	// 'Số lượng' hiện tại lúc nhập bản ghi Theo dõi sản xuất => tính cột Phần trăm
	PROJECT_OPEN_DETAIL = true,					// Dự án/Công việc, tab Nội bộ bấm vào không mở chi tiết
	PROJECT_PAY_CUSTOMER = true,				// Thanh toán, tab Nội bộ có các cột khác với tab Khách hàng
	PROJECT_PROCESS_CRITE_CURRENT = undefined;	// Mã Tiến trình hiện tại => Ẩn các cột Tiêu chí không liên quan

var COMPANIES_DATA = undefined,					// Danh sách công ty
	PROJECT_STATUS_LIST = undefined,			// DS trạng thái dự án/công việc
	PROJECT_PAY_STATUS_LIST = undefined,		// DS trạng thái thanh toán
	PROJECT_TYPE_LIST = undefined,				// DS loại hình CV
	PROJECT_PROCESS_LIST = undefined,			// DS trạng thái dự án/công việc
	CAN_ACCESS_BACKEND = false,					// DS trạng thái dự án/công việc
	BACKEND_MENU_IDS = undefined;				// DS trạng thái dự án/công việc
