odoo.define('project.widgets', function (require) {
"use strict";
var core = require('web.core'),
	common = require('web.form_common'),
	form_common = require('web.form_common'),
	list_common = require('web.list_common'),
	form_relational = require('web.form_relational'),
	ListView = require('web.ListView'),
	data = require('web.data'),
	Model = require('web.DataModel'),

	FieldOne2Many = core.form_widget_registry.get('one2many'),
	FieldMany2Many = core.form_widget_registry.get('many2many'),
	FieldMany2One = core.form_widget_registry.get('many2one'),
	FieldSelection = core.form_widget_registry.get('selection'),
	FieldBinaryFile = core.form_widget_registry.get('binary'),
	FieldFloat = core.form_widget_registry.get('float'),
	FieldChar = core.form_widget_registry.get('char'),
	One2ManyListView = core.one2many_view_registry.get('list'),
	ColumnBoolean = core.list_widget_registry.get('field.boolean'),
	ColumnBinary = core.list_widget_registry.get('field.binary'),
	ColumnChar = core.list_widget_registry.get('field.char'),
	framework = require('web.framework'),
	session = require('web.session'),
	utils = require('web.utils'),
	Pager = require('web.Pager'),
	formats = require('web.formats'),

	_t = core._t,
	IS_ADMIN = session.is_admin,
	QWeb = core.qweb;


ListView.Groups.include({
	render_dataset: function (dataset) {
        var self = this,
            list = new (this.view.options.ListType)(this, {
                options: this.options,
                columns: this.columns,
                dataset: dataset,
                records: this.records
            });
        this.bind_child_events(list);

        var view = this.view;
        var current_min = this.datagroup.openable ? this.current_min : view.current_min;

        var fields = _.pluck(_.select(this.columns, function(x) {return x.tag == "field";}), 'name');
        var options = { offset: current_min - 1, limit: view._limit, context: {bin_size: true} };
        return utils.async_when().then(function() {
            return dataset.read_slice(fields, options).then(function (records) {
            	if (self.view.dataset.model.startsWith('project') && self.view.dataset.model !== 'project.project'
            		&& ['project.project.job',
	            		'project.project.material',
	            		'project.project.drawing',
	            		'project.project.manufact',
	            		'project.project.shipment',
	            		'project.project.costs'].indexOf(self.view.dataset.model) !== -1) {
                    var ids = [], cache = {};
                    $.each(records, function(index, record){
                        if (!self.view.dataset.cache[record.id] || !self.view.dataset.cache[record.id].to_delete) {
                            ids.push(record.id);
	                        cache[record.id] = {
	                        	changes: {},
	                        	from_read: record,
	                        	id: record.id,
	                        	readonly_fields: {},
	                        	to_create: false,
	                        	to_delete: false,
	                        	values: record,
	                        }
                    	}
                    });
                    self.view.dataset.cache = cache;
                    self.view.dataset.parent_view.datarecord[self.view.dataset.child_name] = ids;
                    self.view.dataset.ids = ids;
                    self.view.x2m.starting_ids = ids;
                    self.view.x2m._dirty_flag = false;
            	}
                // FIXME: ignominious hacks, parents (aka form view) should not send two ListView#reload_content concurrently
                if (self.records.length) {
                    self.records.reset(null, {silent: true});
                }
                if (!self.datagroup.openable) {
                    // Update the main list view pager
                    view.update_pager(dataset, current_min);
                }

                self.records.add(records, {silent: true});
                list.render();
                return list;
            });
        });
    },
});

// Ẩn nút "Thêm một hạng mục" ở cuối bảng One2Many
var X2ManyListProject = ListView.List.extend({
	pad_table_to: function (count) {
		this._super(0);

		var self = this;
		var columns = _(this.columns).filter(function (column) {
			return column.invisible !== '1';
		}).length;
		if (this.options.selectable) { columns++; }
		if (this.options.deletable) { columns++; }

		var $cell = $('<td>', {
			colspan: columns,
			'class': 'o_form_field_x2many_list_row_add hidden'
		}).append(
			$('<a>', {href: '#'}).text(_t("Add an item"))
				.click(function (e) {
					e.preventDefault();
					e.stopPropagation();
					var def;
					if (self.view.editable()) {
						// FIXME: there should also be an API for that one
						if (self.view.editor.form.__blur_timeout) {
							clearTimeout(self.view.editor.form.__blur_timeout);
							self.view.editor.form.__blur_timeout = false;
						}
						def = self.view.save_edition();
					}
					$.when(def).done(self.view.do_add_record.bind(self));
				}));

		var $padding = this.$current.find('tr:not([data-id]):first');
		var $newrow = $('<tr>').append($cell);
		if ($padding.length) {
			$padding.before($newrow);
		} else {
			this.$current.append($newrow);
		}
	},
});

var One2ManyListViewProjectDetails = One2ManyListView.extend({
	init: function(){
		this._super.apply(this, arguments);
		this._limit = 0;
		this.filter_domain = [];
		this.options.action = this.options.action || {};
		this.options.action.help = "Không có bản ghi nào để hiển thị.";
	},
	do_activate_record: function(index, id) { return; },
	load_list: function() {
		var self = this;
		return $.when(this._super.apply(this, arguments)).done(function(){
			self.$el.parent().find('table tbody td[data-field="order"]').each(function(i, e){
				$(e).html(i + 1);
			});
		});
	},
    display_nocontent_helper: function () { return (this.dataset.size() === 0); },
	do_delete: function (ids) {
        if (!(ids.length)) {
            return;
        }
        var self = this;
        return $.when(this.dataset.unlink(ids)).done(function () {
            _(ids).each(function (id) {
                self.records.remove(self.records.get(id));
            });
            // Hide the table if there is no more record in the dataset
            if (self.display_nocontent_helper()) {
                self.no_result();
            }
            self.update_pager(self.dataset);
            self.compute_aggregates();
        });
    },
	do_search: function(domain, context, group_by) {
		this.last_domain = domain;
		this.last_context = context;
		this.last_group_by = group_by;
		this.old_search = _.bind(this._super, this);
		return this.search();
	},
	search: function() {
		var self = this;
		var domain = this.filter_domain;
		var compound_domain = new data.CompoundDomain(self.last_domain, domain);
		self.dataset.domain = compound_domain.eval();
		return self.old_search(compound_domain, self.last_context, self.last_group_by);
	},
	my_filter: function(){ return this.dataset.parent_view.can_be_discarded(); },
	render_pager: function($node, options) {},
});


// Sự kiện bấm vào dòng "Phiếu giao việc" có được sửa không
var X2ManyListProjectJob = X2ManyListProject.extend({
	row_clicked: function (event) {
		if ((PROJECT_CURRENT_ROLE | ROLE_EDIT_JOB) === PROJECT_CURRENT_ROLE)
			return this._super.apply(this, arguments);
		return;
	},
	pad_table_to: function (count) {
		if ((PROJECT_CURRENT_ROLE | ROLE_EDIT_JOB) === PROJECT_CURRENT_ROLE)
			return this._super.apply(this, arguments);
		return;
	}
});
// Danh sách "Phiếu giao việc" - Dự án
var One2ManyListViewProjectJob = One2ManyListViewProjectDetails.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.options.ListType = X2ManyListProjectJob;
		this.options.deletable = PROJECT_EDITABLE && (PROJECT_CURRENT_ROLE | ROLE_DEL_JOB) === PROJECT_CURRENT_ROLE;
	},
	start: function(){
		var self = this;
		this._super.apply(this, arguments);
		this.$el.before(QWeb.render("ProjectJobListHead"));

		var $dom = this.$el.parent();

		// Đăng ký sự kiện lọc bản ghi khi ô "Lọc theo" thay đổi giá trị
		$dom.find('.v_filter_type').on('change', function(){
			self.my_filter();
		});
		// Đăng ký sự kiện lọc bản ghi khi ô nhập giá trị cần tìm nhấn nút Enter
		$dom.find('.v_filter_value').on("keypress", function(e){
			if(e.which === 13) {
				self.my_filter();
			}
		});
		$dom.find('.v_export_record_btn').click(function() {
			var domainx = self.my_filter(true);
			$.when(domainx).then(function(d){
				framework.blockUI();
				self.session.get_file({
					url: '/web/export/project_project_job_export',
					data: {
						data: JSON.stringify({ domain : d })
					},
					complete: framework.unblockUI,
				});
			});
		});
		if (PROJECT_EDITABLE && (PROJECT_CURRENT_ROLE | ROLE_EDIT_JOB) === PROJECT_CURRENT_ROLE) {
			$dom.find('.v_edit_btns').removeClass('hidden');
			$dom.find('a.v_add_record_btn').click(function(e){
				e.preventDefault();
				e.stopPropagation();
				self.do_action({
					name: "Thêm phiếu giao việc",
					view_mode:'project_job_popup',
					views : [[false,'project_job_popup']],
					res_model:'project.project.job',
					type:'ir.actions.act_window',
					target:'new',
					context:{'project_id': PROJECT_ID}
				}, {on_close: function(){
					self.my_filter();
				}});
			});
			$dom.find('.v_save_btn').click(function() {
				$.when(self.dataset.parent_view.on_button_save(false)).then(function(){
					self.my_filter();
				});
			});
			$dom.find('.v_cancel_btn').click(function() {
				$.when(self.my_filter()).then(function(){
					self.my_filter();
				});
			});
		}
	},
	do_activate_record: function(index, id) {
		var self = this;
        var record = this.records.get(id);
		if (record && (PROJECT_CURRENT_ROLE | ROLE_EDIT_JOB) === PROJECT_CURRENT_ROLE) {
			this.do_action({
				name: "Phiếu giao việc",
				view_mode:'project_job_popup',
				views : [[false,'project_job_popup']],
				res_model:'project.project.job',
				type:'ir.actions.act_window',
				res_id:record.attributes.id,
				target:'new',
				context:{'project_id': PROJECT_ID}
			}, {on_close: function(){
				self.my_filter();
			}});
		}
    },
	my_filter: function(get_domain){
		var self = this;
		return this._super.apply(this, arguments).then(function(){
			self.filter_domain = [];
			self.filter_domain.push(['project_id', '=', PROJECT_ID]);
			// Người gửi
			var value = self.$el.parent().find('.v_filter_creator').val();
			if (value != '') {
				self.filter_domain.push(["create_u_name", "ilike", value]);
			}
			// Loại phiếu
			var value = self.$el.parent().find('.v_filter_typex').val();
			if (value != '') {
				self.filter_domain.push(["typex", "ilike", value]);
			}
			self.do_search([], [], []);
			if (get_domain===true) {
				return self.filter_domain;
			}
		});
	},
});
var FieldOne2ManyProjectJob = FieldOne2Many.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.x2many_views.list = One2ManyListViewProjectJob;
	},
	get_value: function() {
		var command_list = this._super.apply(this, arguments);
		var ret = [];
		_.each(command_list, function (command) {
            if (command[0] !== 4) {
                ret.push(command);
            }
        });
		return ret;
	},
});
core.form_widget_registry.add('tree_project_job', FieldOne2ManyProjectJob);


// Sự kiện bấm vào dòng "Quy cách vật liệu" có được sửa không
var X2ManyListProjectMaterial = X2ManyListProject.extend({
	row_clicked: function (event) {
		if ((PROJECT_CURRENT_ROLE | ROLE_EDIT_MATERIAL) === PROJECT_CURRENT_ROLE)
			return this._super.apply(this, arguments);
		return;
	},
	pad_table_to: function (count) {
		if ((PROJECT_CURRENT_ROLE | ROLE_EDIT_MATERIAL) === PROJECT_CURRENT_ROLE)
			return this._super.apply(this, arguments);
		return;
	}
});
// Quy cách vật liệu
var One2ManyListViewProjectMaterial = One2ManyListViewProjectDetails.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.options.ListType = X2ManyListProjectMaterial;
		this.options.deletable = PROJECT_EDITABLE && (PROJECT_CURRENT_ROLE | ROLE_DEL_MATERIAL) === PROJECT_CURRENT_ROLE;
	},
	start: function(){
		var self = this;
		this._super.apply(this, arguments);
		this.$el.before(QWeb.render("ProjectMaterial"));

		var $dom = this.$el.parent();

		// Đăng ký sự kiện lọc bản ghi khi ô "Lọc theo" thay đổi giá trị
		$dom.find('.v_filter_type').on('change', function(){
			self.my_filter();
		});
		// Đăng ký sự kiện lọc bản ghi khi ô nhập giá trị cần tìm nhấn nút Enter
		$dom.find('.v_filter_value').on("keypress", function(e){
			if(e.which === 13) {
				self.my_filter();
			}
		});
		$dom.find('.v_export_record_btn').click(function() {
			var domainx = self.my_filter(true);
			$.when(domainx).then(function(d){
				framework.blockUI();
				self.session.get_file({
					url: '/web/export/project_project_material_export',
					data: {
						data: JSON.stringify({ domain : d })
					},
					complete: framework.unblockUI,
				});
			});
		});

		if (PROJECT_EDITABLE && (PROJECT_CURRENT_ROLE | ROLE_EDIT_MATERIAL) === PROJECT_CURRENT_ROLE) {
			$dom.find('.v_edit_btns').removeClass('hidden');
			$dom.find('a.v_add_record_btn').click(function(e){
				e.preventDefault();
				e.stopPropagation();
				self.do_action({
					name: "Thêm Quy cách vật liệu",
					view_mode:'project_material_popup',
					views : [[false,'project_material_popup']],
					res_model:'project.project.material',
					type:'ir.actions.act_window',
					target:'new',
					context:{'project_id': PROJECT_ID}
				}, {on_close: function(){
					self.my_filter();
				}});
			});
			$dom.find('.v_save_btn').click(function() {
				$.when(self.dataset.parent_view.on_button_save(false)).then(function(){
					self.my_filter();
				});
			});
			$dom.find('.v_cancel_btn').click(function() {
				$.when(self.my_filter()).then(function(){
					self.my_filter();
				});
			});
		}
	},
	do_activate_record: function(index, id) {
		var self = this;
        var record = this.records.get(id);
		if (record && (PROJECT_CURRENT_ROLE | ROLE_EDIT_MATERIAL) === PROJECT_CURRENT_ROLE) {
			this.do_action({
				name: "Quy cách vật liệu",
				view_mode:'project_material_popup',
				views : [[false,'project_material_popup']],
				res_model:'project.project.material',
				type:'ir.actions.act_window',
				res_id:record.attributes.id,
				target:'new',
				context:{'project_id': PROJECT_ID}
			}, {on_close: function(){
				self.my_filter();
			}});
		}
    },
	my_filter: function(get_domain){
		var self = this;
		return this._super.apply(this, arguments).then(function(){
			self.filter_domain = [];
			self.filter_domain.push(['project_id', '=', PROJECT_ID]);
			// Hạng mục
			var value = self.$el.parent().find('.v_filter_category').val();
			if (value != '') {
				self.filter_domain.push(['category', "ilike", value]);
			}
			// Người cập nhật
			var value = self.$el.parent().find('.v_filter_write_uid').val();
			if (value != '') {
				self.filter_domain.push(['write_uid', "ilike", value]);
			}
			self.do_search([], [], []);
			if (get_domain===true) {
				return self.filter_domain;
			}
		});
	},
});
var FieldOne2ManyProjectMaterial = FieldOne2Many.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.x2many_views.list = One2ManyListViewProjectMaterial;
	},
	get_value: function() {
		var command_list = this._super.apply(this, arguments);
		var ret = [];
		_.each(command_list, function (command) {
            if (command[0] !== 4) {
                ret.push(command);
            }
        });
		return ret;
	},
});
core.form_widget_registry.add('tree_project_material', FieldOne2ManyProjectMaterial);


// Sự kiện bấm vào dòng "Bản vẽ" có được sửa không
var X2ManyListProjectDrawing = X2ManyListProject.extend({
	row_clicked: function (event) {
		if ((PROJECT_CURRENT_ROLE | ROLE_EDIT_DRAWING) === PROJECT_CURRENT_ROLE)
			return this._super.apply(this, arguments);
		return;
	},
	pad_table_to: function (count) {
		if ((PROJECT_CURRENT_ROLE | ROLE_EDIT_DRAWING) === PROJECT_CURRENT_ROLE)
			return this._super.apply(this, arguments);
		return;
	}
});
// Bản vẽ
var One2ManyListViewProjectDrawing = One2ManyListViewProjectDetails.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.options.ListType = X2ManyListProjectDrawing;
		this.options.deletable = PROJECT_EDITABLE && (PROJECT_CURRENT_ROLE | ROLE_DEL_DRAWING) === PROJECT_CURRENT_ROLE;
	},
	start: function(){
		var self = this;
		this._super.apply(this, arguments);
		this.$el.before(QWeb.render("ProjectDrawing"));

		var $dom = this.$el.parent();

		// Đăng ký sự kiện lọc bản ghi khi ô "Lọc theo" thay đổi giá trị
		$dom.find('.v_filter_type').on('change', function(){
			self.my_filter();
		});
		// Đăng ký sự kiện lọc bản ghi khi ô nhập giá trị cần tìm nhấn nút Enter
		$dom.find('.v_filter_value').on("keypress", function(e){
			if(e.which === 13) {
				self.my_filter();
			}
		});
		$dom.find('.v_export_record_btn').click(function() {
			var domainx = self.my_filter(true);
			$.when(domainx).then(function(d){
				framework.blockUI();
				self.session.get_file({
					url: '/web/export/project_project_drawing_export',
					data: {
						data: JSON.stringify({ domain : d })
					},
					complete: framework.unblockUI,
				});
			});
		});
		if (PROJECT_EDITABLE && (PROJECT_CURRENT_ROLE | ROLE_EDIT_DRAWING) === PROJECT_CURRENT_ROLE) {
			$dom.find('.v_edit_btns').removeClass('hidden');
			$dom.find('a.v_add_record_btn').click(function(e){
				e.preventDefault();
				e.stopPropagation();
				self.do_action({
					name: "Thêm Bản vẽ",
					view_mode:'project_drawing_popup',
					views : [[false,'project_drawing_popup']],
					res_model:'project.project.drawing',
					type:'ir.actions.act_window',
					target:'new',
					context:{'project_id': PROJECT_ID}
				}, {on_close: function(){
					self.my_filter();
				}});
			});
			$dom.find('.v_save_btn').click(function() {
				$.when(self.dataset.parent_view.on_button_save(false)).then(function(){
					self.my_filter();
				});
			});
			$dom.find('.v_cancel_btn').click(function() {
				$.when(self.my_filter()).then(function(){
					self.my_filter();
				});
			});
		}
	},
	do_activate_record: function(index, id) {
		var self = this;
        var record = this.records.get(id);
		if (record && (PROJECT_CURRENT_ROLE | ROLE_EDIT_DRAWING) === PROJECT_CURRENT_ROLE) {
			this.do_action({
				name: "Bản vẽ",
				view_mode:'project_drawing_popup',
				views : [[false,'project_drawing_popup']],
				res_model:'project.project.drawing',
				type:'ir.actions.act_window',
				res_id:record.attributes.id,
				target:'new',
				context:{'project_id': PROJECT_ID}
			}, {on_close: function(){
				self.my_filter();
			}});
		}
    },
	my_filter: function(get_domain){
		var self = this;
		return this._super.apply(this, arguments).then(function(){
			self.filter_domain = [];
			self.filter_domain.push(['project_id', '=', PROJECT_ID]);
			
			// Hạng mục
			var value = self.$el.parent().find('.v_filter_category').val();
			if (value != '') {
				self.filter_domain.push(['category', "ilike", value]);
			}
			// Người cập nhật
			var value = self.$el.parent().find('.v_filter_write_uid').val();
			if (value != '') {
				self.filter_domain.push(['write_uid', "ilike", value]);
			}
			// Tên
			var value = self.$el.parent().find('.v_filter_name').val();
			if (value != '') {
				self.filter_domain.push(['name', "ilike", value]);
			}
			// Trạng thái
			var value = self.$el.parent().find('.v_filter_type').val();
			if (value == '1' || value == '0') {
				self.filter_domain.push(['state', "=", value]);
			}
			self.do_search([], [], []);
			if (get_domain===true) {
				return self.filter_domain;
			}
		});
	},
});
var FieldOne2ManyProjectDrawing = FieldOne2Many.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.x2many_views.list = One2ManyListViewProjectDrawing;
	},
	get_value: function() {
		var command_list = this._super.apply(this, arguments);
		var ret = [];
		_.each(command_list, function (command) {
            if (command[0] !== 4) {
                ret.push(command);
            }
        });
		return ret;
	},
});
core.form_widget_registry.add('tree_project_drawing', FieldOne2ManyProjectDrawing);


// Sự kiện bấm vào dòng "Timeline sản xuất" có được sửa không
var X2ManyListProjectTimeline = X2ManyListProject.extend({
	row_clicked: function (event) {
		if ((PROJECT_CURRENT_ROLE | ROLE_EDIT_TIMELINE) === PROJECT_CURRENT_ROLE)
			return this._super.apply(this, arguments);
		return;
	},
	pad_table_to: function (count) {
		if ((PROJECT_CURRENT_ROLE | ROLE_EDIT_TIMELINE) === PROJECT_CURRENT_ROLE)
			return this._super.apply(this, arguments);
		return;
	}
});
// Timeline sản xuất
var One2ManyListViewProjectTimeline = One2ManyListViewProjectDetails.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.options.ListType = X2ManyListProjectTimeline;
		this.options.deletable = PROJECT_EDITABLE && (PROJECT_CURRENT_ROLE | ROLE_DEL_TIMELINE) === PROJECT_CURRENT_ROLE;
	},
	start: function(){
		var self = this;
		this._super.apply(this, arguments);
		this.$el.before(QWeb.render("ProjectTimeline", {processes: PROJECT_PROCESS_LIST}));

		var $dom = this.$el.parent();

		// Đăng ký sự kiện lọc bản ghi khi ô "Lọc theo" thay đổi giá trị
		$dom.find('.v_filter_process').on('change', function(){
			self.my_filter();
		});
		// Đăng ký sự kiện lọc bản ghi khi ô nhập giá trị cần tìm nhấn nút Enter
		$dom.find('.v_filter_value').on("keypress", function(e){
			if(e.which === 13) {
				self.my_filter();
			}
		});
		// $dom.find('.v_export_record_btn').click(function() {
		// 	var domainx = self.my_filter(true);
		// 	$.when(domainx).then(function(d){
		// 		framework.blockUI();
		// 		self.session.get_file({
		// 			url: '/web/export/project_project_timeline_export',
		// 			data: {
		// 				data: JSON.stringify({ domain : d })
		// 			},
		// 			complete: framework.unblockUI,
		// 		});
		// 	});
		// });
		if (PROJECT_EDITABLE && (PROJECT_CURRENT_ROLE | ROLE_EDIT_TIMELINE) === PROJECT_CURRENT_ROLE) {
			$dom.find('.v_edit_btns').removeClass('hidden');
			$dom.find('a.v_add_record_btn').click(function(e){
				e.preventDefault();
				e.stopPropagation();
				self.do_action({
					name: "Thêm Timeline sản xuất",
					view_mode:'project_timeline_popup',
					views : [[false,'project_timeline_popup']],
					res_model:'project.project.timeline',
					type:'ir.actions.act_window',
					target:'new',
					context:{'project_id': PROJECT_ID}
				}, {on_close: function(){
					self.my_filter();
				}});
			});
			$dom.find('.v_save_btn').click(function() {
				$.when(self.dataset.parent_view.on_button_save(false)).then(function(){
					self.my_filter();
				});
			});
			$dom.find('.v_cancel_btn').click(function() {
				$.when(self.my_filter()).then(function(){
					self.my_filter();
				});
			});
		}
	},
	do_activate_record: function(index, id) {
		var self = this;
        var record = this.records.get(id);
		if (record && (PROJECT_CURRENT_ROLE | ROLE_EDIT_TIMELINE) === PROJECT_CURRENT_ROLE) {
			this.do_action({
				name: "Timeline sản xuất",
				view_mode:'project_timeline_popup',
				views : [[false,'project_timeline_popup']],
				res_model:'project.project.timeline',
				type:'ir.actions.act_window',
				res_id:record.attributes.id,
				target:'new',
				context:{'project_id': PROJECT_ID}
			}, {on_close: function(){
				self.my_filter();
			}});
		}
    },
	my_filter: function(get_domain){
		var self = this;
		return this._super.apply(this, arguments).then(function(){
			self.filter_domain = [];
			self.filter_domain.push(['project_id', '=', PROJECT_ID]);
			// Người gửi
			var value = self.$el.parent().find('.v_filter_create_uid').val();
			if (value != '') {
				self.filter_domain.push(['create_uid', 'ilike', value]);
			}
			// Tiến trình
			var processes = parseInt(self.$el.parent().find('.v_filter_process').val());
			if (!isNaN(processes) && processes > 0) {
				self.filter_domain.push(['process', '=', processes]);
			}
			// Nhà máy
			var value = self.$el.parent().find('.v_filter_factory').val();
			if (value != '') {
				self.filter_domain.push(['factory', 'ilike', value]);
			}
			self.do_search([], [], []);
			if (get_domain===true) {
				return self.filter_domain;
			}
		});
	},
});
var FieldOne2ManyProjectTimeline = FieldOne2Many.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.x2many_views.list = One2ManyListViewProjectTimeline;
	},
	get_value: function() {
		var command_list = this._super.apply(this, arguments);
		var ret = [];
		_.each(command_list, function (command) {
            if (command[0] !== 4) {
                ret.push(command);
            }
        });
		return ret;
	},
});
core.form_widget_registry.add('tree_project_timeline', FieldOne2ManyProjectTimeline);

// Sự kiện bấm vào dòng "Theo dõi sản xuất" có được sửa không
var X2ManyListProjectManufact = X2ManyListProject.extend({
	init: function(group, opts){
		var self = this;
		this._super.apply(this, arguments);
		this.$current.undelegate('td.o_list_record_selector', 'click').delegate('td.o_list_record_selector', 'click', function (e) {
            e.stopPropagation();
            var current_checked = $(e.currentTarget).find('input').prop('checked');
            self.$current.find('td.o_list_record_selector input').prop('checked', false);
            $(e.currentTarget).find('input').prop('checked', current_checked);

            var selection = self.get_selection();
            var checked = $(e.currentTarget).find('input').prop('checked');
            $(self).trigger('selected', [selection.ids, selection.records, ! checked]);
        });
	},
	row_clicked: function (event) {
		var record_id = $(event.currentTarget).data('id');
		var record = this.records.get(record_id);
		if (!record.attributes.is_total && (PROJECT_CURRENT_ROLE | ROLE_EDIT_MANUFACT) === PROJECT_CURRENT_ROLE)
			return this._super.apply(this, arguments);
		return;
	},
	pad_table_to: function (count) {
		if ((PROJECT_CURRENT_ROLE | ROLE_EDIT_MANUFACT) === PROJECT_CURRENT_ROLE)
			return this._super.apply(this, arguments);
		return;
	},
	render: function () {
		var self = this;
		this.$current.html(
			QWeb.render('ListView.rows', _.extend({}, this, {
					render_cell: function () {
						return self.render_cell.apply(self, arguments); }
				})));
		this.pad_table_to(1);
	},
});
// Danh sách "Theo dõi sản xuất" - Dự án
var One2ManyListViewProjectManufact = One2ManyListViewProjectDetails.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.options.deletable = PROJECT_EDITABLE && (PROJECT_CURRENT_ROLE | ROLE_DEL_MANUFACT) === PROJECT_CURRENT_ROLE;
		this._limit = 0;
		this.options.ListType = X2ManyListProjectManufact;
		this.project_id = null;
		this.insert_index = null;
		this.insert_seq = null;
		this.insert_next_seq = null;
		this.highest_seq = null;

		this.insert_total = false;
		this.total_name = null;
	},
	start: function(){
		var self = this;
		this._super.apply(this, arguments);
		this.$el.before(QWeb.render("ProjectManufactListHead", {
			processes: PROJECT_PROCESS_LIST
		}));

		var $dom = this.$el.parent();

		$dom.find('.v_filters select').on('change', function(){
			self.my_filter();
		});
		// Đăng ký sự kiện lọc bản ghi khi ô nhập giá trị cần tìm nhấn nút Enter
		$dom.find('.v_filter_value').on("keypress", function(e){
			if(e.which === 13) {
				self.my_filter();
			}
		});
		$dom.find('.v_export_record_btn').click(function() {
			var domainx = self.my_filter(true);
			$.when(domainx).then(function(d){
				framework.blockUI();
				self.session.get_file({
					url: '/web/export/project_manufact_excel_export',
					data: {
						data: JSON.stringify({ domain : d })
					},
					complete: framework.unblockUI,
				});
			});
				
		});
		$dom.find('.v_export_template_btn').click(function() {
			framework.blockUI;
			var number = parseInt(window.prompt("Nhập số dòng dữ liệu mẫu cần xuất ra", ""), 10);
			if (!isNaN(number) && number > 0) {
				self.session.get_file({
					url: '/web/export/project_manufact_template_excel_export',
					data: {
						data: JSON.stringify({number_of_rows: number})
					},
					complete: framework.unblockUI
				});
			}
		});

		// Đăng ký sự kiện cho các nút thêm, chèn, ... nếu người dùng được quyền sửa
		if (PROJECT_EDITABLE && (PROJECT_CURRENT_ROLE | ROLE_EDIT_MANUFACT) === PROJECT_CURRENT_ROLE) {
			$dom.find('.v_edit_btns').removeClass('hidden');
			$dom.find('a.v_add_record_btn').click(function(e){
				self.dataset.parent_view.$el.addClass('oe_form_touched');
				self.insert_index = null;
				e.preventDefault();
				e.stopPropagation();
				self.$el.parent().find('.v_insert_b_record_btn').hide();
				self.$el.find('.o_form_field_x2many_list_row_add a').click();
			});
			$dom.find('.v_total_record_btn').click(function() {
				self.insert_total = true;
				self.insert_total_record();
			});
			$dom.find('.v_insert_b_record_btn').click(function(e) {
				return self.start_insertion();
			});
			$dom.find('.v_save_btn').click(function() {
				$.when(self.dataset.parent_view.on_button_save(false)).then(function(){
					self.my_filter();
					self.$el.parent().find('.v_add_record_btn').show();
					self.$el.parent().find('.v_insert_b_record_btn').show();
				});
			});
			$dom.find('.v_cancel_btn').click(function() {
				$.when(self.my_filter()).then(function(){
					self.my_filter();
					self.$el.parent().find('.v_add_record_btn').show();
					self.$el.parent().find('.v_insert_b_record_btn').show();
				});
			});
			if (this.options.deletable) {
				$dom.find('.v_import_record_btn').click(function() {
					self.dataset.parent_view.can_be_discarded().then(function(){
						self.$el.parent().find('input.v_project_import_file').trigger('click');
					});
				});
				$dom.find('input.v_project_import_file').on('change', function(e) {
					framework.blockUI();
					var file = $(this)[0].files[0];
					var formData = new FormData();
					formData.append("file", file, file.name);
					formData.append("pid", PROJECT_ID);
					if (core.csrf_token) {
						formData.append("csrf_token", core.csrf_token)
					}
					$.ajax({
						url: "/web/project/project_import_excel",
						type: "POST",
						data: formData,
						processData: false,
						contentType: false,
						success: function (res) {
							res = parseInt(res);
							if (!isNaN(res) && res > 0) {
								self.my_filter();
								alert(res + " bản ghi đã được cập nhật thành công!")
							}
							else if (res === 0) {
								alert("Không có bản ghi nào được cập nhật, dữ liệu cũ được giữ nguyên!")
							}
							else if (res === -1) {
								alert("Định dạng tệp tin không đúng hoặc tệp tin bị lỗi, vui lòng kiểm tra lại!")
							}
							else {
								alert("Lỗi trong quá trình nhập dữ liệu, vui lòng thử lại sau!")
							}
						},
						fail: function(res){
							alert("Lỗi trong quá trình nhập dữ liệu, vui lòng thử lại sau!")
						},
						complete: framework.unblockUI,
					});
				});
			}
			else {
				$dom.find('.v_import_record_btn').remove();
			}
		}
		else {
			$dom.find('ul.v_more_excel_options').hide();
		}
	},
	start_insertion: function () {
		var self = this;
		var selected_ids = this.get_selected_ids();
		if (selected_ids.length === 1) {
			var selected_record = this.records.get(selected_ids[0]);
			var $row = this.$el.find('tr[data-id="'+ selected_record.attributes.id +'"]');
			var row_index = $row.index();
			var $sibling = $($row[0].previousElementSibling);
			this.insert_index = row_index;
			// var sibling_record = this.records.get($sibling.data('id'));
			var sibling_record = undefined;
			var selected_seq = selected_record.attributes.seq;

			var sibling_def = $.Deferred();
			// if (sibling_record === undefined) {
				// Kiểm tra bản ghi phía trên từ db
				var record = new Model('project.project.manufact').call(
					'get_prev_sibling_by_seq',
					[PROJECT_ID, selected_seq]).then(function(res) {
					if (res && res.seq) {
						sibling_record = {'attributes': {'seq': res.seq, 'next_seq': res.next_seq}};
						sibling_def.resolve();
					}
					else {
						sibling_def.resolve();
					}
				});
			// }
			// else {
			// 	sibling_def.resolve();
			// }
			$.when(sibling_def).done(function(){
				if (sibling_record === undefined) {
					var insert_seq = '',
						computed = false;
					for(var i=selected_seq.length-1;i>=0;i--){
						if (!computed && selected_seq[i] != '0') {
							computed = true;
							insert_seq = (parseInt(selected_seq[i]) - 1) + insert_seq;
						}
						else {
							insert_seq = selected_seq[i] + insert_seq;
						}
					}
					self.insert_seq = insert_seq + '01';
					self.insert_next_seq = insert_seq + '02';
				}
				else {
					var sibling_next_seq = sibling_record.attributes.next_seq;
					// Fill up equals length
					if (sibling_next_seq.length > selected_seq.length) {
						selected_seq += '0'.repeat(sibling_next_seq.length - selected_seq.length);
					}
					else if (sibling_next_seq.length < selected_seq.length) {
						sibling_next_seq = '0'.repeat(selected_seq.length - sibling_next_seq.length) + sibling_next_seq;
					}
					// Compares
					if (sibling_next_seq < selected_seq) {
						self.insert_seq = sibling_next_seq;
						var remember = false,
							insert_next_seq = '',
							add_number = '0'.repeat(sibling_next_seq.length - 1) + '1';
						for(var i = sibling_next_seq.length-1; i >= 0; i--){
							var x = parseInt(sibling_next_seq[i]) + parseInt(add_number[i]);
							if (remember) {
								remember = false;
								x++;
							}
							if (x > 9) {
								remember = true;
							}
							x += '';
							insert_next_seq = x[x.length-1] + insert_next_seq;
						}
						self.insert_next_seq = remember ? '1' + insert_next_seq : insert_next_seq;
					}
					else {
						self.insert_seq = sibling_record.attributes.seq + '01';
						self.insert_next_seq = sibling_record.attributes.seq + '02';
					}
				}
				self.insert_seq = (self.insert_seq.length > 3) ? self.insert_seq : '0'.repeat(4 - self.insert_seq.length) + self.insert_seq;
				self.insert_next_seq = (self.insert_next_seq.length > 3) ? self.insert_next_seq : '0'.repeat(4 - self.insert_next_seq.length) + self.insert_next_seq;
				self.$el.parent().find('.v_add_record_btn').hide();
				self.dataset.parent_view.$el.addClass('oe_form_touched');
				return self.start_edition();
			});
				
		}
		else {
			alert("Hãy tick chọn MỘT bản ghi để chèn!");
			return false;
		}
	},
	start_edition: function (record, options) {
		var self = this;

		if (!this.__focus) {
			this._on_focus_one2many();
		}
		if (!this.highest_seq) {
			this.highest_seq = (this.records.records.length) ? this.records.records[this.records.records.length - 1].attributes.seq : 0;
		}

		var item = false;
		if (record) {
			item = record.attributes;
			this.dataset.select_id(record.get('id'));
		} else {
			record = this.make_empty_record(false);
			this.records.add(record, { at: (this.insert_index !== null) ? this.insert_index : (this.prepends_on_create()) ? 0 : null });
		}

		return this.save_edition().then(function() {
			return $.when.apply($, self.editor.form.render_value_defs);
		}).then(function () {
			var $recordRow = self.groups.get_row_for(record);
			var cells = self.get_cells_for($recordRow);
			var fields = {};
			self.fields_for_resize.splice(0, self.fields_for_resize.length); // Empty array
			return self.with_event('edit', {
				record: record.attributes,
				cancel: false,
			}, function () {
				return self.editor.edit(item, function (field_name, field) {
					var cell = cells[field_name];
					if (!cell) {
						return;
					}
					if (!item) {
						if (field_name == "seq") {
							if (self.insert_index !== null)
								field.set_value(self.insert_seq);
							else {
								++self.highest_seq;
								var highest_seq = self.highest_seq + '';
								field.set_value((highest_seq.length > 3) ? highest_seq : '0'.repeat(4 - highest_seq.length) + highest_seq);
							}
						}
						else if (field_name == "next_seq") {
							if (self.insert_index !== null)
								field.set_value(self.insert_next_seq);
							else {
								var highest_seq = (self.highest_seq + 1) + '';
								field.set_value((highest_seq.length > 3) ? highest_seq : '0'.repeat(4 - highest_seq.length) + highest_seq);
							}
						}
						if (self.insert_total) {
							if (field_name == "process") {
								field.set_value(PROJECT_PROCESS_TOTAL_ID);
							}
							else if (field_name == "name")
								field.set_value(self.total_name);
							else if (field_name == "is_total")
								field.set_value(true);
						}
					}

					// FIXME: need better way to get the field back from bubbling (delegated) DOM events somehow
					field.$el.attr('data-fieldname', field_name);
					fields[field_name] = field;
					self.fields_for_resize.push({field: field, cell: cell});
				}, options).then(function () {
					if (self.insert_total) {
						self.insert_total = false;
						return self.save_edition();
					}
					$recordRow.addClass('o_row_edition');
					self.resize_fields();
					// Local function that returns true if field is visible and editable
					var is_focusable = function(field) {
						return field && field.name == 'name';
					};
					var focus_field = options && options.focus_field ? options.focus_field : undefined;
					if (!is_focusable(fields[focus_field])) {
						focus_field = _.find(self.editor.form.fields_order, function(field) {
							return is_focusable(fields[field]);
						});
					}
					if (fields[focus_field]) {
						fields[focus_field].$el.find('input, textarea').andSelf().filter('input, textarea').select();
					}
					return record.attributes;
				});
			}).fail(function () {
				// if the start_edition event is cancelled and it was a creation, remove the newly-created empty record
				if(!record.get('id')) {
					self.records.remove(record);
				}
			});
		}, function() {
			return $.Deferred().resolve(); // Here the save/cancel edition failed so the start_edition is considered as done and succeeded
		});
	},
	_next: function (next_record, options) {
		var self = this;
		if (this.insert_index !== null) {
			return this.save_edition().then(function () {
				self.start_insertion();
			});
		}
		else {
			return this._super.apply(this, arguments);
		}
	},
	insert_total_record: function(){
		var name = prompt("Nhập tên cho tổng mục này:", '');
		if (name !== null && name != '') {
			this.total_name = name;
			var selected_ids = this.get_selected_ids();
			if (selected_ids.length === 1) {
				this.start_insertion();
			}
			else {
				this.$el.find('.o_form_field_x2many_list_row_add a').click();
			}
		}
	},
	my_filter: function(get_domain){
		var self = this;
		return this._super.apply(this, arguments).then(function(){
			self.filter_domain = [];
			self.filter_domain.push(['project_id', '=', PROJECT_ID]);
			// Tiến trình
			var processes = parseInt(self.$el.parent().find('.v_filter_process').val());
			if (!isNaN(processes) && processes > 0) {
				self.filter_domain.push(['process', '=', processes]);
			}
			// Tên
			var value = self.$el.parent().find('.v_filter_name').val();
			if (value != '') {
				self.filter_domain.push(['name', 'ilike', value]);
			}
			// Hạng mục
			var value = self.$el.parent().find('.v_filter_category').val();
			if (value != '') {
				self.filter_domain.push(['category', 'ilike', value]);
			}
			// Nhà máy
			var value = self.$el.parent().find('.v_filter_factory').val();
			if (value != '') {
				self.filter_domain.push(['factory', 'ilike', value]);
			}
			self.do_search([], [], []);
			if (get_domain===true) {
				return self.filter_domain;
			}
		});
	},
	render_pager: function($node, options) {}
});
var FieldOne2ManyProjectManufact = FieldOne2Many.extend({
	multi_selection: true,
	init: function(){
		this._super.apply(this, arguments);
		this.x2many_views.list = One2ManyListViewProjectManufact;
	},
	get_value: function() {
		var command_list = this._super.apply(this, arguments);
		var ret = [];
		_.each(command_list, function (command) {
            if (command[0] !== 4) {
                ret.push(command);
            }
        });
		return ret;
	},
});
core.form_widget_registry.add('tree_project_manufact', FieldOne2ManyProjectManufact);


// Sự kiện bấm vào dòng "Xuất hàng" có được sửa không
var X2ManyListProjectShipment = X2ManyListProject.extend({
	row_clicked: function (event) {
		if ((PROJECT_CURRENT_ROLE | ROLE_EDIT_SHIPMENT) === PROJECT_CURRENT_ROLE)
			return this._super.apply(this, arguments);
		return;
	},
	pad_table_to: function (count) {
		if ((PROJECT_CURRENT_ROLE | ROLE_EDIT_SHIPMENT) === PROJECT_CURRENT_ROLE)
			return this._super.apply(this, arguments);
		return;
	}
});
// Xuất hàng
var One2ManyListViewProjectShipment = One2ManyListViewProjectDetails.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.options.ListType = X2ManyListProjectShipment;
		this.options.deletable = PROJECT_EDITABLE && (PROJECT_CURRENT_ROLE | ROLE_DEL_SHIPMENT) === PROJECT_CURRENT_ROLE;
	},
	start: function(){
		var self = this;
		this._super.apply(this, arguments);
		this.$el.before(QWeb.render("ProjectShipment"));

		var $dom = this.$el.parent();

		// Đăng ký sự kiện lọc bản ghi khi ô "Lọc theo" thay đổi giá trị
		$dom.find('.v_filter_type').on('change', function(){
			self.my_filter();
		});
		// Đăng ký sự kiện lọc bản ghi khi ô nhập giá trị cần tìm nhấn nút Enter
		$dom.find('.v_filter_value').on("keypress", function(e){
			if(e.which === 13) {
				self.my_filter();
			}
		});
		$dom.find('.v_export_record_btn').click(function() {
			var domainx = self.my_filter(true);
			$.when(domainx).then(function(d){
				framework.blockUI();
				self.session.get_file({
					url: '/web/export/project_project_shipment_export',
					data: {
						data: JSON.stringify({ domain : d })
					},
					complete: framework.unblockUI,
				});
			});
		});
		if (PROJECT_EDITABLE && (PROJECT_CURRENT_ROLE | ROLE_EDIT_SHIPMENT) === PROJECT_CURRENT_ROLE) {
			$dom.find('.v_edit_btns').removeClass('hidden');
			$dom.find('a.v_add_record_btn').click(function(e){
				e.preventDefault();
				e.stopPropagation();
				self.do_action({
					name: "Thêm phiếu xuất hàng",
					view_mode:'project_shipment_popup',
					views : [[false,'project_shipment_popup']],
					res_model:'project.project.shipment',
					type:'ir.actions.act_window',
					target:'new',
					context:{'project_id': PROJECT_ID}
				}, {on_close: function(){
					self.my_filter();
				}});
			});
			$dom.find('.v_save_btn').click(function() {
				$.when(self.dataset.parent_view.on_button_save(false)).then(function(){
					self.my_filter();
				});
			});
			$dom.find('.v_cancel_btn').click(function() {
				$.when(self.my_filter()).then(function(){
					self.my_filter();
				});
			});
		}
	},
	do_activate_record: function(index, id) {
		var self = this;
        var record = this.records.get(id);
		if (record && (PROJECT_CURRENT_ROLE | ROLE_EDIT_SHIPMENT) === PROJECT_CURRENT_ROLE) {
			SHIPMENT_ID = record.attributes.id;
			this.do_action({
				name: "Phiếu xuất hàng",
				view_mode:'project_shipment_popup',
				views : [[false,'project_shipment_popup']],
				res_model:'project.project.shipment',
				type:'ir.actions.act_window',
				res_id:record.attributes.id,
				target:'new',
				context:{'project_id': PROJECT_ID}
			}, {on_close: function(){
				self.my_filter();
			}});
		}
    },
	my_filter: function(get_domain){
		var self = this;
		return this._super.apply(this, arguments).then(function(){
			self.filter_domain = [];
			self.filter_domain.push(['project_id', '=', PROJECT_ID]);
			// Số phiếu
			var value = self.$el.parent().find('.v_filter_number').val();
			if (value != '') {
				self.filter_domain.push(['number', "ilike", value]);
			}
			// Đơn vị vận chuyển
			var value = self.$el.parent().find('.v_filter_transport').val();
			if (value != '') {
				self.filter_domain.push(['transport', "ilike", value]);
			}
			// Xuất từ kho
			var value = self.$el.parent().find('.v_filter_fromx').val();
			if (value != '') {
				self.filter_domain.push(['fromx', "ilike", value]);
			}
			// Người cập nhật
			var value = self.$el.parent().find('.v_filter_write_uid').val();
			if (value != '') {
				self.filter_domain.push(['write_uid', "ilike", value]);
			}
			self.do_search([], [], []);
			if (get_domain===true) {
				return self.filter_domain;
			}
		});
	},
});
var FieldOne2ManyProjectShipment = FieldOne2Many.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.x2many_views.list = One2ManyListViewProjectShipment;
	},
	get_value: function() {
		var command_list = this._super.apply(this, arguments);
		var ret = [];
		_.each(command_list, function (command) {
            if (command[0] !== 4) {
                ret.push(command);
            }
        });
		return ret;
	},
});
core.form_widget_registry.add('tree_project_Shipment', FieldOne2ManyProjectShipment);

// Phiếu cân
var One2ManyListViewShipmentWeight = One2ManyListViewProjectDetails.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.options.ListType = X2ManyListProject;
		this.options.deletable = true;
	},
	start: function(){
		var self = this;
		this._super.apply(this, arguments);
		this.$el.before(QWeb.render("ProjectShipmentWeight"));

		self.$el.parent().find('.v_export_record_btn').click(function() {
			var domainx = self.my_filter(true);
			$.when(domainx).then(function(d){
				framework.blockUI();
				self.session.get_file({
					url: '/web/export/project_shipment_weight_export',
					data: {
						data: JSON.stringify({ domain : d })
					},
					complete: framework.unblockUI,
				});
			});
		});
		if (PROJECT_EDITABLE && (IS_ADMIN || (PROJECT_CURRENT_ROLE | ROLE_EDIT_SHIPMENT) === PROJECT_CURRENT_ROLE)) {
			self.$el.parent().find('.v_edit_btns').removeClass('hidden');
			self.$el.parent().find('a.v_add_record_btn').click(function(e){
				e.preventDefault();
				e.stopPropagation();
				self.$el.find('.o_form_field_x2many_list_row_add a').click();
			});
		}
	},
	my_filter: function(get_domain){
		var self = this;
		return this._super.apply(this, arguments).then(function(){
			self.filter_domain = [];
			self.filter_domain.push(['shipment_id', '=', SHIPMENT_ID]);
			// self.do_search([], [], []);
			if (get_domain===true) {
				return self.filter_domain;
			}
		});
	},
});
var FieldOne2ManyProjectShipmentWeight = FieldOne2Many.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.x2many_views.list = One2ManyListViewShipmentWeight;
	},
	get_value: function() {
		var command_list = this._super.apply(this, arguments);
		var ret = [];
		_.each(command_list, function (command) {
            if (command[0] !== 4) {
                ret.push(command);
            }
        });
		return ret;
	},
});
core.form_widget_registry.add('tree_project_shipment_weight', FieldOne2ManyProjectShipmentWeight);


// Sự kiện bấm vào dòng "Chi phí công trường" có được sửa không
var X2ManyListProjectCosts = X2ManyListProject.extend({
	row_clicked: function (event) {
		if ((PROJECT_CURRENT_ROLE | ROLE_EDIT_COSTS) === PROJECT_CURRENT_ROLE)
			return this._super.apply(this, arguments);
		return;
	},
	pad_table_to: function (count) {
		if ((PROJECT_CURRENT_ROLE | ROLE_EDIT_COSTS) === PROJECT_CURRENT_ROLE)
			return this._super.apply(this, arguments);
		return;
	}
});
// Chi phí công trường
var One2ManyListViewProjectCosts = One2ManyListViewProjectDetails.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.options.ListType = X2ManyListProjectCosts;
		this.options.deletable = PROJECT_EDITABLE && (PROJECT_CURRENT_ROLE | ROLE_DEL_COSTS) === PROJECT_CURRENT_ROLE;
	},
	start: function(){
		var self = this;
		this._super.apply(this, arguments);
		var head_rendered = QWeb.render("ProjectCostsHead");
		var $head_dom = $(head_rendered);
		this.$el.before(head_rendered);

		$('.v_costs_total').before(QWeb.render("ProjectCosts", {
			year: (new Date()).getFullYear()
		}));

		// Đăng ký sự kiện lọc bản ghi khi ô "Lọc theo" thay đổi giá trị
		$('.v_costs').find('.v_filters select').on('change', function(){
			self.my_filter();
		});

		// Đăng ký sự kiện lọc bản ghi khi ô nhập giá trị cần tìm nhấn nút Enter
		$('.v_costs').find('.v_filter_value').on("keypress", function(e){
			if(e.which === 13) {
				self.my_filter();
			}
		});
		self.$el.parent().find('.v_export_record_btn').click(function() {
			var domainx = self.my_filter(true);
			$.when(domainx).then(function(d){
				framework.blockUI();
				self.session.get_file({
					url: '/web/export/project_project_costs_export',
					data: {
						data: JSON.stringify({ domain : d })
					},
					complete: framework.unblockUI,
				});
			});
		});

		if (PROJECT_EDITABLE && (PROJECT_CURRENT_ROLE | ROLE_EDIT_COSTS) === PROJECT_CURRENT_ROLE) {
			self.$el.parent().find('.v_edit_btns').removeClass('hidden');
			self.$el.parent().find('a.v_add_record_btn').click(function(e){
				e.preventDefault();
				e.stopPropagation();
				self.do_action({
					name: "Thêm Chi phí công trường",
					view_mode:'project_costs_popup',
					views : [[false,'project_costs_popup']],
					res_model:'project.project.costs',
					type:'ir.actions.act_window',
					target:'new',
					context:{'project_id': PROJECT_ID}
				}, {on_close: function(){
					self.my_filter();
				}});
			});
			self.$el.parent().find('.v_save_btn').click(function() {
				$.when(self.dataset.parent_view.on_button_save(false)).then(function(){
					self.my_filter();
				});
			});
			self.$el.parent().find('.v_cancel_btn').click(function() {
				$.when(self.my_filter()).then(function(){
					self.my_filter();
				});
			});
		}	
	},
	do_activate_record: function(index, id) {
		var self = this;
        var record = this.records.get(id);
		if (record && (PROJECT_CURRENT_ROLE | ROLE_EDIT_COSTS) === PROJECT_CURRENT_ROLE) {
			this.do_action({
				name: "Chi phí công trường",
				view_mode:'project_costs_popup',
				views : [[false,'project_costs_popup']],
				res_model:'project.project.costs',
				type:'ir.actions.act_window',
				res_id:record.attributes.id,
				target:'new',
				context:{'project_id': PROJECT_ID}
			}, {on_close: function(){
				self.my_filter();
			}});
		}
    },
	my_filter: function(get_domain){
		var self = this;
		return this._super.apply(this, arguments).then(function(){
			self.filter_domain = [];
			self.filter_domain.push(['project_id', '=', PROJECT_ID]);
			// Đa tiêu chí
			var typex = $('.v_costs').find('.v_filter_type').val();
			if (typex != '' && ['create_uid', 'approved_uid'].indexOf(typex) !== -1) {
				var value = $('.v_costs').find('.v_filter_value').val();
				if (value != '') {
					self.filter_domain.push([typex, "ilike", value]);
				}
			}
			// Tháng
			var month = parseInt($('.v_costs').find('.v_filter_month').val());
			if(!isNaN(month) && month > 0 && month < 13){
				self.filter_domain.push(['month', "=", month]);
			}
			// Năm
			var year  = parseInt($('.v_costs').find('.v_filter_year').val());
			if(!isNaN(year) && year > 0){
				self.filter_domain.push(['year', "=", year]);
			}
			self.do_search([], [], []).then(function(){
				var income = 0, outcome = 0, rest = 0;
	        	$.each(self.records.records, function(index, r){
	        		income += parseInt(r.attributes.income) || 0;
	        		outcome += parseInt(r.attributes.outcome) || 0;
	        	});
	        	rest = income - outcome;
	        	if (isNaN(income)) {income = 0}
	        	if (isNaN(outcome)) {outcome = 0}
	        	if (isNaN(rest)) {rest = 0}
	        	self.dataset.parent_view.fields.costs_total_income.$el.text(self.addCommas(income));
	        	self.dataset.parent_view.fields.costs_total_outcome.$el.text(self.addCommas(outcome));
	        	self.dataset.parent_view.fields.costs_total_rest.$el.text(self.addCommas(rest));
			});
			if (get_domain===true) {
				return self.filter_domain;
			}
		});
	},
	do_delete: function (ids) {
        if (!(ids.length)) {
            return;
        }
        var self = this;
        return $.when(this.dataset.unlink(ids)).done(function () {
            _(ids).each(function (id) {
                self.records.remove(self.records.get(id));
            });
            // Hide the table if there is no more record in the dataset
            if (self.display_nocontent_helper()) {
                self.no_result();
            }
            self.update_pager(self.dataset);
            self.compute_aggregates();

            var income = 0, outcome = 0, rest = 0;
        	$.each(self.records.records, function(index, r){
        		income += parseInt(r.attributes.income) || 0;
        		outcome += parseInt(r.attributes.outcome) || 0;
        	});
        	rest = income - outcome;
        	if (isNaN(income)) {income = 0}
        	if (isNaN(outcome)) {outcome = 0}
        	if (isNaN(rest)) {rest = 0}
        	self.dataset.parent_view.fields.costs_total_income.$el.text(self.addCommas(income));
        	self.dataset.parent_view.fields.costs_total_outcome.$el.text(self.addCommas(outcome));
        	self.dataset.parent_view.fields.costs_total_rest.$el.text(self.addCommas(rest));
        });
    },
    save_edition: function (cancel_onfail) {
        var self = this;
        return this._super.apply(this, arguments).then(function(){
        	var income = 0, outcome = 0, rest = 0;
        	$.each(self.records.records, function(index, r){
        		income += parseInt(r.attributes.income) || 0;
        		outcome += parseInt(r.attributes.outcome) || 0;
        	});
        	rest = income - outcome;
        	if (isNaN(income)) {income = 0}
        	if (isNaN(outcome)) {outcome = 0}
        	if (isNaN(rest)) {rest = 0}
        	self.dataset.parent_view.fields.costs_total_income.$el.text(self.addCommas(income));
        	self.dataset.parent_view.fields.costs_total_outcome.$el.text(self.addCommas(outcome));
        	self.dataset.parent_view.fields.costs_total_rest.$el.text(self.addCommas(rest));
        });
    },
    addCommas: function(n){
    	n += '';
        if (n.length > 19)
            return '0';
        n = n.replace(/\./g, '');
        var is_valid = true;
        var count_dot = 0;
        for(var i=0; i<n.length; i++) {
            if(!$.isNumeric(n[i]) && n[i] != ',') {
                is_valid = false;
                break;
            }
            if (n[i] == ',')
                count_dot++;
        }
        if (count_dot > 1)
            is_valid = false;

        if (! is_valid) {
            n = parseFloat(n) + '';
        }
        var x = n.split(',');
        var x1 = parseFloat(x[0]) + '';
        // var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        var res = x1;// + x2;
        if (res == "NaN")
            res = '';
        return res;
    },
});
var FieldOne2ManyProjectCosts = FieldOne2Many.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.x2many_views.list = One2ManyListViewProjectCosts;
	},
	get_value: function() {
		var command_list = this._super.apply(this, arguments);
		var ret = [];
		_.each(command_list, function (command) {
            if (command[0] !== 4) {
                ret.push(command);
            }
        });
		return ret;
	},
});
core.form_widget_registry.add('tree_project_Costs', FieldOne2ManyProjectCosts);

// Danh sách dự án
var One2ManyListViewProject = One2ManyListView.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.options.ListType = X2ManyListProject;
		this.filter_domain = [];
		this.current_tab = 'customer';
		this.$filters = null;
	},
	start: function(){
		var self = this;
		$.when(this._super.apply(this, arguments)).then(function(){
			function render(){
				$('a.v_project_customer').click(function(){
					if ($(this).hasClass('active')) {
						return;
					}
					self.$el.addClass('v_project_customer');
					self.$el.removeClass('v_project_internal');
					PROJECT_OPEN_DETAIL = true;
					$('.v_project_tab > a').removeClass('active');
					$(this).addClass('active');

					self.$el.parent().parent().find('.v_filters').removeClass('v_filters_internal');

					self.current_tab = 'customer';
					self.my_filter();
				});
				if (CAN_VIEW_INTERNAL_PROJECT || CAN_VIEW_ALL_INTERNAL_PROJECT) {
					$('a.v_project_internal').removeClass('hidden');
					$('a.v_project_internal').click(function(){
						if ($(this).hasClass('active')) {
							return;
						}
						self.$el.removeClass('v_project_customer');
						self.$el.addClass('v_project_internal');
						PROJECT_OPEN_DETAIL = false;
						$('.v_project_tab > a').removeClass('active');
						$(this).addClass('active');

						self.$el.parent().parent().find('.v_filters').addClass('v_filters_internal');

						self.current_tab = 'internal';
						self.my_filter();
					});
				}
				else {
					$('a.v_project_internal').remove();
				}
					
				$('a.v_export_project_btn').click(function(){
					framework.blockUI;
					self.session.get_file({
		                url: '/web/export/project_project_export',
		                data: {
		                    data: JSON.stringify({
		                        domain : self.my_filter(true),
		                        is_internal: self.current_tab == 'customer' ? false : true
		                    })
		                },
		                complete: framework.unblockUI
		            });
				});
			}
			function wait(){
				setTimeout(function(){
					if ($('a.v_project_internal').length) {
						render();
					}
					else {
						wait();
					}
				}, 50);
			}
			wait();
		});
	},
	load_list: function() {
		var self = this;
		return $.when(this._super.apply(this, arguments)).done(function(){
			self.$filters = $(self.$el.parent().parent().find('.v_filters'));
			self.$filters.find('select').unbind('change').change(function(){
				self.my_filter();
			});
			self.$filters.find('.v_filter_text').unbind('keypress').keypress(function(e){
				if(e.which === 13) {
					self.my_filter();
				}
			});
		});
	},
	reload_record: function(record, options) {
		if (!record)
			return;
		this._super.apply(this, arguments);
	},
	do_activate_record: function(index, id) {
		if (!PROJECT_OPEN_DETAIL)
			return;
		var record = this.records.get(id);
		if (record) {
			this.do_action({
				name: record.attributes.name,
				view_mode:'project_project_detail',
				views : [[false,'project_project_detail']],
				res_model:'project.project',
				type:'ir.actions.act_window',
				res_id:record.attributes.id,
				target:'self',
			});
		}
	 },
	do_add_record: function () {
		var self = this;
		this.do_action(
			PROJECT_OPEN_DETAIL ? 'project.action_project_project_popup' : 'project.action_project_project_popup_internal',
			{additional_context: {'is_internal': this.current_tab == 'customer' ? false : true} }
		)
	},
	remove_pay: function(){
		var pay = $('a[role="tab"][data-str="Thanh toán"]');
		var content_tab_id = pay.attr('href');
		$('div'+ content_tab_id).remove();
		pay.parent().remove();
	},
	show_pay: function(){
		$('a[role="tab"][data-str="Thanh toán"]').parent().removeClass('o_form_invisible');
	},
	do_show: function () {
		var self = this;
		this.$el.addClass('v_project_customer');
		if (!PROJECT_CAN_VIEW_I_PAY && !PROJECT_CAN_VIEW_C_PAY) {
			this.remove_pay();
		}
		else {
			this.show_pay();
		}
		return $.when(this._super.apply(this, arguments)).done(function(){
			self.filter_domain.push(["is_internal", "=", false]);
			if (!CAN_EDIT_PROJECT) {
				self.filter_domain.push(['existed_role_uids', 'like', ',' + self.session.uid + ',']);
			}
			return self.do_search([],[],[]);
		});
	},
	my_filter: function(get_domain){
		this.filter_domain = [];
		if (this.current_tab == 'customer') {
			// Đa tiêu chí
			var typex = this.$filters.find('.v_filter_c_type').val();
			if (typex != '' && ['contractor_id', 'chairman_str'].indexOf(typex) !== -1) {
				var value = this.$filters.find('.v_filter_c_value').val();
				if (value != '') {
					this.filter_domain.push([typex, "ilike", value]);
				}
			}
			// Trạng thái
			var state = parseInt(this.$filters.find('.v_filter_c_status').val(), 10);
			if (!isNaN(state) && state > 0) {
				this.filter_domain.push(['status_id', '=', state]);
			}
			// Công ty
			var company = parseInt(this.$filters.find('.v_filter_c_company').val(), 10);
			if (!isNaN(company) && company > 0) {
				this.filter_domain.push(['company_id', '=', company]);
			}
			// Mã CV
			var code = this.$filters.find('.v_filter_c_code').val();
			if (code != '') {
				this.filter_domain.push(['code', "ilike", code]);
			}
			// Tên dự án
			var name = this.$filters.find('.v_filter_c_name').val();
			if (name != '') {
				this.filter_domain.push(['name', "ilike", name]);
			}
			// Năm
			var year = parseInt(this.$filters.find('.v_filter_c_year').val(), 10);
			if (!isNaN(year) && year > 0) {
				this.filter_domain.push(['year', '=', year + '']);
			}
			this.filter_domain.push(["is_internal", "=", false]);
			if (!IS_ADMIN && !CAN_EDIT_PROJECT) {
				this.filter_domain.push(['existed_role_uids', 'like', ',' + this.session.uid + ',']);
			}
		}
		else {
			// Trạng thái
			var state = parseInt(this.$filters.find('.v_filter_i_status').val(), 10);
			if (!isNaN(state) && state > 0) {
				this.filter_domain.push(['status_id', '=', state]);
			}
			// Năm
			var year = parseInt(this.$filters.find('.v_filter_i_year').val(), 10);
			if (!isNaN(year) && year > 0) {
				this.filter_domain.push(['year', '=', year + '']);
			}
			// Bên mua
			var buyer = parseInt(this.$filters.find('.v_filter_i_buyer').val(), 10);
			if (!isNaN(buyer) && buyer > 0) {
				this.filter_domain.push(['project_buyer', '=', buyer]);
			}
			// Bên bán
			var seller = parseInt(this.$filters.find('.v_filter_i_seller').val(), 10);
			if (!isNaN(seller) && seller > 0) {
				this.filter_domain.push(['project_seller', '=', seller]);
			}
			// Loại hình CV
			var ptype = parseInt(this.$filters.find('.v_filter_i_ptype').val(), 10);
			if (!isNaN(ptype) && ptype > 0) {
				this.filter_domain.push(['project_type', '=', ptype]);
			}
			// Người theo dõi
			var value = this.$filters.find('.v_filter_i_follower').val();
			if (value != '') {
				this.filter_domain.push(['follower_str', "ilike", value]);
			}



			this.filter_domain.push(["is_internal", "=", true]);
			if (!CAN_VIEW_ALL_INTERNAL_PROJECT) {
				this.filter_domain.push(['existed_role_uids', 'like', ',' + this.session.uid + ',']);
			}
		}
			
		if (get_domain === true) {
			return this.filter_domain;
		}
		this.do_search([], this.last_context, this.last_group_by);
	},
	do_search: function(domain, context, group_by) {
		this.last_domain = domain;
		this.last_context = context;
		this.last_group_by = group_by;
		this.old_search = _.bind(this._super, this);
		return this.search();
	},
	search: function() {
		var compound_domain = new data.CompoundDomain(this.last_domain, this.filter_domain);
		this.dataset.domain = compound_domain.eval();
		return this.old_search(compound_domain, this.last_context, this.last_group_by);
	}
});
var FieldOne2ManyProject = FieldOne2Many.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.x2many_views.list = One2ManyListViewProject;
	},
	start: function() {
		var self = this;
		return $.when(this._super.apply(this, arguments)).then(function(){
			function render(){
				self.$el.before(QWeb.render("ProjectListHead", {
					companies: COMPANIES_DATA,
					status: PROJECT_STATUS_LIST,
					ptype: PROJECT_TYPE_LIST,
					year: (new Date()).getFullYear()
				}));
				if (IS_ADMIN || CAN_EDIT_PROJECT) {
					self.$el.parent().find('a.v_add_project_btn').click(function(e){
						e.preventDefault();
						e.stopPropagation();
						self.$el.find('.o_form_field_x2many_list_row_add a').click();
					});
				}
				else {
					self.$el.parent().find('a.v_add_project_btn').remove();
				}
			}
			function wait(){
				setTimeout(function(){
					if (CAN_QUICK_ADD_PARTNER !== undefined) {
						render();
					}
					else {
						wait();
					}
				}, 50);
			}
			wait();
		});
	}
});
core.form_widget_registry.add('tree_project', FieldOne2ManyProject);


// Danh sách thanh toán
var One2ManyListViewProjectPay = One2ManyListView.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.options.ListType = X2ManyListProject;
		this.filter_domain = [];
		this.current_tab = 'customer';
		this.$filters = null;
	},
	start: function(){
		var self = this;
		$.when(this._super.apply(this, arguments)).then(function(){
			function render(){
				$('a.v_pay_customer').click(function(){
					if ($(this).hasClass('active')) {
						return;
					}
					self.$el.addClass('v_pay_customer');
					self.$el.removeClass('v_pay_internal');
					PROJECT_PAY_CUSTOMER = true;
					$('.v_pay_tab > a').removeClass('active');
					$(this).addClass('active');

					self.$el.parent().parent().find('.v_filters').removeClass('v_filters_internal');
					self.current_tab = 'customer';
					self.my_filter();
				});
				if (PROJECT_CAN_VIEW_I_PAY) {
					$('a.v_pay_internal').click(function(){
						if ($(this).hasClass('active')) {
							return;
						}
						self.$el.removeClass('v_pay_customer');
						self.$el.addClass('v_pay_internal');
						PROJECT_PAY_CUSTOMER = false;
						$('.v_pay_tab > a').removeClass('active');
						$(this).addClass('active');

						self.$el.parent().parent().find('.v_filters').addClass('v_filters_internal');
						self.current_tab = 'internal';
						self.my_filter();
					});
				}
				else {
					$('a.v_pay_internal').remove();
				}
					
				$('a.v_export_pay_btn').click(function(){
					framework.blockUI;
					self.session.get_file({
		                url: '/web/export/project_pay_export',
		                data: {
		                    data: JSON.stringify({
		                        domain : self.my_filter(true),
		                        is_internal: self.current_tab == 'customer' ? false : true
		                    })
		                },
		                complete: framework.unblockUI
		            });
				});
			}
			function wait(){
				setTimeout(function(){
					if ($('a.v_pay_customer').length) { render(); }
					else { wait(); }
				}, 50);
			}
			wait();
		});
	},
	load_list: function() {
		var self = this;
		return $.when(this._super.apply(this, arguments)).done(function(){
			self.$filters = $(self.$el.parent().parent().find('.v_filters'));
			self.$filters.find('select').unbind('change').change(function(){
				self.my_filter();
			});
			self.$filters.find('.v_filter_text').unbind('keypress').keypress(function(e){
				if(e.which === 13) {
					self.my_filter();
				}
			});
		});
	},
	do_add_record: function() {
		return;
	},
	do_activate_record: function(index, id) {
		var record = this.records.get(id);
		if (record) {
			PROJECT_ID = record.attributes.id;
			PROJECT_EDITABLE = record.attributes.status_id[0] != PROJECT_STATUS_END_ID;
			this.do_action({
				name: record.attributes.name,
				view_mode:'project_pay_popup',
				views : [[false,'project_pay_popup']],
				res_model:'project.project',
				type:'ir.actions.act_window',
				res_id:record.attributes.id,
				target:'new',
				context: {'edit_payment':true}
			});
		}
	},
	do_show: function() {
		var self = this;
		this.$el.addClass('v_pay_customer');
		return $.when(this._super.apply(this, arguments)).done(function(){
			self.filter_domain = [];
			self.filter_domain.push(["is_internal", "=", self.current_tab == 'customer' ? false : true]);
			if (!CAN_VIEW_PAY && (COUNT_I_PAY > 0 || COUNT_C_PAY > 0)) {
				// TODO: Sửa điều kiện này nếu chỉ cho Chủ nhiệm & Người theo dõi xem thanh toán (ko phải toàn bộ nhân sự nội bộ)
				self.filter_domain.push(['muids', 'like', ',' + self.session.uid + ',']);
			}
			return self.do_search([],[],[]);
		});
	},
	my_filter: function(get_domain){
		var self = this;
		this.filter_domain = [];
		this.filter_domain.push(["is_internal", "=", this.current_tab == 'customer' ? false : true]);

		if (this.current_tab == 'customer') {
			// Đa tiêu chí
			var typex = this.$filters.find('.v_filter_c_type').val();
			if (typex != '' && ['name', 'contractor_id', 'chairman_str'].indexOf(typex) !== -1) {
				var value = this.$filters.find('.v_filter_c_value').val();
				if (value != '') {
					this.filter_domain.push([typex, "ilike", value]);
				}
			}
			// Trạng thái CV
			var state = parseInt(this.$filters.find('.v_filter_c_status').val(), 10);
			if (!isNaN(state) && state > 0) {
				this.filter_domain.push(['status_id', '=', state]);
			}
			// Trạng thái TT
			var pstate = parseInt(this.$filters.find('.v_filter_c_pstatus').val(), 10);
			if (!isNaN(pstate) && pstate > 0) {
				this.filter_domain.push(['pay_status_id', '=', pstate]);
			}
			// Công ty
			var company = parseInt(this.$filters.find('.v_filter_c_company').val(), 10);
			if (!isNaN(company) && company > 0) {
				this.filter_domain.push(['company_id', '=', company]);
			}
			// Năm
			var year = parseInt(this.$filters.find('.v_filter_c_year').val(), 10);
			if (!isNaN(year) && year > 0) {
				this.filter_domain.push(['year', '=', year + '']);
			}
			// Mã CV
			var code = this.$filters.find('.v_filter_c_code').val();
			if (code != '') {
				this.filter_domain.push(['code', "ilike", code]);
			}
			// Tên dự án
			var name = this.$filters.find('.v_filter_c_name').val();
			if (name != '') {
				this.filter_domain.push(['name', "ilike", name]);
			}

		}
		else {
			// Trạng thái CV
			var state = parseInt(this.$filters.find('.v_filter_i_status').val(), 10);
			if (!isNaN(state) && state > 0) {
				this.filter_domain.push(['status_id', '=', state]);
			}
			// Trạng thái TT
			var pstate = parseInt(this.$filters.find('.v_filter_i_pstatus').val(), 10);
			if (!isNaN(pstate) && pstate > 0) {
				this.filter_domain.push(['pay_status_id', '=', pstate]);
			}
			// Năm
			var year = parseInt(this.$filters.find('.v_filter_i_year').val(), 10);
			if (!isNaN(year) && year > 0) {
				this.filter_domain.push(['year', '=', year + '']);
			}
			// Bên bán
			var seller = parseInt(this.$filters.find('.v_filter_i_seller').val(), 10);
			if (!isNaN(seller) && seller > 0) {
				this.filter_domain.push(['project_seller', '=', seller]);
			}
			// Bên mua
			var buyer = parseInt(this.$filters.find('.v_filter_i_buyer').val(), 10);
			if (!isNaN(buyer) && buyer > 0) {
				this.filter_domain.push(['project_buyer', '=', buyer]);
			}
			// Loại hình CV
			var ptype = parseInt(this.$filters.find('.v_filter_i_ptype').val(), 10);
			if (!isNaN(ptype) && ptype > 0) {
				this.filter_domain.push(['project_type', '=', ptype]);
			}
			// Người theo dõi
			var value = this.$filters.find('.v_filter_i_value').val();
			if (value != '') {
				this.filter_domain.push(['follower_str', "ilike", value]);
			}
		}
		if (!CAN_VIEW_PAY && (COUNT_I_PAY > 0 || COUNT_C_PAY > 0)) {
			// TODO: Sửa điều kiện này nếu chỉ cho Chủ nhiệm & Người theo dõi xem thanh toán (ko phải toàn bộ nhân sự nội bộ)
			this.filter_domain.push(['muids', 'like', ',' + self.session.uid + ',']);
		}

		if (get_domain === true) {
			return this.filter_domain;
		}
		this.do_search([], self.last_context, self.last_group_by);
	},
	do_search: function(domain, context, group_by) {
		this.last_domain = domain;
		this.last_context = context;
		this.last_group_by = group_by;
		this.old_search = _.bind(this._super, this);
		return this.search();
	},
	search: function() {
		var domain = this.filter_domain;
		var compound_domain = new data.CompoundDomain(this.last_domain, domain);
		this.dataset.domain = compound_domain.eval();
		return this.old_search(compound_domain, this.last_context, this.last_group_by);
	}
});
var FieldOne2ManyProjectPay = FieldOne2Many.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.x2many_views.list = One2ManyListViewProjectPay;
	},
	start: function() {
		var self = this;
		return $.when(this._super.apply(this, arguments)).then(function(){
			function render(){
				self.$el.before(QWeb.render("ProjectPayListHead", {
					companies: COMPANIES_DATA,
					status: PROJECT_STATUS_LIST,
					pstatus: PROJECT_PAY_STATUS_LIST,
					ptype: PROJECT_TYPE_LIST,
					year: (new Date()).getFullYear()
				}));
			}
			function wait(){
				setTimeout(function(){
					if (COMPANIES_DATA !== undefined) {
						render();
					}
					else {
						wait();
					}
				}, 50);
			}
			wait();
		});
	},
});
core.form_widget_registry.add('tree_pay', FieldOne2ManyProjectPay);


// Bỏ nút 'Thêm một hạng mục' nếu người dùng không có quyền tạo
var X2ManyListProjectPayPaid = X2ManyListProject.extend({
	pad_table_to: function (count) {
		if (PROJECT_EDITABLE && CAN_EDIT_PAY_PAID)
			return this._super.apply(this, arguments);
		return;
	},
	row_clicked: function (event) {
		if (PROJECT_EDITABLE && CAN_EDIT_PAY_PAID)
			return this._super.apply(this, arguments);
		return;
	},
	render_record: function (record) {
		var self = this;
		var index = this.records.indexOf(record);
		record.attributes.order = index + 1;
		return QWeb.render('ListView.row', {
			columns: this.columns,
			options: this.options,
			record: record,
			row_parity: (index % 2 === 0) ? 'even' : 'odd',
			view: this.view,
			render_cell: function () {
				return self.render_cell.apply(self, arguments); }
		});
	}
});
// Chi tiết thanh toán
var One2ManyListViewProjectPaydetail = One2ManyListView.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.options.ListType = X2ManyListProjectPayPaid;
		this.options.deletable = PROJECT_EDITABLE && CAN_EDIT_PAY_PAID;
		this.filter_domain = [];
	},
	start: function(){
		var self = this;
		this._super.apply(this, arguments);
		this.$el.addClass('v_project_pay_detail');
		if (PROJECT_PAY_CUSTOMER) {
			this.$el.removeClass('v_pay_detail_internal');
		}
		else {
			this.$el.addClass('v_pay_detail_internal');
		}
		this.$el.before(QWeb.render("ProjectPayDetail"));

		var $dom = this.$el.parent();

		if (PROJECT_EDITABLE && CAN_EDIT_PAY_PAID) {
			$dom.find('a.v_add_record_btn').click(function(e){
				e.preventDefault();
				e.stopPropagation();
				self.$el.find('.o_form_field_x2many_list_row_add a').click();
			});
		}
		else {
			$dom.find('a.v_add_record_btn').remove();
		}

		// Đăng ký sự kiện lọc bản ghi khi ô "Lọc theo" thay đổi giá trị
		$dom.find('.v_filter_type').on('change', function(){
			self.my_filter();
		});
		// Đăng ký sự kiện lọc bản ghi khi ô nhập giá trị cần tìm nhấn nút Enter
		$dom.find('.v_filter_value').on("keypress", function(e){
			if(e.which === 13) {
				self.my_filter();
			}
		});
		$dom.find('.v_export_record_btn').click(function() {
			framework.blockUI;
			self.session.get_file({
                url: '/web/export/project_pay_detail_export',
                data: {
                    data: JSON.stringify({
                        domain : self.my_filter(true),
                        is_internal: !PROJECT_PAY_CUSTOMER
                    })
                },
                complete: framework.unblockUI
            });
		});
	},
	// do_activate_record: function(index, id) {
	// 	return;
	// },
	load_list: function() {
		var self = this;
		return $.when(this._super.apply(this, arguments)).done(function(){
			self.$el.parent().find('table tbody td[data-field="order"]').each(function(i, e){
				$(e).html(i + 1);
			});
		});
	},
	my_filter: function(get_domain){
		var self = this;
		this.filter_domain = [];
		this.filter_domain.push(['project_id', '=', PROJECT_ID]);
		if (get_domain===true)
			return this.filter_domain;
		this.do_search([], [], []);
	},
	do_search: function(domain, context, group_by) {
		this.last_domain = domain;
		this.last_context = context;
		this.last_group_by = group_by;
		this.old_search = _.bind(this._super, this);
		return this.search();
	},
	search: function() {
		var self = this;
		var domain = this.filter_domain;
		var compound_domain = new data.CompoundDomain(self.last_domain, domain);
		self.dataset.domain = compound_domain.eval();
		return self.old_search(compound_domain, self.last_context, self.last_group_by);
	}
});
var FieldOne2ManyProjectPayDetail = FieldOne2Many.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.x2many_views.list = One2ManyListViewProjectPaydetail;
	},
});
core.form_widget_registry.add('tree_project_pay_detail', FieldOne2ManyProjectPayDetail);


// Bỏ nút 'Thêm một hạng mục' nếu người dùng không có quyền tạo
var X2ManyListProjectPayNext = X2ManyListProject.extend({
	pad_table_to: function (count) {
		if (PROJECT_EDITABLE && CAN_EDIT_PAY_NEXT)
			return this._super.apply(this, arguments);
		return;
	},
	row_clicked: function (event) {
		if (PROJECT_EDITABLE && CAN_EDIT_PAY_NEXT)
			return this._super.apply(this, arguments);
		return;
	},
});
// Dự kiến thanh toán
var One2ManyListViewProjectPayExpected = One2ManyListView.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.options.ListType = X2ManyListProjectPayNext;
		this.options.deletable = PROJECT_EDITABLE && CAN_DEL_PAY_NEXT;
		this.filter_domain = [];
	},
	start: function(){
		var self = this;
		this._super.apply(this, arguments);
		this.$el.before(QWeb.render("ProjectPayExpected"));


		var $dom = this.$el.parent();

		if (PROJECT_EDITABLE && CAN_EDIT_PAY_NEXT) {
			$dom.find('a.v_add_record_btn').click(function(e){
				e.preventDefault();
				e.stopPropagation();
				self.$el.find('.o_form_field_x2many_list_row_add a').click();
			});
		}
		else {
			$dom.find('a.v_add_record_btn').remove();
		}

		// Đăng ký sự kiện lọc bản ghi khi ô "Lọc theo" thay đổi giá trị
		$dom.find('.v_filter_type').on('change', function(){
			self.my_filter();
		});
		// Đăng ký sự kiện lọc bản ghi khi ô nhập giá trị cần tìm nhấn nút Enter
		$dom.find('.v_filter_value').on("keypress", function(e){
			if(e.which === 13) {
				self.my_filter();
			}
		});
		$dom.find('.v_export_record_btn').click(function() {
			framework.blockUI;
			self.session.get_file({
                url: '/web/export/project_pay_expected_export',
                data: {
                    data: JSON.stringify({
                        domain : self.my_filter(true)
                    })
                },
                complete: framework.unblockUI
            });
		});
	},
	do_activate_record: function(index, id) {
		return;
	},
	load_list: function() {
		var self = this;
		return $.when(this._super.apply(this, arguments)).done(function(){
			self.$el.parent().find('table tbody td[data-field="order"]').each(function(i, e){
				$(e).html(i + 1);
			});
		});
	},
	my_filter: function(get_domain){
		var self = this;
		this.filter_domain = [];
		this.filter_domain.push(['project_id', '=', PROJECT_ID]);
		if (get_domain===true)
			return this.filter_domain;
		this.do_search([], [], []);
	},
	do_search: function(domain, context, group_by) {
		this.last_domain = domain;
		this.last_context = context;
		this.last_group_by = group_by;
		this.old_search = _.bind(this._super, this);
		return this.search();
	},
	search: function() {
		var self = this;
		var domain = this.filter_domain;
		var compound_domain = new data.CompoundDomain(self.last_domain, domain);
		self.dataset.domain = compound_domain.eval();
		return self.old_search(compound_domain, self.last_context, self.last_group_by);
	}
});
var FieldOne2ManyProjectPayExpected = FieldOne2Many.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.x2many_views.list = One2ManyListViewProjectPayExpected;
	},
});
core.form_widget_registry.add('tree_project_pay_expected', FieldOne2ManyProjectPayExpected);

// Số lượng - Phần trăm : Theo dõi sản xuất - Dự án
var ProjectManufactForm = form_common.AbstractField.extend({
	template: 'ProjectManufactForm',
	events: {
		'keyup input': 'change'
	},
	start: function() {
		var self = this;
		this.$input = this.$el.find('input');
		this.$span = this.$el.find('span');
		return $.when(this._super()).then(function(){
			$('select[name="process"]').change(function(){
				if (!PROJECT_PROCESS_CRITE_CURRENT) {
					self.$el.hide();
				}
				else {
					if (PROJECT_PROCESS_CRITE_REL[PROJECT_PROCESS_CRITE_CURRENT] !== undefined && PROJECT_PROCESS_CRITE_REL[PROJECT_PROCESS_CRITE_CURRENT].indexOf(self.name) === -1) {
						self.$el.hide();
					}
					else {
						self.$el.show();
						self.internal_set_value(0);
						self.$input.val(0);
						self.$span.html("0%");
					}
				}
					
			});
			$('input.v_manufact_number').on('keyup', function(){
				self.change();
			});
		});
	},
	change: function(){
		var value = parseInt(this.$input.val(), 10);
		if (isNaN(value))
			value = 0;
		this.$input.val(value);
		var percent = (value > 0) ? parseInt(parseFloat(value) / PROJECT_PROCESS_NUMBER_CURRENT * 100) : 0;
		if (isNaN(percent))
			this.$span.html("0%");
		else
			this.$span.html(percent + '%');
		this.internal_set_value(value);
	},
	render_value: function() {
		if (!this.field_manager.datarecord.process) {
			this.$el.hide();
		}
		else {
			if (PROJECT_PROCESS_CRITE_REL[this.field_manager.datarecord.process[0]] !== undefined && PROJECT_PROCESS_CRITE_REL[this.field_manager.datarecord.process[0]].indexOf(this.name) === -1) {
				this.$el.hide();
			}
			else {
				var value = parseInt(this.get('value'), 10);
				if (isNaN(value)) {
					value = 0;
				}
				var total = this.field_manager.datarecord.number;
				this.$input.val(value);
				var percent = (value > 0) ? parseInt(parseFloat(value) / total * 100) : 0;
				this.$span.html(percent + '%');
			}
		}
			
	},
});
core.form_widget_registry.add('project_manufact', ProjectManufactForm);

// Số lượng - Phần trăm : Theo dõi sản xuất - Dự án
var ProjectManufactTree = ColumnBoolean.extend({
	init: function () {
		this._super.apply(this, arguments);
		this.sortable = false;
	},
	_format: function (row_data, options) {
		var value = parseInt(row_data[this.id].value, 10);
		var processes = $.isArray(row_data.process.value) ? row_data.process.value[0] : row_data.process.value;
		if (!processes 
			|| !PROJECT_PROCESS_CRITE_REL[processes].length 
			|| row_data.is_total.value == true 
			|| (PROJECT_PROCESS_CRITE_REL[processes].indexOf(this.name) === -1)
			) {
			return;
		}
		return _.str.sprintf('<div class="o_textbox"><input type="text" value="%d"  disabled="disabled"/><span> %d %s</span></div>',
			value, (row_data.number.value > 0) ? parseFloat(value) / row_data.number.value * 100 : 0, '%');
	}
});
core.list_widget_registry.add('field.project_manufact', ProjectManufactTree);

// Tiêu đề tổng mục Theo dõi sản xuất - Dự án
var ProjectManufactTotalTitle = ColumnChar.extend({
	_format: function (row_data, options) {
		var value = row_data[this.id].value;
		if (row_data.is_total.value == true) {
			return _.str.sprintf('<b>%s</b>', value);
		}
		return this._super(row_data, options);
	}
});
core.list_widget_registry.add('field.project_manufact_title', ProjectManufactTotalTitle);

// Phân quyền bản vẽ
var ProjectRoleBooleanForm = form_common.AbstractField.extend({
	template: 'FieldProjectRoleBoolean',
	events: {
		'click input.v_project_role_1': function() {
			this.v_click_view();
		},
		'click input.v_project_role_2': function() {
			this.v_click_edit();
		},
		'click input.v_project_role_3': function() {
			this.v_click_delete();
		},
	},
	start: function() {
		this.$checkbox1 = this.$('input.v_project_role_1');
		this.$checkbox2 = this.$('input.v_project_role_2');
		this.$checkbox3 = this.$('input.v_project_role_3');


		this.$checkbox1.prop('disabled', this.get("effective_readonly"));
		this.on("change:effective_readonly", this, function() {
			this.$checkbox1.prop('disabled', this.get("effective_readonly"));
		});

		this.$checkbox2.prop('disabled', this.get("effective_readonly"));
		this.on("change:effective_readonly", this, function() {
			this.$checkbox2.prop('disabled', this.get("effective_readonly"));
		});

		this.$checkbox3.prop('disabled', this.get("effective_readonly"));
		this.on("change:effective_readonly", this, function() {
			this.$checkbox1.prop('disabled', this.get("effective_readonly"));
		});

		this.setupFocus(this.$checkbox1);

		return this._super();
	},

	v_click_view: function(){
		var view = this.$checkbox1.prop('checked');
		if (view) {
			this.internal_set_value(1);
		}
		else {
			this.internal_set_value(0);
			this.$checkbox2.prop('checked', false);
			this.$checkbox3.prop('checked', false);
		}
	},
	v_click_edit: function(){
		var view = this.$checkbox1.prop('checked');
		var edit = this.$checkbox2.prop('checked');
		if (edit) {
			this.internal_set_value(3);
			this.$checkbox1.prop('checked', true);
		}
		else {
			this.internal_set_value(1);
			this.$checkbox3.prop('checked', false);
		}
	},
	v_click_delete: function(){
		var view = this.$checkbox1.prop('checked');
		var edit = this.$checkbox2.prop('checked');
		var vdelete = this.$checkbox3.prop('checked');
		if(vdelete){
			this.internal_set_value(7);
			this.$checkbox1.prop('checked', true);
			this.$checkbox2.prop('checked', true);          
		}
		else {
			if(edit){
				this.internal_set_value(3);
			}
			else{
				this.internal_set_value(1);
			}
		}
	},
	render_value: function() {
		var value = parseInt(this.get('value'), 10);
		this.$checkbox1.prop('checked', (value | 1) === value);
		this.$checkbox2.prop('checked', (value | 2) === value);
		this.$checkbox3.prop('checked', (value | 4) === value);
	},
	// set_dimensions: function(height, width) {}, // Checkboxes have a fixed height and width (even in list editable)
	is_false: function() {
		return false;
	},
});
core.form_widget_registry.add('project_role_boolean', ProjectRoleBooleanForm);

var ProjectRoleBoolean = ColumnChar.extend({
	init: function () {
		this._super.apply(this, arguments);
		this.sortable = false;
	},
	_format: function (row_data, options) {
		var value = parseInt(row_data[this.id].value, 10);
		return _.str.sprintf('<div class="o_checkbox"> <span><input type="checkbox" %s disabled="disabled"/></span><span><input type="checkbox" %s disabled="disabled"/></span><span><input type="checkbox" %s disabled="disabled"/></span></div>',
			(value | 1) === value ? 'checked="checked"' : '', (value | 2) === value ? 'checked="checked"' : '', (value | 4) === value ? 'checked="checked"' : '');
	}
});
core.list_widget_registry.add('field.project_role_boolean', ProjectRoleBoolean);

// Chọn tiến trình sản xuất
var FieldSelectionProjectProcess = FieldSelection.extend({
	store_dom_value: function(){
		PROJECT_PROCESS_CRITE_CURRENT = JSON.parse(this.$el.val());
		this._super();
	},
	render_value: function() {
		PROJECT_PROCESS_CRITE_CURRENT = this.get("value");
		var values = this.get("values");
		values =  [[false, this.node.attrs.placeholder || '']].concat(values);
		var found = _.find(values, function(el) { return el[0] === this.get("value"); }, this);
		if (!found) {
			found = [this.get("value"), ''];
			values = [found].concat(values);
		}
		if (!this.get("effective_readonly")) {
			this.$el.empty();
			for(var i = 0 ; i < values.length ; i++) {
				this.$el.append($('<option/>', {
					value: JSON.stringify(values[i][0]),
					html: values[i][1]
				}))
			}
			this.$el.val(JSON.stringify(found[0]));
		} else {
			this.$el.text(found[1]);
		}
	},
});
core.form_widget_registry.add('project_process_selection', FieldSelectionProjectProcess);

// Cột tiến trình sản xuất
var FieldSelectionProjectProcessTree = ColumnChar.extend({
	_format: function (row_data, options) {
		var value = row_data[this.id].value;
		if (value)
			return value[1];
		return this._super(row_data, options);
	}
});
core.list_widget_registry.add('field.project_process_selection', FieldSelectionProjectProcessTree);

// Nhập số lượng sản xuất
var FieldIntegerProjectNumber = FieldFloat.extend({
	initialize_content: function() {
		var self = this;
		this._super();
		this.$el.addClass('v_manufact_number');
		if (this.$input) {
			this.$input.on('keyup', function(){
				if (self.is_syntax_valid()) {
					PROJECT_PROCESS_NUMBER_CURRENT = parseInt(self.$input.val());
				}
			});
		}
	},
	render_value: function() {
		PROJECT_PROCESS_NUMBER_CURRENT = this.get("value");
		this._super();
	},
});
core.form_widget_registry.add('project_number', FieldIntegerProjectNumber);

var ColumnNumberEmpty = ColumnChar.extend({
	_format: function (row_data, options) {
		var value = row_data[this.id].value;
		if (value != 0)
			return value;
		return '';
	}
});
core.list_widget_registry.add('field.project_number', ColumnNumberEmpty);
core.list_widget_registry.add('field.number_empty', ColumnNumberEmpty);


// Sự kiện bấm vào dòng "Nhân sự nội bộ" có được sửa không
var X2ManyListInternalUser = ListView.List.extend({
	row_clicked: function (event) {
		if (IS_ADMIN && PROJECT_EDITABLE)
			return this._super.apply(this, arguments);
		return;
	},
    render: function () {
        var self = this;
        this.$current.html(
            QWeb.render('InternalUserListView.rows', _.extend({}, this, {
                    render_cell: function () {
                        return self.render_cell.apply(self, arguments); }
                })));
        this.pad_table_to(1);
    },
    render_record: function (record) {
        var self = this;
        var index = this.records.indexOf(record);
        return QWeb.render('InternalUserListView.row', {
            columns: this.columns,
            options: this.options,
            record: record,
            row_parity: (index % 2 === 0) ? 'even' : 'odd',
            view: this.view,
            render_cell: function () {
                return self.render_cell.apply(self, arguments); }
        });
    },
	pad_table_to: function (count) {
		if (IS_ADMIN && PROJECT_EDITABLE) {
			this._super(0);

			var self = this;
			var columns = _(this.columns).filter(function (column) {
				return column.invisible !== '1';
			}).length;
			if (this.options.selectable) { columns++; }
			if (this.options.deletable) { columns++; }

			var $cell = $('<td>', {
				colspan: columns,
				'class': 'o_form_field_x2many_list_row_add'
			}).append(
				$('<a>', {href: '#'}).text(_t("Add an item"))
					.click(function (e) {
						e.preventDefault();
						e.stopPropagation();
						var def;
						if (self.view.editable()) {
							// FIXME: there should also be an API for that one
							if (self.view.editor.form.__blur_timeout) {
								clearTimeout(self.view.editor.form.__blur_timeout);
								self.view.editor.form.__blur_timeout = false;
							}
							def = self.view.save_edition();
						}
						$.when(def).done(self.view.do_add_record.bind(self));
					}));

			var $padding = this.$current.find('tr:not([data-id]):first');
			var $newrow = $('<tr>').append($cell);
			if ($padding.length) {
				$padding.before($newrow);
			} else {
				this.$current.append($newrow);
			}
		}
		return;
	},
});
// Thêm nút xoá vào phía sau bản ghi trên danh sách sửa trực tiếp
var One2ManyListViewDeletetable = One2ManyListView.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.options.deletable = IS_ADMIN && PROJECT_EDITABLE;
		this.options.ListType = X2ManyListInternalUser;
	},
});
var FieldOne2ManyDeletable = FieldOne2Many.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.x2many_views.list = One2ManyListViewDeletetable;
	},
});
core.form_widget_registry.add('one2many_deletable', FieldOne2ManyDeletable);

// TODO: Many2ManyListView is not original returned by it own module - web.form_relational. Returns it before uses
var Many2ManyListViewDeletable = form_relational.Many2ManyListView.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.options.deletable = IS_ADMIN;
	},
	do_delete: function (ids) {
        var self = this;
        return $.when(this.dataset.unlink(ids)).done(function () {
            _(ids).each(function (id) {
                self.records.remove(self.records.get(id));
            });
            // Hide the table if there is no more record in the dataset
            if (self.display_nocontent_helper()) {
                self.no_result();
            } else {
                if (self.records.length && self.current_min === 1) {
                    // Reload the list view if we delete all the records of the first page
                    self.reload();
                } else if (self.records.length && self.dataset.size() > 0) {
                    // Load previous page if the current one is empty
                    self.pager.previous();
                }
                // Reload the list view if we are not on the last page
                if (self.current_min + self._limit - 1 < self.dataset.size()) {
                    self.reload();
                }
            }
            self.update_pager(self.dataset);
            self.compute_aggregates();
        });
    },
});
var FieldMany2ManyDeletable = FieldMany2Many.extend({
	init: function(){
		this._super.apply(this, arguments);
		this.x2many_views.list = Many2ManyListViewDeletable;
	},
});
core.form_widget_registry.add('many2many_deletable', FieldMany2ManyDeletable);

var FieldMoneyNumber = FieldFloat.extend({
	events: {
        'keyup': 'key_up',
    },
    init: function(parent, dataset, view_id, options) {
        this._super.apply(this, arguments);
        this.required = (this.node.attrs.modifiers.indexOf('"required": true') != -1 
                        || this.node.attrs.modifiers.indexOf('"required": 1') != -1) ? true : false;
    },
    key_up: function(e) {
        var value = e.currentTarget.value.replace(/,/g, '');
        if (value.length)
            e.currentTarget.value = this.addCommas(value);
        this.store_dom_value();
    },
    addCommas: function(n){
    	n += '';
        if (n.length > 19)
            return '';
        n = n.replace(/\./g, '');
        var is_valid = true;
        var count_dot = 0;
        for(var i=0; i<n.length; i++) {
            if(!$.isNumeric(n[i]) && n[i] != ',') {
                is_valid = false;
                break;
            }
            if (n[i] == ',')
                count_dot++;
        }
        if (count_dot > 1)
            is_valid = false;

        if (! is_valid) {
            n = parseFloat(n) + '';
        }
        var x = n.split(',');
        var x1 = parseFloat(x[0]) + '';
        // var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        var res = x1;// + x2;
        if (res == "NaN" || res == '0')
            res = '';
        return res;
    },
    render_value: function() {
        var show_value = this.format_value(this.get('value'), '');
        if (this.$input) {
            this.$input.val(this.addCommas(show_value));
        } else {
            if (this.password) {
                show_value = new Array(show_value.length + 1).join('*');
            }
            this.$el.text(this.addCommas(show_value));
        }
    },
    store_dom_value: function () {
        if (this.$input && this.is_syntax_valid()) {
        	var v = this.$input.val().replace(/\./g, '');
            this.internal_set_value(this.parse_value(v));
        }
    },
    is_syntax_valid: function() {
        if (!this.get("effective_readonly") && this.required) {
            try {
                this.parse_value(this.$input.val().replace(/\./g, ''), '');
            } catch(e) {
                return false;
            }
        }
        return true;
    }
});
core.form_widget_registry.add('money_number', FieldMoneyNumber);

var ColumnMoneyNumber = ColumnChar.extend({
    addCommas: function(n){
    	n += '';
        if (n.length > 19)
            return '0';
        n = n.replace(/\./g, '');
        var is_valid = true;
        var count_dot = 0;
        for(var i=0; i<n.length; i++) {
            if(!$.isNumeric(n[i]) && n[i] != ',') {
                is_valid = false;
                break;
            }
            if (n[i] == ',')
                count_dot++;
        }
        if (count_dot > 1)
            is_valid = false;

        if (! is_valid) {
            n = parseFloat(n) + '';
        }
        var x = n.split(',');
        var x1 = parseFloat(x[0]) + '';
        // var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        var res = x1;// + x2;
        if (res == "NaN")
            res = '';
        return res;
    },
	_format: function (row_data, options) {
		var value = row_data[this.id].value;
		if (value)
			return this.addCommas(value);
		return '';
	}
});
core.list_widget_registry.add('field.money_number', ColumnMoneyNumber);

var FieldNumberOnly = FieldChar.extend({
	events: {
        'keyup': 'key_up',
    },
    key_up: function(e) {
        var value = this.$input.val();
        this.$input.val(value.replace(/[^\d]/, ''));
        this.store_dom_value();
    },
});
core.form_widget_registry.add('number_only', FieldNumberOnly);

var ColumnCharEllipsis = ColumnChar.extend({
	_format: function (row_data, options) {
		var value = row_data[this.id].value;
		var width = this.modifiers.width || '';
		if (value) {
			return '<span class="v_ellipsis" style="width:'+ width +'" title="' + value + '">' + value + '</span>';
		}
		return '';
	}
});
core.list_widget_registry.add('field.char_ellipsis', ColumnCharEllipsis);
core.form_widget_registry.add('char_ellipsis', FieldChar);

var FieldMany2OneCustomize = FieldMany2One.extend({
	init: function(field_manager, node) {
        this._super(field_manager, node);
        if (!CAN_QUICK_ADD_PARTNER)
        	this.options = this.modifiers.options;
	}
});
core.form_widget_registry.add('many2one_customize', FieldMany2OneCustomize);

var MyFieldBinaryFile = FieldBinaryFile.extend({
	template: 'MyFieldBinaryFile',
    on_file_change: function(e) {
        var self = this;
        var file_node = e.target;
        if ((this.useFileAPI && file_node.files.length) || (!this.useFileAPI && $(file_node).val() !== '')) {
            if (this.useFileAPI) {
                var file = file_node.files[0];
                if (file.size > this.max_upload_size) {
                    var msg = _t("The selected file exceed the maximum file size of %s.");
                    alert(_t(_.str.sprintf(msg, utils.human_size(this.max_upload_size))));
                    return false;
                }
                var filereader = new FileReader();
                filereader.readAsDataURL(file);
                filereader.onloadend = function(upload) {
                    var data = upload.target.result;
                    data = data.split(',')[1];
                    self.on_file_uploaded(file.size, file.name, file.type, data);
                };
            } else {
                this.$('form.o_form_binary_form input[name=session_id]').val(this.session.session_id);
                this.$('form.o_form_binary_form').submit();
            }
            this.$('.o_form_binary_progress').show();
            this.$('button').hide();
        }
    },
});
core.form_widget_registry.add('my_binary', MyFieldBinaryFile);

var ColumnMyBinary = ColumnBinary.extend({
    _format: function (row_data, options) {
        var text = _t("Download"), filename=_t('Binary file');
        var value = row_data[this.id].value;
        if (!value) {
            return options.value_if_empty || '';
        }

        var download_url;
        if (value.substr(0, 10).indexOf(' ') == -1) {
            download_url = "data:application/octet-stream;base64," + value;
        } else {
            download_url = session.url('/web/content', {model: options.model, field: this.id, id: options.id, download: true});
            if (this.filename) {
                download_url += '&filename_field=' + this.filename;
            }
        }
        if (this.filename && row_data[this.filename]) {
        	text = _.str.sprintf(_t("%s"), formats.format_value(row_data[this.filename].value, {type: 'char'}));
            filename = row_data[this.filename].value;
        }
        return _.template('<a class="v_attach_link" download="<%-download%>" href="<%-href%>" title="<%-text%>"><%-text%></a>')({
            text: text,
            href: download_url,
            download: filename,
        });
    }
});
core.list_widget_registry.add('field.my_binary', ColumnMyBinary);

});
