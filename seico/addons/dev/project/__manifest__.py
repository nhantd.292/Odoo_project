# -*- coding: utf-8 -*-
{
    'name':u'Dự án',
    'description': u'Quản lý dự án - Seico v2',
    'version':'1.0',
    'author':'CN',

    'data': [
        'security/rules.xml',
        'security/ir.model.access.csv',
        'data/data.xml',
        'views/views.xml',
        'views/admin.xml',
        'views/project.xml',
    ],
    'website': 'vtechcom.org',
    'depends': ['customize', 'staff_evaluate'],
    "qweb": [
        'static/src/xml/*.xml',
    ],
    'application': True,
}
