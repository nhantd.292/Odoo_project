# -*- coding: utf-8 -*-
from odoo import fields, models, api
from odoo.http import parser as my_parser
from odoo.exceptions import Warning
from lxml import etree
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import os
from openpyxl import load_workbook
from openpyxl.styles import Border, Side, Alignment, PatternFill
from openpyxl.utils import get_column_letter

DATE_NOW = datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)

ROLE_VIEW_JOB = 1
ROLE_EDIT_JOB = 2
ROLE_DEL_JOB = 4

ROLE_VIEW_MATERIAL = 8
ROLE_EDIT_MATERIAL = 16
ROLE_DEL_MATERIAL = 32

ROLE_VIEW_DRAWING = 64
ROLE_EDIT_DRAWING = 128
ROLE_DEL_DRAWING = 256

ROLE_VIEW_MANUFACT = 512
ROLE_EDIT_MANUFACT = 1024
ROLE_DEL_MANUFACT = 2048

ROLE_VIEW_SHIPMENT = 4096
ROLE_EDIT_SHIPMENT = 8192
ROLE_DEL_SHIPMENT = 16384

ROLE_VIEW_COSTS = 32768
ROLE_EDIT_COSTS = 65536
ROLE_DEL_COSTS = 131072

ROLE_VIEW_TIMELINE = 262144
ROLE_EDIT_TIMELINE = 524288
ROLE_DEL_TIMELINE = 1048576


def diff(a, b):
	b = set(b)
	return [i for i in a if i not in b]

def intersect(a, b):
	return list(set(a) & set(b))


class ProjectPayStatus(models.Model):
	_name = 'project.pay.status'
	_description = u'Trạng thái TT'

	name = fields.Char(u'Tên trạng thái')
	state = fields.Selection([('0', u'Ẩn'), ('1', u'Hiện')], string=u'Hiện/Ẩn', default='1')


class ProjectStatus(models.Model):
	_name = 'project.status'
	_description = u'Trạng thái CV'

	name = fields.Char(u'Tên trạng thái')
	state = fields.Selection([('0', u'Ẩn'), ('1', u'Hiện')], string=u'Hiện/Ẩn', default='1')
	
	@api.multi
	def write(self, vals):
		if self.id == my_parser.attr_read('project_status_review_id') \
			or self.id == my_parser.attr_read('project_status_end_id'):
			raise Warning("Không thể sửa bản ghi mặc định!")
		else:
			return super(ProjectStatus, self).write(vals)
	
	@api.multi
	def unlink(self):
		for r in self:
			if r.id == my_parser.attr_read('project_status_review_id') \
				or r.id == my_parser.attr_read('project_status_end_id'):
				raise Warning("Không thể xoá bản ghi mặc định!")
			else:
				r.unlink()

	@api.model
	def set_status_review(self):
		idx = self.env.ref('project.project_status_review').id
		my_parser.attr_set('project_status_review_id', idx)
	
	@api.model
	def set_status_end(self):
		idx = self.env.ref('project.project_status_end').id
		my_parser.attr_set('project_status_end_id', idx)


class ProjectInvestorContact(models.Model):
	_name = 'project.investor.contact'
	_description = u'Liên hệ chủ đầu tư'

	name 		= fields.Char(u'Tên')
	position 	= fields.Char(u'Chức vụ')
	email 		= fields.Char(u'Email')
	phone 		= fields.Char(u'Điện thoại')
	investor_id = fields.Many2one('project.investor', u'Chủ đầu tư', ondelete='cascade')
	
	@api.model
	def create(self, vals):
		investor_id = self._context.get('investor_id', False)
		if investor_id:
			vals['investor_id'] = investor_id
		return super(ProjectInvestorContact, self).create(vals)
	
	@api.model
	def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
		investor_id = self._context.get('investor_id', None)
		if investor_id:
			domain.append(['investor_id', '=', investor_id])
		return super(ProjectInvestorContact, self).search_read(domain, fields, offset, limit, order)


class ProjectInvestorAddress(models.Model):
	_name = 'project.investor.address'
	_description = u'Địa chỉ chủ đầu tư'

	name = fields.Char(u'Địa chỉ')
	investor_id = fields.Many2one('project.investor', u'Chủ đầu tư', ondelete='cascade')


class ProjectInvestor(models.Model):
	_name = 'project.investor'
	_description = u'Chủ đầu tư'
	_inherit = ['mail.thread']

	name = fields.Char(u'Tên chủ đầu tư', track_visibility='onchange')
	full_name = fields.Char(u'Tên đầy đủ', track_visibility='onchange')
	contact_ids = fields.One2many('project.investor.contact', 'investor_id', string=u'Các liên hệ')
	address_ids = fields.One2many('project.investor.address', 'investor_id', string=u'Các địa chỉ')


class ProjectSupervisorContact(models.Model):
	_name = 'project.supervisor.contact'
	_description = u'Liên hệ tư vấn giám sát'

	name 		= fields.Char(u'Tên')
	position 	= fields.Char(u'Chức vụ')
	email 		= fields.Char(u'Email')
	phone 		= fields.Char(u'Điện thoại')
	supervisor_id = fields.Many2one('project.supervisor', u'Tư vấn giám sát', ondelete='cascade')
	
	@api.model
	def create(self, vals):
		supervisor_id = self._context.get('supervisor_id', False)
		if supervisor_id:
			vals['supervisor_id'] = supervisor_id
		return super(ProjectSupervisorContact, self).create(vals)
	
	@api.model
	def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
		supervisor_id = self._context.get('supervisor_id', None)
		if supervisor_id:
			domain.append(['supervisor_id', '=', supervisor_id])
		return super(ProjectSupervisorContact, self).search_read(domain, fields, offset, limit, order)


class ProjectSupervisorAdress(models.Model):
	_name = 'project.supervisor.address'
	_description = u'Địa chỉ tư vấn giám sát'

	name = fields.Char(u'Tên')
	supervisor_id = fields.Many2one('project.supervisor', u'Tư vấn giám sát', ondelete='cascade')


class ProjectSupervisor(models.Model):
	_name = 'project.supervisor'
	_description = u'Tư vấn giám sát'
	_inherit = ['mail.thread']

	name 		= fields.Char(u'Tên đơn vị giám sát', track_visibility='onchange')
	full_name 	= fields.Char(u'Tên đầy đủ', track_visibility='onchange')
	contact_ids = fields.One2many('project.supervisor.contact', 'supervisor_id', string=u'Các liên hệ')
	address_ids = fields.One2many('project.supervisor.address', 'supervisor_id', string=u'Các địa chỉ')


class ProjectContractorContact(models.Model):
	_name = 'project.contractor.contact'
	_description = u'Liên hệ thầu chính'

	name 		= fields.Char(u'Tên')
	position 	= fields.Char(u'Chức vụ')
	email 		= fields.Char(u'Email')
	phone 		= fields.Char(u'Điện thoại')
	contractor_id = fields.Many2one('project.contractor', u'Thầu chính', ondelete='cascade')
	
	@api.model
	def create(self, vals):
		contractor_id = self._context.get('contractor_id', False)
		if contractor_id:
			vals['contractor_id'] = contractor_id
		return super(ProjectContractorContact, self).create(vals)
	
	@api.model
	def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
		contractor_id = self._context.get('contractor_id', None)
		if contractor_id:
			domain.append(['contractor_id', '=', contractor_id])
		return super(ProjectContractorContact, self).search_read(domain, fields, offset, limit, order)


class ProjectContractorAddress(models.Model):
	_name = 'project.contractor.address'
	_description = u'Địa chỉ thầu chính'

	name = fields.Char(u'Tên')
	contractor_id = fields.Many2one('project.contractor', u'Thầu chính', ondelete='cascade')


class ProjectContractor(models.Model):
	_name = 'project.contractor'
	_description = u'Thầu chính'
	_inherit = ['mail.thread']

	name 		= fields.Char(u'Tên thầu chính', track_visibility='onchange')
	full_name 	= fields.Char(u'Tên đầy đủ', track_visibility='onchange')
	contact_ids = fields.One2many('project.contractor.contact', 'contractor_id', string=u'Các liên hệ')
	address_ids = fields.One2many('project.contractor.address', 'contractor_id', string=u'Các địa chỉ')


class ProjectProjectRole(models.Model):
	_name = 'project.project.role'
	_description = u'Nhân sự nội bộ'

	project_id 		= fields.Many2one('project.project', u'Dự án', ondelete='cascade')
	user_id 		= fields.Many2one('res.users', u'Tên')
	position_id 	= fields.Many2one(related='user_id.position', string=u'Chức vụ', store=True)
	quota_id 		= fields.Char(related='user_id.quota_id.code', string=u'Ngạch', store=True)
	user_code		= fields.Char(related='user_id.code', string=u'Mã NV', store=True)

	role_job_assign = fields.Integer(u"Phiếu giao việc<span class='v_raw_head'><span>Xem</span><span>Sửa</span><span>Xóa</span></span>", groups="staff_evaluate.group_seico_admin")
	role_material 	= fields.Integer(u"Quy cách vật liệu<span class='v_raw_head'><span>Xem</span><span>Sửa</span><span>Xóa</span></span>", groups="staff_evaluate.group_seico_admin")
	role_drawing 	= fields.Integer(u"Bản vẽ<span class='v_raw_head'><span>Xem</span><span>Sửa</span><span>Xóa</span></span>", groups="staff_evaluate.group_seico_admin")
	role_timeline	= fields.Integer(u"Timeline sản xuất<span class='v_raw_head'><span>Xem</span><span>Sửa</span><span>Xóa</span></span>", groups="staff_evaluate.group_seico_admin")
	role_manufact 	= fields.Integer(u"Theo dõi sản xuất<span class='v_raw_head'><span>Xem</span><span>Sửa</span><span>Xóa</span></span>", groups="staff_evaluate.group_seico_admin")
	role_shipment 	= fields.Integer(u"Xuất hàng<span class='v_raw_head'><span>Xem</span><span>Sửa</span><span>Xóa</span></span>", groups="staff_evaluate.group_seico_admin")
	role_costs 		= fields.Integer(u"Chi phí công trường<span class='v_raw_head'><span>Xem</span><span>Sửa</span><span>Xóa</span></span>", groups="staff_evaluate.group_seico_admin")

	inline_deltable = fields.Boolean(readonly=True, default=True)


class ProjectProjectJob(models.Model):
	_name = 'project.project.job'
	_inherit = ['mail.thread']
	_description = u'Phiếu giao việc'

	order 		= fields.Integer(u'STT', store=False, readonly=False)
	create_uid 	= fields.Many2one('res.users', string=u"Người gửi", readonly=True, default=lambda self: self._uid)
	create_u_name = fields.Char(related='create_uid.name', store=True, readonly=True)
	typex 		= fields.Char(u'Loại phiếu', track_visibility='onchange')
	attach 		= fields.Binary(u'Đính kèm')
	attach_name = fields.Char(u'Tên file', track_visibility='onchange')
	project_id 	= fields.Many2one('project.project', u'Dự án', ondelete='cascade')

	@api.model
	def create(self, vals):
		project_id = self._context.get('project_id', False)
		if project_id:
			vals['project_id'] = project_id
		return super(ProjectProjectJob, self).create(vals)


class ProjectProjectMaterial(models.Model):
	_name = 'project.project.material'
	_description = u'Quy cách vật liệu'
	_inherit = ['mail.thread']

	order 		= fields.Integer(u'STT', store=False, readonly=False)
	material 	= fields.Char(u'Vật liệu', track_visibility='onchange')
	style 		= fields.Char(u'Quy cách', track_visibility='onchange')
	category 	= fields.Char(u'Hạng mục/khu vực', track_visibility='onchange')
	original 	= fields.Char(u'Xuất xứ/nhà cung cấp', track_visibility='onchange')
	color 		= fields.Char(u'Màu', track_visibility='onchange')
	state 		= fields.Char(u'Trạng thái', track_visibility='onchange')
	approved_date = fields.Date(u'Ngày duyệt', track_visibility='onchange')
	project_id 	= fields.Many2one('project.project', u'Dự án', ondelete='cascade')

	@api.model
	def create(self, vals):
		project_id = self._context.get('project_id', False)
		if project_id:
			vals['project_id'] = project_id
		return super(ProjectProjectMaterial, self).create(vals)


class ProjectProjectDrawing(models.Model):
	_name = 'project.project.drawing'
	_inherit = ['mail.thread']
	_description = u'Bản vẽ'

	order 		= fields.Integer(u'STT', store=False, readonly=False)
	name 		= fields.Char(u'Tên file bản vẽ', track_visibility='onchange')
	typex 		= fields.Char(u'Loại bản vẽ', track_visibility='onchange')
	category 	= fields.Char(u'Hạng mục/khu vực', track_visibility='onchange')
	version 	= fields.Char(u'Phiên bản', track_visibility='onchange')
	version_date= fields.Date(u'Ngày phiên bản', track_visibility='onchange')
	state 		= fields.Selection([('1', u'Còn hiệu lực'), ('0', u'Hết hiệu lực')], u'Trạng thái', track_visibility='onchange')
	attach 		= fields.Binary(u'File đính kèm')
	attach_name = fields.Char(u'Tên file', track_visibility='onchange')
	note 		= fields.Char(u'Ghi chú', track_visibility='onchange')
	project_id 	= fields.Many2one('project.project', u'Dự án', ondelete='cascade')

	@api.model
	def create(self, vals):
		project_id = self._context.get('project_id', False)
		if project_id:
			vals['project_id'] = project_id
		return super(ProjectProjectDrawing, self).create(vals)


class ProjectProcessCriteria(models.Model):
	_name = 'project.process.criteria'
	_description = u'Tiêu chí tiến trình'
	_order = 'sequence asc'
	
	_sql_constraints = [('name_unique', 'unique(name)', u'Tên tiêu chí không được đặt trùng nhau!')]
	
	name = fields.Char(u'Tên tiêu chí', required=True)
	fname = fields.Char(readonly=True)
	sequence = fields.Integer(u'Sắp xếp', default=1)
	state = fields.Selection([('0', u'Ẩn'), ('1', u'Hiện')], string=u'Hiện/Ẩn', default='1')
	
	def update_exceltemplate_head(self):
		criterias = self.search_read([('state', '=', '1')], ['name', 'fname'], order='sequence asc')
		if len(criterias):
			# Tải mẫu gốc lên
			tmp_path = os.path.abspath(__file__ + "/../../excel_templates/")
			wb = load_workbook(os.path.join(tmp_path, 'project_manufact0.xlsx'))
			# Thêm cột
			sheet = wb.worksheets[0]
			sheet.insert_cols(9, 2*len(criterias))
			# Đổ dữ liệu
			ws = wb.active
			# Đổ màu dòng head
			grayFill = PatternFill("solid", fgColor="BFBFBF")
			for i in range(9, 2*len(criterias) + 10):
				ws['%s1' % (get_column_letter(i))].fill = grayFill
				
			# Dòng, cột head đầu tiên
			row0, col0 = 2, 9
			# Định nghĩa styles
			thin_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
			def set_border(ws, cell_range):
				rows = ws[cell_range]
				side = Side(border_style='thin', color="FF000000")
			
				rows = list(rows)  # we convert iterator to list for simplicity, but it's not memory efficient solution
				max_y = len(rows) - 1  # index of the last row
				for pos_y, cells in enumerate(rows):
					max_x = len(cells) - 1  # index of the last cell
					for pos_x, cell in enumerate(cells):
						border = Border(
							left=cell.border.left,
							right=cell.border.right,
							top=cell.border.top,
							bottom=cell.border.bottom
						)
						if pos_x == 0:
							border.left = side
						if pos_x == max_x:
							border.right = side
						if pos_y == 0:
							border.top = side
						if pos_y == max_y:
							border.bottom = side
			
						# set new border only if it's one of the edge cells
						if pos_x == 0 or pos_x == max_x or pos_y == 0 or pos_y == max_y:
							cell.border = border
			for crite in criterias:
				# Ghép ô tên tiêu chí
				ws.merge_cells(start_row=row0, start_column=col0, end_row=row0, end_column=col0 + 1)
				# Tên tiêu chí
				ws.cell(row=row0, column=col0).value, ws.cell(row=row0, column=col0).alignment = crite['name'], Alignment(horizontal='center')
				set_border(ws, "%s%s:%s%s" % (get_column_letter(col0), row0, get_column_letter(col0 + 1), row0))
				# "Số lượng"
				ws.cell(row=row0 + 1, column=col0).value, ws.cell(row=row0 + 1, column=col0).border = u'S.Lượng', thin_border
				# "Phần trăm
				ws.cell(row=row0 + 1, column=col0 + 1).value, ws.cell(row=row0 + 1, column=col0 + 1).border = u'%', thin_border
				# Chuyển sang 2 cột bên phải => tiêu chí sau
				col0 += 2
			# "Ghi chú"
			ws.cell(row=row0, column=col0).value = u'Ghi chú'
			set_border(ws, "%s%s:%s%s" % (get_column_letter(col0), row0, get_column_letter(col0), row0 + 1))
			# Dòng chú ý khi nhập liệu
			ws['A4'].value = u"Dữ liệu nhập vào phải đúng theo cột để tránh lỗi trong quá trình đọc vào hệ thống. Nhập từ dòng dưới, không bỏ trống dòng!"
			# Lưu thành mẫu nhập, xuất dữ liệu
			wb.save(os.path.join(tmp_path, 'project_manufact1.xlsx'))
	
	@api.model
	def create(self, vals):
		count = self.search_count([])
		vals['sequence'] = count + 1
		vals['fname'] = 'x_crite_%s' % (count + 1)
		res = super(ProjectProcessCriteria, self).create(vals)
		
		manufact_model = self.env['ir.model'].search([('model', '=', 'project.project.manufact')], limit=1)
		new_field = self.env['ir.model.fields'].create({
			'name': res.fname,
			'field_description': res.name,
			'ttype': 'integer',
			'model': 'project.project.manufact',
			'model_id': manufact_model.id,
			'store': True,
			'copy': True
		})
		if new_field:
			if res.state == '1':
				project_detail_view = self.env.ref('project.view_project_project_detail')
				doc = etree.XML(project_detail_view.arch)
				for note in doc.xpath("//field[@name='manufact_ids']/tree/field[@name='note']"):
					note.getparent().insert(note.getparent().index(note), etree.Element('field', name=res.fname, string=res.name + u"<span class='v_raw_head v_raw_head_manufact'><span>SL</span><span>%</span></span>", widget="project_manufact", attrs="{'raw_heading':1}", tail='\n'))
				arch = etree.tostring(doc)
				query = 'update "ir_ui_view" set "arch_db"=%s where id=%s'
				self.env.cr.execute(query, (arch, project_detail_view.id))
				# Cập nhật mẫu nhập-xuất excel (phần đầu bảng)
				self.update_exceltemplate_head()
		else:
			raise Warning("Lỗi trong quá trình ghi dữ liệu!")
		return res
	
	@api.model
	def update_view(self):
		criterias = self.search_read([('state', '=', '1')], ['name', 'fname'], order='sequence asc')
		project_detail_view = self.env.ref('project.view_project_project_detail')
		doc = etree.XML(project_detail_view.arch)
		for x_ in doc.xpath("//field[@widget='project_manufact']"):
			x_.getparent().remove(x_)
		if len(criterias):
			for note in doc.xpath("//field[@name='manufact_ids']/tree/field[@name='note']"):
				for item in criterias:
					element = etree.Element('field', name=item['fname'], string=item['name'] + u"<span class='v_raw_head v_raw_head_manufact'><span>SL</span><span>%</span></span>", widget="project_manufact", attrs="{'raw_heading':1}")
					element.tail = note.tail
					note.getparent().insert(note.getparent().index(note), element)
		arch = etree.tostring(doc)
		query = 'update "ir_ui_view" set "arch_db"=%s where id=%s'
		self.env.cr.execute(query, (arch, project_detail_view.id))
		# Cập nhật mẫu nhập-xuất excel (phần đầu bảng)
		self.update_exceltemplate_head()

	@api.multi
	def write(self, vals):
		res = super(ProjectProcessCriteria, self).write(vals)
		
		self.update_view()
		return res


class ProjectManufactProcess(models.Model):
	_name = 'project.manufact.process'
	_description = u'Tiến trình sản xuất'
	_order = 'sequence asc'
	
	@api.model
	def get_total_id(self):
		return my_parser.attr_read("project_process_total_id")
	
	@api.model
	def set_total_id(self):
		total_id = self.env.ref('project.process_total').id
		my_parser.attr_set('project_process_total_id', total_id)
		
	@api.model
	def _set_process_crite_rel(self):
		records = self.search([('state', '=', '1')])
		res = {}
		for record in records:
			crits = []
			for crit in record.criteria_ids:
				crits.append(crit.fname)
			res[record.id] = crits
		my_parser.attr_set('project_process_crite_rel', res)
		
	@api.model
	def get_process_crite_rel(self):
		return my_parser.attr_read("project_process_crite_rel")

	sequence = fields.Integer(u'Sắp xếp', default=1)
	name = fields.Char(u'Tên tiến trình', required=True)
	state = fields.Selection([('0', u'Ẩn'), ('1', u'Hiện')], string=u'Hiển thị', default='1')
	criteria_ids = fields.Many2many('project.process.criteria', 'project_process_criteria_rel', 'process_id', 'criteria_id', u'Các tiêu chí')

	@api.model
	def name_search(self, name='', args=None, operator='ilike', limit=100):
		if self._context.get('form_inline'):
			args.append(('id', '!=', my_parser.attr_read("project_process_total_id")))
			args.append(('state', '=', '1'))
		return super(ProjectManufactProcess, self).name_search(name, args, operator, limit)
	
	@api.model
	def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
		if self._context.get('form_inline'):
			domain.append(('id', '!=', my_parser.attr_read("project_process_total_id")))
			domain.append(('state', '=', '1'))
		return super(ProjectManufactProcess, self).search_read(domain, fields, offset, limit, order)
	
	@api.model
	def create(self, vals):
		res = super(ProjectManufactProcess, self).create(vals)
		if res.state == '1':
			self._set_process_crite_rel()
		return res

	@api.multi
	def write(self, vals):
		if self.id != my_parser.attr_read("project_process_total_id"):
			res = super(ProjectManufactProcess, self).write(vals)
			self._set_process_crite_rel()
			return res
		else:
			raise Warning("Bản ghi tổng mục không thể sửa!")
		
	@api.multi
	def unlink(self):
		if self.id != my_parser.attr_read("project_process_total_id"):
			return super(ProjectManufactProcess, self).unlink()
		else:
			raise Warning("Bản ghi tổng mục không thể xoá!")


class ProjectProjectTimeline(models.Model):
	_name = 'project.project.timeline'
	_description = u'Timeline sản xuất'

	order 		= fields.Integer(u'STT', store=False, readonly=False)
	create_uid	= fields.Many2one('res.users', string=u'Người gửi', default=lambda self: self._uid)
	process 	= fields.Many2one('project.manufact.process', u'Tiến trình')
	descx 		= fields.Char(u'Mô tả')
	factory 	= fields.Many2one('project.factory', u'Nhà máy')
	attach 		= fields.Binary(u'File đính kèm')
	attach_name = fields.Char(u'Tên file')
	project_id 	= fields.Many2one('project.project', u'Dự án', ondelete='cascade')

	@api.model
	def create(self, vals):
		project_id = self._context.get('project_id', False)
		if project_id:
			vals['project_id'] = project_id
		return super(ProjectProjectTimeline, self).create(vals)


class ProjectProjectManufact(models.Model):
	_name = 'project.project.manufact'
	_description = u'Theo dõi sản xuất'
	_order = 'seq asc'
	
	@api.model
	def get_prev_sibling_by_seq(self, project_id, seq):
		record = self.search([('project_id', '=', project_id), ('seq', '<', seq)], limit=1, order='seq desc')
		if len(record):
			return {'seq': record.seq, 'next_seq': record.next_seq}
		return None

	seq 		= fields.Char()
	next_seq 	= fields.Char()
	order 		= fields.Integer(u'STT', store=False, readonly=True)
	name 		= fields.Char(u'Tên')
	unit 		= fields.Char(u'Đơn vị')
	number 		= fields.Integer(u'Số lượng', default=0)
	longx 		= fields.Char(u'Độ dài')
	category 	= fields.Char(u'Khu vực/H.Mục')
	factory 	= fields.Many2one('project.factory', u'Nhà máy')
	process 	= fields.Many2one('project.manufact.process', u'Tiến trình')
	note 		= fields.Char(u'Ghi chú')
	is_total	= fields.Boolean()
	project_id 	= fields.Many2one('project.project', u'Dự án', ondelete='cascade')
	
	@api.model
	def clean_old(self, pid):
		to_deletes = self.search([('project_id', '=', pid)])
		return to_deletes.unlink()


class ProjectShipmentWeight(models.Model):
	_name = 'project.shipment.weight'
	_description = u'Phiếu cân'

	order = fields.Integer(u'STT', store=False, readonly=True)
	number 		= fields.Char(u'Số phiếu')
	descx		= fields.Char(u'Mô tả hàng')
	datex 		= fields.Date(u'Ngày cân')
	weight		= fields.Float(u'Số cân(KG)')
	attach 		= fields.Binary(u'File đính kèm')
	attach_name = fields.Char(u'Tên file')
	write_uid	= fields.Many2one('res.users', default=lambda self:self._uid)
	shipment_id = fields.Many2one('project.project.shipment', u'Phiếu xuất hàng', ondelete='cascade')


class ProjectProjectShipment(models.Model):
	_name = 'project.project.shipment'
	_inherit = ['mail.thread']
	_description = u'Xuất hàng'
	_rec_name = 'number'

	order = fields.Integer(u'STT', store=False, readonly=False)
	number 		= fields.Char(u'Số phiếu', track_visibility='onchange')
	datex 		= fields.Date(u'Ngày xuất', track_visibility='onchange')
	transport 	= fields.Char(u'Đơn vị vận chuyển', track_visibility='onchange')
	car_plate 	= fields.Char(u'Biển số xe', track_visibility='onchange')
	driver 		= fields.Char(u'Lái xe', track_visibility='onchange')
	driver_phone = fields.Char(u'Số điện thoại', track_visibility='onchange')
	fromx 		= fields.Many2one('project.warehouse', u'Xuất từ kho', track_visibility='onchange')
	to 			= fields.Char(u'Nơi đến/dự án', track_visibility='onchange')
	descx		= fields.Char(u'Mô tả hàng', track_visibility='onchange')
	location 	= fields.Char(u'Khu vực', track_visibility='onchange')
	attach 		= fields.Binary(u'File đính kèm')
	attach_name = fields.Char(u'Tên file', track_visibility='onchange')
	weight_id 	= fields.One2many('project.shipment.weight', 'shipment_id', u'Phiếu cân')
	project_id 	= fields.Many2one('project.project', u'Dự án', ondelete='cascade')
	
	@api.model
	def create(self, vals):
		project_id = self._context.get('project_id', False)
		if project_id:
			vals['project_id'] = project_id
		return super(ProjectProjectShipment, self).create(vals)


class ProjectProjectCosts(models.Model):
	_name = 'project.project.costs'
	_inherit = ['mail.thread']
	_description = u'Chi phí công trường'

	order 		= fields.Integer(u'STT', store=False, readonly=False)
	descx 		= fields.Char(u'Diễn giải', track_visibility='onchange')
	datex 		= fields.Date(u'Ngày', track_visibility='onchange')
	month		= fields.Integer(readonly=True)
	year		= fields.Integer(readonly=True)
	income 		= fields.Float(u'Tiền ứng', track_visibility='onchange')
	outcome 	= fields.Float(u'Tiền chi', track_visibility='onchange')
	bill_num 	= fields.Char(u'Số hoá đơn', track_visibility='onchange')
	bill_note 	= fields.Char(u'Ghi chú hoá đơn', track_visibility='onchange')
	create_uid 	= fields.Many2one('res.users', string=u"Người cập nhật", readonly=True, default=lambda self: self._uid)
	approved_uid= fields.Char(u'Người duyệt', track_visibility='onchange')
	project_id 	= fields.Many2one('project.project', u'Dự án', ondelete='cascade')
	
	@api.model
	@api.returns('self', lambda value:value.id)
	def create(self, vals):
		if 'datex' in vals:
			try:
				datex = datetime.strptime(vals['datex'], DEFAULT_SERVER_DATE_FORMAT)
				if datex and datex.month and datex.year:
					vals['month'] = datex.month
					vals['year'] = datex.year
			except:
				pass
		return super(ProjectProjectCosts, self).create(vals)
	
	@api.multi
	def write(self, vals):
		if 'datex' in vals:
			try:
				datex = datetime.strptime(vals['datex'], DEFAULT_SERVER_DATE_FORMAT)
				if datex and datex.month and datex.year:
					vals['month'] = datex.month
					vals['year'] = datex.year
			except:
				pass
		return super(ProjectProjectCosts, self).write(vals)


# Chi tiết thanh toán
class ProjectProjectPayDetail(models.Model):
	_name = 'project.project.paydetail'
	_description = u'Chi tiết thanh toán'
	_rec_name = 'descx'

	order 		= fields.Integer(u'STT', store=False, readonly=False)
	descx		= fields.Char(u'Diễn giải')
	contract	= fields.Char(u'Số HĐ/Phát sinh')
	chairman_ids= fields.Many2many('res.users','project_pay_chairman_user_rel', 'pay_chairman_id','user_id', u'Chủ nhiệm')
	chairman_str= fields.Char(readonly=True, store=True, string=u'Chủ nhiệm')
	follower_ids= fields.Many2many('res.users','project_pay_follower_user_rel', 'pay_follower_id','user_id', u'Người theo dõi')
	follower_str= fields.Char(readonly=True, store=True, string=u'Người theo dõi')
	bill_date	= fields.Date(u'Ngày HĐ/N.Vụ')
	value		= fields.Float(u'Giá trị')
	paid 		= fields.Float(u'Đã thanh toán')
	bill 		= fields.Char(u'Số hóa đơn')
	note 		= fields.Char(u'Ghi chú')
	project_id = fields.Many2one('project.project', u'Dự án', ondelete='cascade')
	
	@api.model
	@api.returns('self', lambda value:value.id)
	def create(self, vals):
		res = super(ProjectProjectPayDetail, self).create(vals)
		valsx = {}
		if len(res.chairman_ids):
			valsx['chairman_str'] = ''
			for r in res.chairman_ids:
				valsx['chairman_str'] += r.name
		if len(res.follower_ids):
			valsx['follower_str'] = ''
			for r in res.follower_ids:
				valsx['follower_str'] += r.name
		if len(valsx):
			res.with_context(exitx=True).write(valsx)
		return res
	
	@api.multi
	def write(self, vals):
		exitx = self._context.get('exitx', False)
		res = super(ProjectProjectPayDetail, self).write(vals)
		if exitx:
			return res
		valsx = {}
		if len(self.chairman_ids):
			valsx['chairman_str'] = ''
			for r in self.chairman_ids:
				valsx['chairman_str'] += r.name + ', '
		if len(self.follower_ids):
			valsx['follower_str'] = ''
			for r in self.follower_ids:
				valsx['follower_str'] += r.name + ', '
		if len(valsx):
			self.with_context(exitx=True).write(valsx)
	
	@api.model
	def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
		res = super(ProjectProjectPayDetail, self).search_read(domain, fields, offset, limit, order)
		i = 1
		for r in res:
			r['order'] = i
			i += 1
		return res


class ProjectProjectPayExpected(models.Model):
	_name = 'project.project.payexpected'
	_description = u'Dự kiến thanh toán'
	
	def email_notify_outdate(self):
		if self.expected_date != False and DATE_NOW > self.expected_date:
			outdate_days = (datetime.strptime(DATE_NOW, DEFAULT_SERVER_DATE_FORMAT) - datetime.strptime(self.expected_date, DEFAULT_SERVER_DATE_FORMAT)).days
			mail_obj = self.env['mail.mail']
			mail_body = '''
			Thông báo:<br/>
			Mã dự án: <b>%s</b><br/>
			Tên dự án: <b>%s</b><br/>
			Thầu chính: <b>%s</b><br/>
			Chủ đầu tư: <b>%s</b><br/>
			Đã quá hạn thanh toán <b>%s</b> ngày theo kế hoạch đã đăng ký<br/>
			Đề nghị người theo dõi có biện pháp xử lý<br/><br/>
			<i style="font-size:smaller">Email được gửi từ hệ thống Seico ERP, vui lòng không trả lời thư này!</i><br/>
			'''
			mail_id = mail_obj.create({
	            'subject': '[No-Reply]Thông báo quá hạn dự kiến thanh toán - Seico ERP',
	            'state' : 'outgoing',
	            'auto_delete' : True,
	            #'email_from': 'Seico ERP',
	            'email_to' : self.follower.login,
	            'body_html' : mail_body % (self.project_id.code or '',
										self.project_id.name or '',
										self.project_id.contractor_fname or '',
										self.project_id.investor_fname or '',
										outdate_days),
	        })
			mail_obj.send(mail_id)
			mail_obj.process_email_queue()
		return {
	        "type": "ir.actions.do_nothing",
	    }
	
	@api.multi
	def compute_outdate(self):
		for r in self:
			if r.expected_date != False and DATE_NOW > r.expected_date and not r.pay_date:
				r.outdate = True
	
	@api.multi
	def compute_outdate_notify_sendable(self):
		perm = self._uid == 1 or self.env.user.quota_id.can_send_outdate_email
		for r in self:
			r.outdate_sendable = perm
	
	@api.multi
	def compute_completable_expect(self):
		perm = self._uid == 1 or self.env.user.quota_id.can_edit_pay_paid
		for r in self:
			r.completable_expect = perm

	order 			= fields.Integer(u'STT', store=False, readonly=False)
	descx			= fields.Char(u'Diễn giải')
	follower 		= fields.Many2one('res.users', u'Người theo dõi', default=lambda self: self.env.user, readonly=True)
	expected_date	= fields.Date(u'Ngày dự kiến')
	expecten_pay 	= fields.Float(u'Dự kiến TT')
	pay_date		= fields.Date(u'Ngày thực tế')
	pay_value		= fields.Float(u'Thực tế TT')
	bill 			= fields.Char(u'Số hóa đơn')
	pay_status 		= fields.Selection([('1', u'Đã xong'), ('2', u'Đang chờ')], u'Trạng thái', default='2')
	note1			= fields.Char(u'Ghi chú')
	note			= fields.Char(u'Quá hạn', readonly=True, store=True)
	outdate 		= fields.Boolean(store=False, compute=compute_outdate, default=False)
	project_id 		= fields.Many2one('project.project', u'Dự án', ondelete='cascade')
	# Có được gửi email thông báo quá hạn không
	outdate_sendable= fields.Boolean(store=False, readonly=True, compute=compute_outdate_notify_sendable)
	# Có được nhập các thông tin hoàn thành dự kiến thanh toán
	completable_expect = fields.Boolean(store=False, readonly=True, default=lambda self: self._uid == 1 or self.env.user.quota_id.can_edit_pay_paid, compute=compute_completable_expect)
	
	@api.model
	def ir_cron_paynext_outdate(self):
		records = self.search([('pay_date', '=', False), ('expected_date', '!=', False)])
		for r in records:
			if DATE_NOW > r.expected_date:
				note = u'Quá hạn %d ngày' % ((datetime.strptime(DATE_NOW, DEFAULT_SERVER_DATE_FORMAT) - datetime.strptime(r.expected_date, DEFAULT_SERVER_DATE_FORMAT)).days)
				r.update({'note': note})

	@api.model
	def create(self, vals):
		res = super(ProjectProjectPayExpected, self).create(vals)
		diffs = 0
		if res.pay_date and res.expected_date and res.pay_date >= res.expected_date:
			diffs = (datetime.strptime(res.pay_date, DEFAULT_SERVER_DATE_FORMAT) - datetime.strptime(res.expected_date, DEFAULT_SERVER_DATE_FORMAT)).days
		elif not res.pay_date and res.expected_date and DATE_NOW >= res.expected_date:
			diffs = (datetime.strptime(DATE_NOW, DEFAULT_SERVER_DATE_FORMAT) - datetime.strptime(res.expected_date, DEFAULT_SERVER_DATE_FORMAT)).days
		if diffs > 0:
			note = u'Quá hạn %d ngày' % (diffs)
			res.with_context(exitx=True).write({'note': note})
		return res
	
	@api.multi
	def write(self, vals):
		exitx = self._context.get('exitx', False)
		res = super(ProjectProjectPayExpected, self).write(vals)
		if exitx:
			return res
		
		diffs = -1
		if self.pay_date and self.expected_date and self.pay_date >= self.expected_date:
			diffs = (datetime.strptime(self.pay_date, DEFAULT_SERVER_DATE_FORMAT) - datetime.strptime(self.expected_date, DEFAULT_SERVER_DATE_FORMAT)).days
		elif not self.pay_date and self.expected_date and DATE_NOW >= self.expected_date:
			diffs = (datetime.strptime(DATE_NOW, DEFAULT_SERVER_DATE_FORMAT) - datetime.strptime(self.expected_date, DEFAULT_SERVER_DATE_FORMAT)).days

		note = u'Quá hạn %d ngày' % (diffs)
		self.with_context(exitx=True).write({'note': note if diffs > 0 else ''})
			
		# if ('expected_date' in vals or 'pay_date' in vals) and self.expected_date and DATE_NOW > self.expected_date:
		# 	d1 = res.pay_date if res.pay_date else DATE_NOW
		# 	note = u'Quá hạn %d ngày' % ((datetime.strptime(d1, DEFAULT_SERVER_DATE_FORMAT) - datetime.strptime(self.expected_date, DEFAULT_SERVER_DATE_FORMAT)).days)
		# 	self.with_context(exitx=True).write({'note': note})
		return res


class ProjectFactory(models.Model):
	_name = 'project.factory'
	_description = u'Nhà máy'
	
	name = fields.Char(u'Tên nhà máy')
	state = fields.Selection([('0', u'Ẩn'), ('1', u'Hiện')], string=u'Trạng thái', default='1')


class ProjectWarehouse(models.Model):
	_name = 'project.warehouse'
	_description = u'Kho'
	
	name = fields.Char(u'Tên kho')
	state = fields.Selection([('0', u'Ẩn'), ('1', u'Hiện')], string=u'Trạng thái', default='1')


class ProjectProject(models.Model):
	_name = 'project.project'
	_description = u'Dự án'
	_rec_name = 'fake_name'
	
	@api.model
	def get_constants(self):
		companies = self.env['res.company'].sudo().search_read([], ['name'])
		companies = [{'id': x['id'], 'name':x['name']} for x in companies]
		status = self.env['project.status'].sudo().search_read([], ['name'])
		status = [{'id': x['id'], 'name':x['name']} for x in status]
		pay_status = self.env['project.pay.status'].sudo().search_read([], ['name'])
		pay_status = [{'id': x['id'], 'name':x['name']} for x in pay_status]
		processes = self.env['project.manufact.process'].sudo().search_read([], ['name'])
		processes = [{'id': x['id'], 'name':x['name']} for x in processes]
		count_internal = self.search_count([('is_internal', '=', True), ('existed_role_uids', 'like', ',%s,' % (self._uid))])
		count_i_pay = self.search_count([('is_internal', '=', True), ('existed_role_uids', 'like', ',%s,' % (self._uid))])
		count_c_pay = self.search_count([('is_internal', '=', False), ('existed_role_uids', 'like', ',%s,' % (self._uid))])
		return {
			'process_total_id': my_parser.attr_read("project_process_total_id"),
			'status_end_id': my_parser.attr_read("project_status_end_id"),
			'companies_data': companies,
			'project_status': status,
			'pay_status': pay_status,
			'project_processes': processes,
			'count_internal': count_internal,
			'count_i_pay': count_i_pay,
			'count_c_pay': count_c_pay,
			'project_type': self._fields['project_type'].selection,
			'backend_menu_ids': my_parser.attr_read("custom_access_bkend_menu_ids")
		}

	def compute_role(self):
		try:
			accessible_uids = map(int, self.existed_role_uids[1:-1].split(','))
		except:
			accessible_uids = []
		for r in self:
			r.role = 0
			if self._uid == 1:
				r.role = -1
			elif len(accessible_uids) and self._uid in accessible_uids:
				role = self.env['project.project.role'].sudo().search([('project_id', '=', self.id), ('user_id', '=', self._uid)], limit=1)
				if (role.role_job_assign | 1) == role.role_job_assign:
					r.role |= ROLE_VIEW_JOB
					if (role.role_job_assign | 3) == role.role_job_assign:
						r.role |= ROLE_EDIT_JOB
						if (role.role_job_assign | 7) == role.role_job_assign:
							r.role |= ROLE_DEL_JOB
				if (role.role_material | 1) == role.role_material:
					r.role |= ROLE_VIEW_MATERIAL
					if (role.role_material | 3) == role.role_material:
						r.role |= ROLE_EDIT_MATERIAL
						if (role.role_material | 7) == role.role_material:
							r.role |= ROLE_DEL_MATERIAL
				if (role.role_drawing | 1) == role.role_drawing:
					r.role |= ROLE_VIEW_DRAWING
					if (role.role_drawing | 3) == role.role_drawing:
						r.role |= ROLE_EDIT_DRAWING
						if (role.role_drawing | 7) == role.role_drawing:
							r.role |= ROLE_DEL_DRAWING
				if (role.role_manufact | 1) == role.role_manufact:
					r.role |= ROLE_VIEW_MANUFACT
					if (role.role_manufact | 3) == role.role_manufact:
						r.role |= ROLE_EDIT_MANUFACT
						if (role.role_manufact | 7) == role.role_manufact:
							r.role |= ROLE_DEL_MANUFACT
				if (role.role_shipment | 1) == role.role_shipment:
					r.role |= ROLE_VIEW_SHIPMENT
					if (role.role_shipment | 3) == role.role_shipment:
						r.role |= ROLE_EDIT_SHIPMENT
						if (role.role_shipment | 7) == role.role_shipment:
							r.role |= ROLE_DEL_SHIPMENT
				if (role.role_costs | 1) == role.role_costs:
					r.role |= ROLE_VIEW_COSTS
					if (role.role_costs | 3) == role.role_costs:
						r.role |= ROLE_EDIT_COSTS
						if (role.role_costs | 7) == role.role_costs:
							r.role |= ROLE_DEL_COSTS
				if (role.role_timeline | 1) == role.role_timeline:
					r.role |= ROLE_VIEW_TIMELINE
					if (role.role_timeline | 3) == role.role_timeline:
						r.role |= ROLE_EDIT_TIMELINE
						if (role.role_timeline | 7) == role.role_timeline:
							r.role |= ROLE_DEL_TIMELINE
	
	@api.multi
	def compute_actor_id(self):
		for r in self:
			r.actor_id = self.env.user.id
			r.editable_project = self.env.user.quota_id.can_edit_project
	
	@api.multi
	def compute_show_review_date(self):
		for r in self:
			# Ẩn/hiện ô nhập ngày review
			r.show_review_date = True if r.status_id.id == my_parser.attr_read("project_status_review_id") else False
	
	@api.one
	def compute_review_date(self):
		if self.status_id == my_parser.attr_read("project_status_review_id") and self.review_to:
			self.review_date = (('%s %s' % (DATE_NOW, '23:59:59')).strptime(DEFAULT_SERVER_DATETIME_FORMAT) - self.review_to.strptime(DEFAULT_SERVER_DATETIME_FORMAT)).days
	
	@api.one
	def compute_project_end(self):
		self.project_end = self.status_id.id == my_parser.attr_read("project_status_end_id")
	
	@api.one
	def compute_pay_status_editable(self):
		self.pay_status_editable = self._uid == 1 or self.env.user.quota_id.can_edit_pay_paid
			
	fake_name 				= fields.Char(readonly=True)
	order 					= fields.Integer(u'STT', store=False, readonly=False)
	code 					= fields.Char(u'Mã CV')
	year 					= fields.Char(u'Năm')
	name 					= fields.Char(u'Tên dự án')
	typex 					= fields.Char(u'Hạng mục')
	location 				= fields.Char(u'Địa điểm')

	investor_id				= fields.Many2one('project.investor', u'Chủ đầu tư')
	investor_fname			= fields.Char(related='investor_id.full_name', store=False, readonly=True)
	investor_contact_ids 	= fields.Many2many('project.investor.contact', 'project_investor_contact_rel', 'project_id', 'contact_id', string=u'Liên hệ chủ đầu tư')
	investor_address_ids 	= fields.One2many(related="investor_id.address_ids", string=u'Địa chỉ chủ đầu tư')

	supervisor_id 			= fields.Many2one('project.supervisor', u'Tư vấn giám sát')
	supervisor_fname		= fields.Char(related='supervisor_id.full_name', store=False, readonly=True)
	supervisor_contact_ids 	= fields.Many2many('project.supervisor.contact', 'project_supervisor_contact_rel', 'project_id', 'contact_id', string=u'Liên hệ giám sát')
	supervisor_address_ids 	= fields.One2many(related='supervisor_id.address_ids', string=u'Địa chỉ giám sát')

	contractor_id 			= fields.Many2one('project.contractor', u'Thầu chính')
	contractor_fname		= fields.Char(related='contractor_id.full_name', store=False, readonly=True)
	contractor_contact_ids 	= fields.Many2many('project.contractor.contact', 'project_contractor_contact_rel', 'project_id', 'contact_id', string=u'Liên hệ thầu chính')
	contractor_address_ids 	= fields.One2many(related="contractor_id.address_ids", string=u'Địa chỉ thầu chính')

	chairman_ids 			= fields.Many2many('res.users','project_chairman_user_rel', 'project_chairman_id','user_id', u'Chủ nhiệm')
	chairman_str			= fields.Char(readonly=True)
	follower_ids 			= fields.Many2many('res.users','project_follower_user_rel', 'project_follower_id','user_id', u'Người theo dõi')
	follower_str			= fields.Char(readonly=True)
	status_id 				= fields.Many2one('project.status', u'Trạng thái CV')
	create_date 			= fields.Date(u'Ngày khởi tạo')
	write_date 				= fields.Date(u'Ngày cập nhật')
	note 					= fields.Text(u'Ghi chú')
	is_internal 			= fields.Boolean(u'Dự án nội bộ', help=u"Đây có phải là dự án nội bộ không?")
	master_id 				= fields.Many2one('project.master', u'Dự án')

	pay_status_id			= fields.Many2one('project.pay.status', u'Trạng thái TT')
	amount 					= fields.Float(u'Giá trị')
	paid 					= fields.Float(u'Đã TT')
	wait 					= fields.Float(u'Phải thu')
	pay_note 				= fields.Text(u'Ghi chú')
	pay_write_date 			= fields.Date(u'Ngày cập nhật', readonly=True)
	# pay detail
	pay_detail_id 			= fields.One2many('project.project.paydetail', 'project_id', string=u'Chi tiết thanh toán')
	pay_expected_id 		= fields.One2many('project.project.payexpected', 'project_id', u'Dự kiến thanh toán')

	start_date 				= fields.Date(u'Thời gian bắt đầu')
	role_ids 				= fields.One2many('project.project.role', 'project_id', u'Nhân sự nội bộ')
	job_ids 				= fields.One2many('project.project.job', 'project_id', u'Phiếu giao việc')
	material_ids 			= fields.One2many('project.project.material', 'project_id', u'Quy cách vật liệu')
	drawing_ids 			= fields.One2many('project.project.drawing', 'project_id', u'Bản vẽ')
	timeline_ids 			= fields.One2many('project.project.timeline', 'project_id', u'Timeline sản xuất')
	manufact_ids 			= fields.One2many('project.project.manufact', 'project_id', u'Theo dõi sản xuất')
	shipment_ids 			= fields.One2many('project.project.shipment', 'project_id', u'Xuất hàng')
	costs_ids 				= fields.One2many('project.project.costs', 'project_id', u'Chi phí công trường')

	# Lấy ra id các user nội bộ đã lưu
	# 	=> tìm user chưa thêm vào nội bộ, tránh trùng lặp
	# 	=> lọc các dự án mà user có thể xem
	existed_role_uids 		= fields.Char(readonly=True)
	# Mã các chủ nhiệm, người theo dõi của dự án
	muids					= fields.Char(readonly=True)
	role 					= fields.Integer(readonly=True, compute=compute_role, store=False)
	actor_id 				= fields.Integer(compute=compute_actor_id, store=False)
	editable_project 		= fields.Boolean(store=False)
	
	project_type			= fields.Selection([('1', u'Theo dự án'), ('2', u'Mua vật tư lẻ'), ('3', u'Mua vật tư thường kì')], string=u'Loại hình CV')
	project_desc			= fields.Char(u'Mô tả CV')
	project_buyer			= fields.Many2one('res.company', u'Bên mua')
	project_seller			= fields.Many2one('res.company', u'Bên bán')
	# Số ngày review dự án
	review_date				= fields.Integer(u'Số ngày review', help=u'Số ngày review dự án tính từ hôm nay', store=True, default=0)
	review_to				= fields.Datetime(u'Review đến', help=u"Review dự án đến hết ngày này\nSau ngày này tự động kết thúc review", readonly=True)
	show_review_date		= fields.Boolean(default=False, store=False, compute=compute_show_review_date)
	# Dự án đã kết thúc chưa
	project_end				= fields.Boolean(compute=compute_project_end)
	company_id				= fields.Many2one('res.company', u'Công ty')
	# Tổng tiền ứng - chi phí công trường
	costs_total_income		= fields.Float(u'Tổng ứng', readonly=True)
	# Tổng tiền chi - chi phí công trường
	costs_total_outcome		= fields.Float(u'Tổng chi', readonly=True)
	# Tổng dư - chi phí công trường
	costs_total_rest		= fields.Float(u'Số dư', readonly=True)
	# Điều khoản thanh toán
	pay_terms				= fields.Text(u'Điều khoản thanh toán')
	# Có thể cập nhật trạng thái TT
	pay_status_editable 	= fields.Boolean(compute=compute_pay_status_editable, readonly=True, store=False)


class Project(models.Model):
	_name = 'project.master'
	_description = u'Dự án'
	
	def _compute_pay_ids(self):
		self.pay_ids = self.project_ids
	
	name = fields.Char(u'Tên dự án')
	project_ids = fields.One2many('project.project', 'master_id', u'Dự án')
	pay_ids = fields.One2many('project.project', 'master_id', compute='_compute_pay_ids', store=True, string=u'Thanh toán')
	
	@api.multi
	def write(self, vals):
		return super(Project, self).write(vals)

	@api.model
	def set_custom_access_bkend_menu_ids(self):
		ids = [
			self.env.ref('project.menu_manager_project_contractor').id,
			self.env.ref('project.menu_manager_project_investor').id,
			self.env.ref('project.menu_manager_project_supervisor').id,
		]
		my_parser.attr_set('custom_access_bkend_menu_ids', ids)

