# -*- coding: utf-8 -*-
from odoo import models, fields


class View(models.Model):
    _inherit = 'ir.ui.view'

    type = fields.Selection(
		selection_add=[
			('project_master', 'Form'),
			('project_project_popup', 'Form'),
			('project_project_detail', 'Form'),
			('project_pay_popup', 'Form'),
			('project_shipment_popup', 'Form'),
			('project_job_popup', 'Form'),
			('project_material_popup', 'Form'),
			('project_drawing_popup', 'Form'),
			('project_costs_popup', 'Form'),
			('project_timeline_popup', 'Form'),
		])


class ActWindowView(models.Model):
    _inherit = 'ir.actions.act_window.view'

    view_mode = fields.Selection(
		selection_add=[
			('project_master', 'Form'),
			('project_project_popup', 'Form'),
			('project_project_detail', 'Form'),
			('project_pay_popup', 'Form'),
			('project_shipment_popup', 'Form'),
			('project_job_popup', 'Form'),
			('project_material_popup', 'Form'),
			('project_drawing_popup', 'Form'),
			('project_costs_popup', 'Form'),
			('project_timeline_popup', 'Form'),
		])
