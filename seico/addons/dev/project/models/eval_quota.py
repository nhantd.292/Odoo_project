# -*- coding: utf-8 -*-
from odoo import fields, models, api


class EvalQuota(models.Model):
	_inherit = 'eval.quota'
	
	@api.onchange('force_cant_view_pay', 'can_view_pay')
	def onchange_view_pay(self):
		if self.force_cant_view_pay == True:
			self.can_view_pay = False
			self.can_edit_pay_paid = False
		elif self.can_view_pay == True:
			self.force_cant_view_pay = False

	can_edit_project = fields.Boolean(string=u'Cập nhật dự án', 
		help=u"Nhân sự ngạch này có được tạo/sửa dự án không?")
	force_cant_view_pay = fields.Boolean(string=u'Ẩn thanh toán', default=True,
		help=u"Nhân sự ngạch này không được xem\nbất cứ thông tin thanh toán nào?")
	can_view_pay = fields.Boolean(string=u'Xem thanh toán',
		help=u"Nhân sự ngạch này có được xem\ntất cả thông tin thanh toán không?")
	can_edit_pay_paid = fields.Boolean(string=u'Cập nhật thanh toán',
		help=u"Nhân sự ngạch này có được tạo/sửa/xoá\nthông tin thanh toán không?")
	can_view_internal_project = fields.Boolean(string=u'Xem dự án nội bộ',
		help=u"Nhân sự ngạch này có được xem toàn bộ\ndự án/công việc nội bộ không?")
	can_del_pay_next = fields.Boolean(string=u'Xoá dự kiến TT', default=False,
		help=u"Nhân sự ngạch này có được xoá\nthông tin dự kiến thanh toán không?")
	can_access_backend = fields.Boolean(string=u'Cập nhật đối tác', default=False,
		help=u"Nhân sự ngạch này có được cập nhật thông tin Thầu chính,\nChủ đầu tư, Tư vấn giám sát thanh toán không?")
	can_quick_add_partner = fields.Boolean(string=u'Thêm nhanh đối tác', default=False,
		help=u"Nhân sự ngạch này có được thêm nhanh Thầu chính,\nChủ đầu tư, Tư vấn giám sát thanh toán không?")
	can_send_outdate_email = fields.Boolean(string=u'Gửi email quá hạn', default=False,
		help=u"Nhân sự ngạch này có được gửi email\nthông báo quá hạn dự kiến thanh toán không?")
	