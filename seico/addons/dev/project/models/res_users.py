# -*- coding: utf-8 -*-
from odoo import models, api


class ResUsers(models.Model):
	_inherit = 'res.users'

	@api.model
	def get_login_info(self):
		user = self.env['res.users'].sudo().browse(self.env.uid)
		companies = [x.name for x in user.company_ids if x.id != user.company_id.id]
		res = {
			'id': user.id,
			'name': user.name,
			'position': user.position.name,
			'quota': user.quota_id.code,
			'number': user.number,
			'company': user.company_id.name,
			'companies': companies,
		}
		return res
	
	@api.model
	def get_staff_role(self):
		actor = self.env['res.users'].sudo().browse(self.env.uid)
		admin_gid = self.env.ref('staff_evaluate.group_seico_admin').id
		res = {
			'is_admin': admin_gid in actor.groups_id.ids,
			'can_edit_project': actor.quota_id.can_edit_project,
			'can_view_pay': actor.quota_id.can_view_pay,
			'can_edit_pay_paid': actor.quota_id.can_edit_pay_paid,
			'can_view_internal_project': actor.quota_id.can_view_internal_project,
			'force_cant_view_pay': actor.quota_id.force_cant_view_pay,
			'can_del_pay_next': actor.quota_id.can_del_pay_next,
			'can_access_backend': actor.quota_id.can_access_backend,
			'can_quick_add_partner': actor.quota_id.can_quick_add_partner
		}
		return res

	@api.model
	def name_search(self, name='', args=None, operator='ilike', limit=100):
		# Lấy ds loại trừ 1 số giá trị
		if self._context.get('v_except', False):
			v_self = self._context.get('v_self', False)
			v_self_existed = self._context.get('v_self_existed', False)
			if v_self or v_self_existed:
				ids = []
				if v_self:
					for r in v_self:
						if r[0] == 0 and r[2]['user_id'] != False:
							ids.append(r[2]['user_id'])
				if v_self_existed:
					ids += map(int, v_self_existed[1:-1].split(','))
				args.append(['id', 'not in', ids])
		# Chỉ lấy theo chuỗi id của user
		elif self._context.get('v_restrict', False):
			ids_str = self._context.get('ids_str', '')
			ids = []
			if len(ids_str):
				ids = map(int, ids_str[1:-1].split(','))
			args.append(['id', 'in', ids])

		if self._context.get('v_details', False):
			args.append(['name', 'ilike', name])
			records = self.search(args, limit=8)
			res = []
			for r in records:
				res.append((r.id, '%s | %s | %s | %s' % (r.name, r.quota_id.code or u'', r.code or u'', r.position.name or u'')))
			return res
		return super(ResUsers, self).name_search(name, args, operator=operator, limit=limit)
	
	@api.model
	def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
		# Lấy ds loại trừ 1 số giá trị
		if self._context.get('v_except', False):
			v_self = self._context.get('v_self', False)
			v_self_existed = self._context.get('v_self_existed', False)
			ids = []
			if v_self:
				for r in v_self:
					if r[0] == 0 and r[2]['user_id'] != False:
						ids.append(r[2]['user_id'])
			if v_self_existed:
				ids += map(int, v_self_existed[1:-1].split(','))
			domain.append(['id', 'not in', ids])
		# Chỉ lấy theo chuỗi id của user
		elif self._context.get('v_restrict', False):
			ids_str = self._context.get('ids_str', '')
			ids = []
			if len(ids_str):
				ids = map(int, ids_str[1:-1].split(','))
			domain.append(['id', 'in', ids])
			
		return super(ResUsers, self).search_read(domain, fields, offset, limit, order)