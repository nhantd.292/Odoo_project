# -*- coding: utf-8 -*-
from odoo import api, models


class IrRule(models.Model):
	_inherit = 'ir.rule'
	
	@api.model
	def set_default_company_rule(self):
		r = self.env.ref('base.res_users_rule')
		r.update({
			'domain_force': '',
		})
