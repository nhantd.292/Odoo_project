# -*- coding: utf-8 -*-
from odoo import api, models


class IrMailServer(models.Model):
	_inherit = 'ir.mail_server'
	
	@api.model
	def set_default_mail_server(self):
		r = self.env.ref('base.ir_mail_server_localhost0')
		r.update({
			'name': u'Máy chủ mặc định',
			'smtp_host': 'smtp.gmail.com',
			'smtp_port': 465,
			'smtp_encryption': 'ssl',
			# TODO: Change default mail server account
			'smtp_user': 'qlhc@sbcsteel.com',
			'smtp_pass': 'sbcsteel2017'
		})
