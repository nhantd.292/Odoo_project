# -*- coding: utf-8 -*-

from odoo import models, api

class BaseConfigSettings(models.TransientModel):
	_inherit = 'base.config.settings'

	@api.model
	def enable_reset_password(self):
		existed = self.search([], limit=1)
		if len(existed):
			existed.update({'auth_signup_reset_password': True})
		else:
			self.create({'auth_signup_reset_password': True})