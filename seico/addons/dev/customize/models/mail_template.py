# -*- coding: utf-8 -*-
from odoo import api, models


class MailTemplate(models.Model):
	_inherit = 'mail.template'
	
	@api.model
	def set_default_mail_template(self):
		r = self.env.ref('auth_signup.reset_password_email')
		r.update({
			'name': u'Khôi phục mật khẩu',
			'subject': "Khôi phục mật khẩu - ${object.company_id.name}",
			'body_html': '''<![CDATA[
<div style="padding:0px;width:600px;margin:auto;background: #FFFFFF repeat top /100%;color:#777777">
    <p>Chào ${object.name},</p>
    <p>Chúng tôi nhận được yêu cầu khôi phục mật khẩu tài khoản ${object.company_id.name} liên kết đến email này</p>
    <p>Bạn có thể nhập mật khẩu mới theo link bên dưới (trong vòng 24 giờ)</p>
    <div style="text-align: center; margin-top: 16px;">
        <a href="${object.signup_url}" style="padding:5px 10px;font-size:12px;line-height:18px;color:#ffffff;border-color: #1e60aa;text-decoration:none;display:inline-block;margin-bottom:0px;font-weight:400;text-align:center;vertical-align:middle;white-space:nowrap;background-image:none;background-color: #1e60aa;border: 1px solid #1e60aa;border-radius:3px;">Đổi mật khẩu</a>
    </div>
    <p>Nếu bạn không yêu cầu đổi mật khẩu, bạn có thể bỏ qua email này</p>
    <p>Trân trọng,</p>
</div>''',
		})
		
		r = self.env.ref('auth_signup.set_password_email')
		r.update({
			'name': u'Đặt mật khẩu tài khoản lần đầu',
			'subject': "Đặt mật khẩu tài khoản - ${object.company_id.name}",
			'body_html': '''<![CDATA[
<div style="padding:0px;width:600px;margin:auto;background: #FFFFFF repeat top /100%;color:#777777">
<p>Dear ${object.name},</p>
    <p>Bạn đã được tạo tài khoản truy cập vào hệ thống ${object.company_id.name}.</p>
    <p>Bấm vào link bên dưới để thiết lập mật khẩu cho tài khoản.</p>
    <div style="text-align: center; margin-top: 16px;">
         <a href="${object.signup_url}" style="padding:5px 10px;font-size:12px;line-height:18px;color:#ffffff;border-color: #1e60aa;text-decoration:none;display:inline-block;margin-bottom:0px;font-weight:400;text-align:center;vertical-align:middle;white-space:nowrap;background-image:none;background-color: #1e60aa;border: 1px solid #1e60aa;border-radius:3px;">Thiết lập mật khẩu</a>
    </div>
    <p>Trân trọng,</p>
</div>''',
		})
