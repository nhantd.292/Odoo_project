# -*- coding: utf-8 -*-
from odoo import api, models


class ResUsers(models.Model):
	_inherit = 'res.users'

	@api.model
	def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
		domain.append(('|'))
		domain.append(('active', '=', False))
		domain.append(('active', '=', True))
		domain.append(('login', '!=', 'portaltemplate'))
		#self = self.sudo()
		res = super(ResUsers, self).search_read(domain, fields, offset, limit, order)
		res[:] = [d for d in res if d.get('id') not in [3,4]]
		return res