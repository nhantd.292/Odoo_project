{
    'name':'System customize',
    'description': 'Customize system',
    'version':'1.0',
    'author':'Cuu Nguyen',

    'data': [
        'security/rules.xml',
        'security/ir.model.access.csv',
        'data/data.xml',
        'views/views.xml',
    ],
    'website': 'vtechcom.org',
    'depends': ['base','web', 'auth_signup'],
    "qweb": [
        'static/src/xml/templates.xml',
    ],
    'application': False,
    'auto_install': False
}
