# -*- coding: utf-8 -*-
from odoo import http
import json
import os


class Dataparser():
	def __init__(self):
		self.path = os.path.abspath(__file__ + "/../data.json")
		self._data_reload()

	def _data_reload(self):
		self.data = json.load(open(self.path))

	def _write(self):
		with open(self.path, mode='w') as f:
			json.dump(self.data, f)
		http.parser._data_reload()

	def attr_read(self, attr_name):
		return self.data[attr_name]

	def attr_set(self, attr_name, attr_value):
		self.data[attr_name] = attr_value
		self._write()

	def attr_append(self, attr_name, attr_value):
		if attr_name not in self.data:
			self.data[attr_name] = attr_value
		else:
			if (isinstance(self.data[attr_name], list)) and (isinstance(attr_value, list)):
				for item in attr_value:
					self.data[attr_name].append(item)
				self.data[attr_name] = list(set(self.data[attr_name]))
			elif (isinstance(self.data[attr_name], dict)) and (isinstance(attr_value, dict)):
				for k,v in attr_value.iteritems():
					self.data[attr_name].update({k:v})
		self._write()

	def attr_property_remove(self, attr_name, attr_value):
		if (isinstance(self.data[attr_name], list)) and (isinstance(attr_value, list)):
			for item in attr_value:
				self.data[attr_name].remove(item)
			self.data[attr_name] = list(set(self.data[attr_name]))
		elif (isinstance(self.data[attr_name], dict)) and (isinstance(attr_value, dict)):
			for k in attr_value:
				del self.data[attr_name][k]
		self._write()

	def attr_delete(self, attr_name):
		for e in self.data:
			if e == attr_name:
				del self.data[e]
				break
		self._write()