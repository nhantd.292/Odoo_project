# -*- coding: utf-8 -*-
from odoo import http
import random
import time
import os
import logging
import odoo
import json
from odoo.tools import ustr
from odoo.addons.web.controllers.main import Action
import data_parser as parser

http.parser = parser.Dataparser()

_logger = logging.getLogger(__name__)
rpc_request = logging.getLogger(__name__ + '.rpc.request')
rpc_response = logging.getLogger(__name__ + '.rpc.response')


def new_session_gc(session_store):
	if random.random() < 0.1:
		# we keep session one day
		last_week = time.time() - 86400
		for fname in os.listdir(session_store.path):
			path = os.path.join(session_store.path, fname)
			try:
				if os.path.getmtime(path) < last_week:
					os.unlink(path)
			except OSError:
				pass
http.session_gc = new_session_gc

def serialize_exception(e):
	res = http.serialize_exception(e)
	res["name"] = type(e).__module__ + "." + type(e).__name__ if type(e).__module__ else type(e).__name__
	res["debug"] = ''
	res["message"] = ustr(e)
	res["arguments"] = http.to_jsonable(e.args)
	return res

def _handle_exception(self, exception):
	try:
		return super(http.JsonRequest, self)._handle_exception(exception)
	except Exception:
		if not isinstance(exception, (odoo.exceptions.Warning, http.SessionExpiredException, odoo.exceptions.except_orm)):
			_logger.exception("Lỗi trong quá trình xử lý yêu cầu.")
		error = {
			'code': 200,
			'message': "Lỗi máy chủ",
			'data': serialize_exception(exception),
			'exception': ''
		}
		if isinstance(exception, http.AuthenticationError):
			error['code'] = 100
			error['message'] = "Phiên làm việc không hợp lệ"
		if isinstance(exception, http.SessionExpiredException):
			error['code'] = 100
			error['message'] = "Phiên làm việc hết hạn"
			error['exception'] = "SessionExpired"
		return self._json_response(error=error)
http.JsonRequest._handle_exception = _handle_exception

# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import Action
