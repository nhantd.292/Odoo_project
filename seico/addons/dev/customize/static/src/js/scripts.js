odoo.define('customize', function (require) {
    "use strict";

    var core = require('web.core');
	var WebClient = require('web.AbstractWebClient');
	var CrashManager = require('web.CrashManager');
	var Dialog = require('web.Dialog');
	var RedirectWarningHandler = core.crash_registry.get('odoo.exceptions.RedirectWarning');
    var Ajax = require('web.ajax');
    var time = require('web.time');

    var QWeb = core.qweb;
    var _t = core._t;
    
    function genericJsonRpc (fct_name, params, fct) {
        var data = {
            jsonrpc: "2.0",
            method: fct_name,
            params: params,
            id: Math.floor(Math.random() * 1000 * 1000 * 1000)
        };
        var xhr = fct(data);
        var result = xhr.pipe(function(result) {
            core.bus.trigger('rpc:result', data, result);
            if (result.error !== undefined) {
                if (result.error.exception == "SessionExpired") {
                    window.location.href = '/web/login';
                    return false;
                }
                if (result.error.data.arguments[0] !== "bus.Bus not available in test mode") {
                    // console.error("Server application error", JSON.stringify(result.error));
                }
                return $.Deferred().reject("server", result.error);
            } else {
                return result.result;
            }
        }, function() {
            //console.error("JsonRPC communication error", _.toArray(arguments));
            var def = $.Deferred();
            return def.reject.apply(def, ["communication"].concat(_.toArray(arguments)));
        });
        // FIXME: jsonp?
        result.abort = function () { if (xhr.abort) xhr.abort(); };
        return result;
    }

    Ajax.jsonRpc = function (url, fct_name, params, settings) {
        return genericJsonRpc(fct_name, params, function(data) {
            return $.ajax(url, _.extend({}, settings, {
                url: url,
                dataType: 'json',
                type: 'POST',
                data: JSON.stringify(data, time.date_to_utc),
                contentType: 'application/json'
            }));
        });
    }

	WebClient.include({
	    init: function(parent) {
	        this._super.apply(this, arguments);
	        this.set('title_part', {"zopenerp": "Seico"});
	    }
	});

    Dialog.include({
        init: function (parent, options) {
            options = _.defaults(options || {}, {
                title: _t('Seico'), subtitle: '',
                size: 'large',
                dialogClass: '',
                $content: false,
                buttons: [{text: _t("Ok"), close: true}]
            });
            this._super(parent, options);
        }
    });

	CrashManager.include({
	    show_warning: function(error) {
	        if (!this.active) {
                return;
            }
            new Dialog(this, {
                size: 'medium',
                title: (_.str.capitalize(error.type) || _t("Warning")),
                subtitle: '',
                $content: $('<div>').html(QWeb.render('CrashManager.warning', {error: error}))
            }).open();
	    },
	    show_error: function(error) {
            if (!this.active) {
                return;
            }
            new Dialog(this, {
                title: _.str.capitalize(error.type),
                $content: QWeb.render('CrashManager.error', {error: error})
            }).open();
        }
	});

    RedirectWarningHandler.include({
        display: function() {
            var self = this;
            var error = this.error;
            error.data.message = error.data.arguments[0];

            new Dialog(this, {
                size: 'medium',
                title: (_.str.capitalize(error.type) || "Warning"),
                buttons: [
                    {text: error.data.arguments[2], classes : "btn-primary", click: function() {
                        window.location.href = '#action='+error.data.arguments[1];
                        self.destroy();
                    }},
                    {text: _t("Cancel"), click: function() { self.destroy(); }, close: true}
                ],
                $content: QWeb.render('CrashManager.warning', {error: error}),
            }).open();
        }
    });
});
