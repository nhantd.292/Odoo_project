# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Web enterprise',
    'category': 'Tools',
    'summary': 'Web enterprise module',
    'description': """Web enterprise module""",
    'depends': ['web'],
    'data': [ 'data.xml' ],
    'qweb': [ 'static/src/xml/templates.xml' ],
    'application': False,
    'is_public': True,
    'auto_install': True
}
