/* /web_enterprise/static/src/js/menu.js defined in bundle 'web.assets_backend' */
odoo.define('web_enterprise.Menu', function(require) {
    "use strict";
    var config = require('web.config');
    var core = require('web.core');
    var Widget = require('web.Widget');
    var SystrayMenu = require('web.SystrayMenu');
    var UserMenu = require('web.UserMenu');
    UserMenu.prototype.sequence = 0;
    SystrayMenu.Items.push(UserMenu);
    var QWeb = core.qweb;
    var Menu = Widget.extend({
        template: 'Menu',
        events: {
            'click .o_menu_toggle': function(ev) {
                ev.preventDefault();
                this.trigger_up((this.appswitcher_displayed) ? 'hide_app_switcher' : 'show_app_switcher');
                this.$el.parent().removeClass('o_mobile_menu_opened');
                // if (this.appswitcher_displayed) {
                //     this.$el.find('.o_mobile_menu_toggle').hide();
                // }
                // else {
                //     this.$el.find('.o_mobile_menu_toggle').show();
                // }
            },
            'click .o_mobile_menu_toggle': function() {
                if (!this.inited) {
                    this.inited = true;
                    $('span.v_mainmenu_bg').click(function(){
                        $('body.o_web_client').removeClass('o_mobile_menu_opened');
                    });
                    this.$el.find("a[data-action-id="+parseInt(this.getHashValue('action'), 10)+"]").addClass('active');
                    this.$el.find("ul.o_menu_sections").prepend('<li><img src="/web/static/src/img/logo3.png" class="v_logo_submenu"/>');
                }
                this.$el.parent().toggleClass('o_mobile_menu_opened');
            },
            'mouseover .o_menu_sections > li:not(.open)': function(e) {
                if (config.device.size_class >= config.device.SIZES.SM) {
                    var $opened = this.$('.o_menu_sections > li.open');
                    if ($opened.length) {
                        $opened.removeClass('open');
                        $(e.currentTarget).addClass('open').find('> a').focus();
                    }
                }
            },
        },
        init: function(parent, menu_data) {
            var self = this;
            this._super.apply(this, arguments);
            this.appswitcher_displayed = true;
            this.backbutton_displayed = false;
            this.$menu_sections = {};
            this.menu_data = menu_data;
            var $menu_sections = $(QWeb.render('Menu.sections', {
                'menu_data': this.menu_data
            }));
            $menu_sections.filter('section').each(function() {
                self.$menu_sections[parseInt(this.className, 10)] = $(this).children('li');
            });
            core.bus.on('change_menu_section', this, this.change_menu_section);
            core.bus.on('toggle_mode', this, this.toggle_mode);
            this.inited = false;
        },
        start: function() {
            var self = this;
            this.$menu_toggle = this.$('.o_menu_toggle');
            this.$menu_brand_placeholder = this.$('.o_menu_brand');
            this.$section_placeholder = this.$('.o_menu_sections');
            this.$menu_mobile_toggle = this.$('.o_mobile_menu_toggle');
            var on_secondary_menu_click = function(ev) {
                ev.preventDefault();
                var menu_id = $(ev.currentTarget).data('menu');
                var action_id = $(ev.currentTarget).data('action-id');
                self._on_secondary_menu_click(menu_id, action_id);
            };
            var menu_ids = _.keys(this.$menu_sections);
            var primary_menu_id, $section;
            for (var i = 0; i < menu_ids.length; i++) {
                primary_menu_id = menu_ids[i];
                $section = this.$menu_sections[primary_menu_id];
                $section.on('click', 'a[data-menu]', self, on_secondary_menu_click.bind(this));
            }
            this.systray_menu = new SystrayMenu(this);
            this.systray_menu.attachTo(this.$('.o_menu_systray'));
            core.bus.on("resize", this, _.debounce(this._handle_extra_items, 500));
            return this._super.apply(this, arguments);
        },
        getHashValue: function(key) {
          var matches = location.hash.match(new RegExp(key+'=([^&]*)'));
          return matches ? matches[1] : null;
        },
        toggle_mode: function(appswitcher, overapp) {
            this.appswitcher_displayed = !!appswitcher;
            this.backbutton_displayed = this.appswitcher_displayed && !!overapp;
            this.$menu_toggle.toggleClass('fa-times', this.appswitcher_displayed).toggleClass('fa-th', !this.appswitcher_displayed);
            if (this.appswitcher_displayed && !this.backbutton_displayed) {
                this.$menu_toggle.removeAttr('accesskey');
            } else {
                this.$menu_toggle.attr('accesskey', 'h');
            }
            this.$menu_toggle.toggleClass('hidden', this.appswitcher_displayed && !this.backbutton_displayed);
            this.$menu_brand_placeholder.toggleClass('hidden', this.appswitcher_displayed);
            this.$section_placeholder.toggleClass('hidden', this.appswitcher_displayed);
            this.$menu_mobile_toggle.toggleClass('hidden', this.appswitcher_displayed);
        },
        change_menu_section: function(primary_menu_id) {
            if (!this.$menu_sections[primary_menu_id]) {
                return;
            }
            if (this.current_primary_menu === primary_menu_id) {
                return;
            }
            if (this.current_primary_menu) {
                this.$menu_sections[this.current_primary_menu].detach();
            }
            for (var i = 0; i < this.menu_data.children.length; i++) {
                if (this.menu_data.children[i].id === primary_menu_id) {
                    this.$menu_brand_placeholder.text(this.menu_data.children[i].name);
                    break;
                }
            }
            this.$menu_sections[primary_menu_id].appendTo(this.$section_placeholder);
            this.current_primary_menu = primary_menu_id;
            this._handle_extra_items();
        },
        _trigger_menu_clicked: function(menu_id, action_id) {
            // console.log(menu_id, action_id)
            this.$el.find('a').removeClass('active');
            this.$el.find("a[data-action-id="+action_id+"][data-menu="+menu_id+"]").addClass('active');
            this.trigger_up('menu_clicked', {
                id: menu_id,
                action_id: action_id,
                previous_menu_id: this.current_secondary_menu || this.current_primary_menu,
            });
        },
        _on_secondary_menu_click: function(menu_id, action_id) {
            var self = this;
            if (action_id) {
                self._trigger_menu_clicked(menu_id, action_id);
                this.current_secondary_menu = menu_id;
            }
        },
        action_id_to_primary_menu_id: function(action_id) {
            var primary_menu_id, found;
            for (var i = 0; i < this.menu_data.children.length && !primary_menu_id; i++) {
                found = this._action_id_in_subtree(this.menu_data.children[i], action_id);
                if (found) {
                    primary_menu_id = this.menu_data.children[i].id;
                }
            }
            return primary_menu_id;
        },
        _action_id_in_subtree: function(root, action_id) {
            if (root.action && root.action.split(',')[1] === String(action_id)) {
                return true;
            }
            var found;
            for (var i = 0; i < root.children.length && !found; i++) {
                found = this._action_id_in_subtree(root.children[i], action_id);
            }
            return found;
        },
        menu_id_to_action_id: function(menu_id, root) {
            if (!root) {
                root = $.extend(true, {}, this.menu_data);
            }
            if (root.id === menu_id) {
                return root.action.split(',')[1];
            }
            for (var i = 0; i < root.children.length; i++) {
                var action_id = this.menu_id_to_action_id(menu_id, root.children[i]);
                if (action_id !== undefined) {
                    return action_id;
                }
            }
            return undefined;
        },
        _handle_extra_items: function() {
            if (!this.$el.is(":visible")) return;
            if (this.$extraItemsToggle) {
                this.$extraItemsToggle.find("> ul > *").appendTo(this.$section_placeholder);
                this.$extraItemsToggle.remove();
            }
            if (config.device.size_class < config.device.SIZES.SM) {
                return;
            }
            var width = this.$el.width();
            var menuItemWidth = this.$section_placeholder.outerWidth(true);
            var othersWidth = this.$menu_toggle.outerWidth(true) + this.$menu_brand_placeholder.outerWidth(true) + this.systray_menu.$el.outerWidth(true);
            if (width < menuItemWidth + othersWidth) {
                var $items = this.$section_placeholder.children();
                var nbItems = $items.length;
                menuItemWidth += 44;
                do {
                    nbItems--;
                    menuItemWidth -= $items.eq(nbItems).outerWidth(true);
                } while (width < menuItemWidth + othersWidth);
                var $extraItems = $items.slice(nbItems).detach();
                this.$extraItemsToggle = $("<li/>", {
                    "class": "o_extra_menu_items"
                });
                this.$extraItemsToggle.append($("<a/>", {
                    href: "#",
                    "class": "dropdown-toggle fa fa-plus",
                    "data-toggle": "dropdown"
                }));
                this.$extraItemsToggle.append($("<ul/>", {
                    "class": "dropdown-menu"
                }).append($extraItems));
                this.$extraItemsToggle.appendTo(this.$section_placeholder);
            }
        },
    });
    return Menu;
});;