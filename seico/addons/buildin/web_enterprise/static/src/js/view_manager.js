/* /web_enterprise/static/src/js/view_manager.js defined in bundle 'web.assets_backend' */
odoo.define('web_enterprise.ViewManager', function(require) {
    "use strict";
    var config = require('web.config');
    var ViewManager = require('web.ViewManager');
    ViewManager.include({
        get_default_view: function() {
            var default_view = this._super.apply(this, arguments);
            if (config.device.size_class <= config.device.SIZES.XS && !default_view.mobile_friendly) {
                default_view = (_.find(this.views, function(v) {
                    return v.mobile_friendly;
                })) || default_view;
            }
            return default_view;
        },
    });
});;